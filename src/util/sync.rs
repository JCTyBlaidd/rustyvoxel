use parking_lot::{Mutex, MutexGuard};
use rayon_core::{current_num_threads, current_thread_index};
use std::cell::UnsafeCell;

/// Rayon-Synchronised
///  thread-local access
/// Allows for a vector of values
/// to be shared across a rayon scope
pub struct ThreadSync<T: Send> {
    locals: Vec<UnsafeCell<T>>,
    fallback: Mutex<T>,
}

/// This is safe to sync since vec is
///  accessed, read-only thread_locally
///  and the mutex is thread
unsafe impl<T: Send> Sync for ThreadSync<T> {}

/// This is safe to send since the mutex
///  is safe to send and the vec can be
///  sent, only being accessed in a shared
///  manner
unsafe impl<T: Send> Send for ThreadSync<T> {}

impl<T: Send> ThreadSync<T> {
    /// Create new per-thread synchronised access
    ///  this assumes that rayon will not change the
    ///  number of threads for optimal performance
    ///  but it has a fallback mutex as well
    pub fn new<F: FnMut() -> T>(mut create: F) -> ThreadSync<T> {
        let num_threads = current_num_threads();
        let mut vec = Vec::with_capacity(num_threads);
        for _i in 0..num_threads {
            vec.push(UnsafeCell::new(create()));
        }
        debug_assert_eq!(vec.len(), num_threads);
        ThreadSync {
            locals: vec,
            fallback: Mutex::new(create()),
        }
    }

    #[inline]
    pub fn with_local<F: FnOnce(&mut T)>(&self, f: F) {
        match current_thread_index() {
            Some(index) => match self.locals.get(index) {
                Some(t) => unsafe {
                    f(&mut *t.get());
                },
                None => self.with_fallback(f),
            },
            None => self.with_fallback(f),
        }
    }

    #[inline]
    pub fn with_local_no_fallback<F: FnOnce(&mut T)>(&self, f: F) {
        let index = current_thread_index().unwrap();
        let value = &self.locals[index];
        unsafe {
            f(&mut *value.get());
        }
    }

    #[cold]
    fn get_fallback(&self) -> MutexGuard<T> {
        self.fallback.lock()
    }

    #[cold]
    pub fn with_fallback<F: FnOnce(&mut T)>(&self, f: F) {
        let mut guard = self.get_fallback();
        f(&mut *guard);
    }

    #[inline]
    pub fn with_fallback_mut<F: FnOnce(&mut T)>(&mut self, f: F) {
        f(self.fallback.get_mut());
    }

    #[inline]
    pub fn for_each<F: FnMut(&mut T)>(&mut self, mut f: F) {
        for local in self.locals.iter() {
            unsafe {
                let mut_ref = &mut *local.get();
                f(mut_ref);
            }
        }
        f(self.fallback.get_mut());
    }

    /*
    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> {
        let mut mutex_iter: Option<&mut T> = Some(self.fallback.get_mut());
        let mut vec_iter = self.locals.iter_mut()
            .map(|v| {
            unsafe {
                &mut *v.get();
            }
        });
        vec_iter.chain(mutex_iter.iter_mut())
    }*/
}

#[cfg(test)]
mod tests {
    use rayon::prelude::*;
    use std::mem;
    use std::thread::sleep;
    use std::time::Duration;
    use test::{black_box, Bencher};

    #[test]
    fn test_addition() {
        let mut sync = super::ThreadSync::new(|| 0);
        async_calc_sum(500, &mut sync);
        sync.with_fallback_mut(|v| {
            assert_eq!(*v, 0);
            *v = 42;
        });
        let mut total = 0;
        sync.for_each(|v| {
            total += *v;
        });
        assert_eq!(total, 542);
    }

    struct DropGuard<F: Fn()> {
        f: F,
    }
    impl<F: Fn()> Drop for DropGuard<F> {
        fn drop(&mut self) {
            (self.f)()
        }
    }

    #[test]
    fn test_drop() {
        let count: super::Mutex<isize> = super::Mutex::new(0isize);
        let mut sync = super::ThreadSync::new(|| {
            *count.lock() += 1;
            DropGuard {
                f: || {
                    *count.lock() -= 1;
                },
            }
        });
        rayon::scope(|scope| {
            for _i in 0..100 {
                scope.spawn(|_| {
                    sync.with_local(|val| {
                        black_box(val); //touch value
                    })
                })
            }
        });
        let mut total = 0isize;
        sync.for_each(|_v| {
            total += 1;
        });
        assert_eq!(*count.lock(), total);
        mem::drop(sync);
        assert_eq!(*count.lock(), 0);
    }

    #[bench]
    fn bench_async_sum100(bench: &mut Bencher) -> Result<(), ()> {
        let mut sync = super::ThreadSync::new(|| 0);
        bench.iter(|| {
            black_box(async_calc_sum(black_box(100), &mut sync));
        });
        Ok(())
    }

    #[bench]
    fn bench_tl_access(bench: &mut Bencher) -> Result<(), ()> {
        let sync = super::ThreadSync::new(|| "Random_Text");
        rayon::scope(|scope| {
            scope.spawn(|_| {
                bench.iter(|| {
                    sync.with_local(|val| {
                        assert_eq!(black_box(val), &"Random_Text");
                    })
                });
            });
        });
        Ok(())
    }

    #[bench]
    fn bench_tl_panic_access(bench: &mut Bencher) -> Result<(), ()> {
        let sync = super::ThreadSync::new(|| "Random_Text");
        rayon::scope(|scope| {
            scope.spawn(|_| {
                bench.iter(|| {
                    sync.with_local_no_fallback(|val| {
                        assert_eq!(black_box(val), &"Random_Text");
                    })
                });
            });
        });
        Ok(())
    }

    #[bench]
    fn bench_tl_fallback_access(bench: &mut Bencher) -> Result<(), ()> {
        let sync = super::ThreadSync::new(|| "Random_Text");
        bench.iter(|| {
            sync.with_fallback(|val| {
                assert_eq!(black_box(val), &"Random_Text");
            })
        });
        Ok(())
    }

    #[bench]
    fn bench_read_async100(bench: &mut Bencher) -> Result<(), ()> {
        let sync = super::ThreadSync::new(|| "Random_Text");
        bench.iter(|| {
            rayon::scope(|scope| {
                for _i in 0..100 {
                    scope.spawn(|_| {
                        sync.with_local(|val| {
                            black_box(val);
                        })
                    })
                }
            });
        });
        Ok(())
    }

    fn async_calc_sum(target: i32, sync: &mut super::ThreadSync<i32>) {
        sync.for_each(|v| {
            *v = 0;
        });
        rayon::scope(|scope| {
            for _i in 0..target {
                scope.spawn(|_| {
                    sync.with_local(|v| {
                        *v += 1;
                    });
                });
            }
        });
        let mut total = 0;
        sync.for_each(|v| {
            total += *v;
        });
        assert_eq!(total, target);
        sync.with_fallback_mut(|v| {
            assert_eq!(*v, 0);
        });
    }
}
