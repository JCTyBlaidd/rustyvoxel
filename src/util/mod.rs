use clap::{crate_authors, crate_version, App, Arg};
use std::time::{Duration, Instant};

pub mod logging;
pub mod sync;
pub mod graph;

pub struct ProgramConfig {
    pub server_bootstrap: bool,
    pub use_vulkan: Option<bool>,
    pub rayon_cpu_count: u8,
    pub standard_debug: u8,
    pub graphics_debug: u8,
    pub setup_render_doc: bool,
}
impl ProgramConfig {
    pub fn parse_arguments() -> ProgramConfig {
        let matches = App::new("Rusty Voxel")
            .version(&crate_version!()[..])
            .author(crate_authors!())
            .about("Rust Voxel Game...")
            .arg(
                Arg::with_name("SERVER_BOOTSTRAP")
                    .short("s")
                    .long("server")
                    .help("Launch as a server"),
            )
            .arg(
                Arg::with_name("GRAPHICS_API")
                    .short("G")
                    .long("graphics")
                    .takes_value(true)
                    .possible_values(&["vk", "gl", "vulkan", "open_gl"])
                    .help("Request the usage of a specific graphics API"),
            )
            .arg(
                Arg::with_name("RAYON_CPU")
                    .short("A")
                    .long("threading")
                    .takes_value(true)
                    .validator(|v| match v.parse::<i64>() {
                        Ok(val) => {
                            if val > 0 && val < 128 {
                                Ok(())
                            } else {
                                Err("Value is not in range 0 < x < 128".to_owned())
                            }
                        }
                        Err(_) => Err("Value is not an valid integer".to_owned()),
                    })
                    .help("Choose the size of the rayon thread_pool, defaults to number of cpus"),
            )
            .arg(
                Arg::with_name("GRAPHICS_DEBUG")
                    .short("D")
                    .multiple(true)
                    .help("Enable Graphics Debugging, multiple enables more features"),
            )
            .arg(
                Arg::with_name("STANDARD_DEBUG")
                    .short("d")
                    .long("debug")
                    .multiple(true)
                    .help("Enable Standard Debugging, multiple enables more features"),
            )
            .arg(
                Arg::with_name("RENDER_DOC")
                    .long("render_doc")
                    .help("Setup RenderDoc Debugging automatically"),
            )
            .get_matches();

        let use_vulkan = matches
            .value_of("GRAPHICS_API")
            .map(|s| &s == &"vk" || &s == &"vulkan");

        let rayon_cpu_count = matches
            .value_of("RAYON_CPU")
            .map(|s| s.parse::<u8>().unwrap_or(0))
            .unwrap_or(0);

        ProgramConfig {
            server_bootstrap: matches.is_present("SERVER_BOOTSTRAP"),
            use_vulkan,
            rayon_cpu_count,
            standard_debug: matches.occurrences_of("STANDARD_DEBUG") as u8,
            graphics_debug: matches.occurrences_of("GRAPHICS_DEBUG") as u8,
            setup_render_doc: matches.is_present("RENDER_DOC"),
        }
    }
}

#[derive(Debug, Clone)]
pub struct PerSecondTimer {
    array: Vec<Duration>,
    current_index: usize,
    last_notify: Instant,
    current_total: Duration,
}

impl PerSecondTimer {
    pub fn new(alloc: usize) -> PerSecondTimer {
        PerSecondTimer {
            array: vec![Default::default(); alloc],
            current_index: 0,
            last_notify: Instant::now(),
            current_total: Default::default(),
        }
    }

    pub fn notify_event(&mut self) {
        let current = Instant::now();
        self.current_total -= self.array[self.current_index];
        self.array[self.current_index] = current.duration_since(self.last_notify);
        self.current_total += self.array[self.current_index];
        self.last_notify = current;
        self.current_index = (self.current_index + 1) % self.array.len();
    }

    pub fn get_average_seconds(&self) -> f64 {
        (self.current_total.as_secs() as f64) / (self.array.len() as f64)
    }

    pub fn get_average_rate(&self) -> f64 {
        (self.array.len() as f64) / (self.current_total.as_secs() as f64)
    }
}

#[derive(Copy, Clone, Hash, Debug, PartialEq, Eq, PartialOrd, Ord, Default)]
pub struct Version {
    pub major: u8,
    pub minor: u8,
    pub patch: u16,
}

impl Version {
    pub fn as_int(&self) -> u32 {
        ((self.major as u32) << 24) | ((self.minor as u32) << 16) | (self.patch as u32)
    }
}
