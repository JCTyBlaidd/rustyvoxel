use log::{Log, Metadata, Record};
use std::borrow::Cow;
use std::fmt::Debug;
use std::fmt::{Display, Error, Formatter};
use std::io::{stdout, Stdout, Write};

#[cfg(windows)]
use ansi_term::enable_ansi_support;
use ansi_term::{ANSIGenericString, Style};

pub use ansi_term::Colour;
pub struct Enabled(pub bool);

pub fn highlight<'a, I, S: 'a + ToOwned + ?Sized>(input: I) -> ANSIGenericString<'a, S>
where
    I: Into<Cow<'a, S>>,
    <S as ToOwned>::Owned: Debug,
{
    Colour::Purple.paint(input)
}

pub fn success<'a, I, S: 'a + ToOwned + ?Sized>(input: I) -> ANSIGenericString<'a, S>
where
    I: Into<Cow<'a, S>>,
    <S as ToOwned>::Owned: Debug,
{
    Colour::Green.bold().paint(input)
}

pub fn failure<'a, I, S: 'a + ToOwned + ?Sized>(input: I) -> ANSIGenericString<'a, S>
where
    I: Into<Cow<'a, S>>,
    <S as ToOwned>::Owned: Debug,
{
    Colour::Red.bold().paint(input)
}

pub fn enabled(val: bool) -> Enabled {
    Enabled(val)
}

pub fn bold<'a, I, S: 'a + ToOwned + ?Sized>(input: I) -> ANSIGenericString<'a, S>
    where
        I: Into<Cow<'a, S>>,
        <S as ToOwned>::Owned: Debug,
{
    Style::new().bold().paint(input)
}

pub fn mark_cyan<'a, I, S: 'a + ToOwned + ?Sized>(input: I) -> ANSIGenericString<'a, S>
    where
        I: Into<Cow<'a, S>>,
        <S as ToOwned>::Owned: Debug,
{
    Colour::Cyan.bold().paint(input)
}
pub fn mark_yellow<'a, I, S: 'a + ToOwned + ?Sized>(input: I) -> ANSIGenericString<'a, S>
    where
        I: Into<Cow<'a, S>>,
        <S as ToOwned>::Owned: Debug,
{
    Colour::Yellow.bold().paint(input)
}

impl Display for Enabled {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let paint = if self.0 {
            Colour::Green.bold().paint("Enabled")
        } else {
            Colour::Red.bold().paint("Disabled")
        };
        write!(f, "{}", paint)
    }
}

pub struct AnsiLogger {
    out: Stdout,
    colours: [(Colour, &'static str); 6],
}

impl AnsiLogger {
    pub fn new_logger() -> Box<dyn Log> {
        #[cfg(windows)]
        {
            let _ = enable_ansi_support();
        }

        Box::new(AnsiLogger {
            out: stdout(),
            colours: [
                (Colour::White,  "[OFF]   "),
                (Colour::Red,    "[ERROR] "),
                (Colour::Yellow, "[WARN]  "),
                (Colour::Cyan,   "[INFO]  "),
                (Colour::Purple, "[DEBUG] "),
                (Colour::White,  "[TRACE] "),
            ],
        })
    }
}

impl Log for AnsiLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        let (col, name) = self.colours[record.level() as usize];
        let painted_col = col.bold().paint(name);
        let mut lock = self.out.lock();
        let _ = writeln!(lock, "{}{}", &painted_col, record.args());
    }

    fn flush(&self) {
        let mut lock = self.out.lock();
        let _ = lock.flush();
    }
}
