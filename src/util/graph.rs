pub use petgraph::Graph;
use petgraph::{
	graph::{
		Edge,
		EdgeIndex,
		Node,
		NodeIndex,
		IndexType,
	},
	visit::{
		depth_first_search,
		DfsEvent,
		EdgeRef,
		GraphRef
	},
	Direction,
	Directed,
};
use hashbrown::{
	HashSet,
	HashMap
};

pub enum GraphError {
	CircularDependency
}

pub trait GraphExt {
	type NodeIndexExt;
	type ChainResExt;
	fn topological_sort(&self) -> Result<Vec<Self::NodeIndexExt>,GraphError>;
	fn transitive_reduction(&mut self) -> Result<(),GraphError>;
	fn make_chains(&self) -> Self::ChainResExt;
}

impl<N: Clone, E: Clone, Ix: IndexType> GraphExt for Graph<N,E,Directed,Ix> {
	
	type NodeIndexExt = NodeIndex<Ix>;
	type ChainResExt = Graph<Vec<N>,E,Directed,Ix>;
	
	fn topological_sort(&self) -> Result<Vec<NodeIndex<Ix>>,GraphError> {
		let mut topological_sort = Vec::new();
		let mut node_set : Vec<_> = self.externals(Direction::Incoming).collect();
		let mut edge_set : HashSet<_> = self.edge_indices().collect();
		
		while let Some(node) = node_set.pop() {
			topological_sort.push(node);
			for edge in Self::edges(self,node) {
				if edge_set.remove(&edge.id()) {
					let edge_count = Self::edges_directed(
						self,edge.target(),Direction::Incoming
					).filter(|edge| {
						edge_set.contains(&edge.id())
					}).count();
					if edge_count == 0 {
						node_set.push(edge.target());
					}
				}
			}
		}
		
		if edge_set.len() != 0 {
			Err(GraphError::CircularDependency)
		}else {
			Ok(topological_sort)
		}
	}
	
	
	///Calculate transitive dependency of graph
	/// and detect cycles
	fn transitive_reduction(&mut self) -> Result<(),GraphError> {
		let topological_sort = self.topological_sort()?;
		let mut source_map = Vec::new();
		
		for (curr_idx,node) in topological_sort.iter().enumerate() {
			let mut inbound_count = Self::edges_directed(
				self,*node,Direction::Incoming
			).count();
			let mut source_set  = HashSet::new();
			
			//Add dependency requirements transitively
			if inbound_count != 0 {
				for prev_idx in (0..curr_idx).rev() {
					let prev_node = topological_sort[prev_idx];
					if let Some(edge) = self.find_edge(prev_node,*node) {
						let prev_sources  = &source_map[prev_idx];
						
						if source_set.is_superset(prev_sources) {
							//Dependency not required
							self.remove_edge(edge);
						}else{
							//Merge Sources
							for value in prev_sources {
								source_set.insert(*value);
							}
						}
					}
				}
			}
			
			//Add self as node
			source_set.insert(*node);
			source_map.push(source_set);
		}
		Ok(())
	}
	
	/// Merge A->B->C 1-1 dependency chains into
	///  a single node with (A,B,C) as the value
	fn make_chains(&self) -> Graph<Vec<N>,E,Directed,Ix> {
		/*
		let mut new_graph = self.map(
			|node| {
				self.node_weight(node).unwrap().clone();
			},
			|edge| {
				self.edge_weight(edge).unwrap().clone();
			}
		);
		let node_list : Vec<_> = new_graph.node_indices().collect();
		for node in node_list {
			if self.node_weight(node).is_some() {
				//Node Exists!
			}
		}
		new_graph
		*/
		unimplemented!()
	}
}


#[cfg(test)]
mod tests {
	use super::*;
	use hashbrown::HashMap;
	
	fn make_graph(src: &[(usize,usize)]) -> Graph<usize,i32> {
		let mut graph = Graph::new();
		let mut assignments = HashMap::new();
		for (src,dst) in src {
			let src_idx = *assignments.entry(*src).or_insert_with(|| {
				graph.add_node(*src)
			});
			let dst_idx = *assignments.entry(*dst).or_insert_with( || {
				graph.add_node(*dst)
			});
			graph.add_edge(src_idx,dst_idx,1);
		}
		graph
	}
	
	fn fix_dep_list(deps: &[(usize,usize)]) -> Vec<(usize,usize)> {
		let mut graph = make_graph(deps);
		
		assert!(graph.transitive_reduction().is_ok());
		
		let mut result: Vec<(usize,usize)> = graph.edge_references().map(|v| {
			let src = *graph.node_weight(v.source()).unwrap();
			let dst = *graph.node_weight(v.target()).unwrap();
			(src,dst)
		}).collect();
		result.sort_unstable();
		
		result
	}
	
	#[test]
	pub fn test_dep_chain() {
		let src_deps = &[(0,1),(0,2),(0,3),(0,4),(1,2),(1,3),(1,4),(2,3),(2,4),(3,4)];
		let dst_deps = &[(0,1),(1,2),(2,3),(3,4)];
		
		assert_eq!(fix_dep_list(src_deps).as_slice(),dst_deps);
	}
	
	#[test]
	pub fn test_dep_extra_triple() {
		let src_deps = &[
			(1,2),(1,3),(1,5),(1,6),(1,10),(1,13),(2,3),(3,4),
			(3,5),(4,5),(6,7),(6,11),(6,13),(7,8),(7,10),(7,11),
			(7,12),(8,9),(8,12),(8,13),(9,13),(10,11),(10,12),
			(11,13),(12,5),(12,13),(13,5)
		];
		let dst_deps = &[
			(1,2),(1,6),(2,3),(3,4),(4,5),(6,7),(7,8),(7,10),(8,9),
			(8,12),(9,13),(10,11),(10,12),(11,13),(12,13),(13,5)
		];
		
		assert_eq!(fix_dep_list(src_deps).as_slice(),dst_deps);
	}
	
	#[test]
	pub fn test_multi_direct_deps() {
		let src_deps = &[(0,1),(0,2),(0,3),(1,3),(2,3)];
		let dst_deps = &[(0,1),(0,2),(1,3),(2,3)];
		
		assert_eq!(fix_dep_list(src_deps).as_slice(),dst_deps);
	}
	
	#[test]
	pub fn test_cycles() {
		assert!(make_graph(&[(0,0)]).transitive_reduction().is_err());
		assert!(make_graph(&[(0,1),(1,0)]).transitive_reduction().is_err());
	}
	
	#[test]
	pub fn test_no_remove_empty() {
		let src_deps = &[];
		let dst_deps = src_deps;
		
		assert_eq!(fix_dep_list(src_deps).as_slice(),dst_deps);
	}
	
	#[test]
	pub fn test_no_remove_sparse() {
		let src_deps = &[(0,1),(1,2),(2,3),(6,1),(10,1),(14,2),(24,2),(30,33)];
		let dst_deps = src_deps;
		
		assert_eq!(fix_dep_list(src_deps).as_slice(),dst_deps);
	}
	
	#[test]
	pub fn test_no_remove_in_out() {
		let src_deps = &[(0,3),(1,3),(2,3),(3,4),(3,5),(3,6)];
		let dst_deps = src_deps;
		
		assert_eq!(fix_dep_list(src_deps).as_slice(),dst_deps);
	}
	
	#[test]
	pub fn test_no_remove_dual_path() {
		let src_deps = &[(0,1),(0,2),(1,3),(2,4),(3,5),(4,5)];
		let dst_deps = src_deps;
		
		assert_eq!(fix_dep_list(src_deps).as_slice(),dst_deps);
	}
	
	#[test]
	pub fn test_no_remove_triple_path() {
		let src_deps = &[(0,1),(0,4),(0,6),(1,2),(2,3),(3,8),(4,5),(5,3),(6,7),(7,3)];
		let dst_deps = src_deps;
		
		assert_eq!(fix_dep_list(src_deps).as_slice(),dst_deps);
	}
}
