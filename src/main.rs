#![feature(const_transmute)]
#![feature(try_trait)]
#![feature(cfg_target_has_atomic)]
#![feature(integer_atomics)]
#![feature(test)]
#![feature(termination_trait_lib)]

//FIXME: REMOVE LATER
//#![allow(unused)]

#[cfg(test)]
extern crate test;

#[cfg(feature = "client_side")]
mod client;

mod common;
mod util;
mod prelude;
mod resource;

use self::prelude::*;
use self::common::GameState;
use self::resource::ResourceManager;
use self::util::{ProgramConfig,logging::AnsiLogger};

use log::{set_boxed_logger, set_max_level, LevelFilter, logger};
use rayon::{current_num_threads, ThreadPoolBuilder};
use winit::event_loop::ControlFlow;
use std::mem::drop;


#[cfg(feature = "client_side")]
use self::client::{
    GlobalRender,
    UserInterface,
};

fn main() -> Result<(),()> {
    
    //Global State Initialization: with config parsing
    let config : ProgramConfig = match global_init() {
        Some(config) => config,
        None => return Err(())
    };
    
    
    //Dispatch to either of the two main loops
    if config.server_bootstrap || !cfg!(feature = "client_side") {
        if !config.server_bootstrap {
            warn!("{}",fmt_bold("Client boot not available: Using Server Boot"));
        }
        info!("{}",fmt_bold("Starting Server Bootstrap:"));
        info!("");
        server_main_loop(config)
    } else {
        info!("{}",fmt_bold("Starting Client Bootstrap:"));
        info!("");
        #[cfg(feature = "client_side")]
        {
            client_main_loop(config)
        }
    }
    
    //Finish Shutdown Sequence
    global_destroy();
    Ok(())
}

fn global_destroy() {
    info!("");
    info!("{}", fmt_bold("Shutting Down..."));
    
    //Destroy Global Thread Pool
    // TODO: CAN BE IMPLEMENTED??
    
    //Destroy Global Logger:
    // TODO: CAN BE IMPLEMENTED??
    
    //DEBUG
    trace!("TRACE");
    debug!("DEBUG");
    info!("INFO: {}", util::logging::Enabled(true));
    warn!("WARN: {}", util::logging::Enabled(false));
    error!("ERROR");//DEBUG: CODE
}

fn global_init() -> Option<ProgramConfig> {
    
    //Load Global Logger
    match set_boxed_logger(AnsiLogger::new_logger()) {
        Ok(()) => {
            set_max_level(LevelFilter::Trace);
            info!("{}: {}", fmt_bold("Global Logger"), fmt_enabled(true));
        }
        Err(_) => {
            eprintln!("AnsiLogger: Disabled");
            return None;
        }
    }
    
    //Load Global Config
    info!("{}",fmt_bold("Loading Global State:"));
    let config = ProgramConfig::parse_arguments();
    
    //Setup Global Config: LogLevel
    if config.standard_debug == 0 {
        set_max_level(LevelFilter::Info);
        info!(" - {}:       {}", fmt_bold("Logging Filter"), fmt_cyan("Info"));
    } else {
        set_max_level(LevelFilter::Trace);
        info!(" - {}:       {}", fmt_bold("Logging Filter"), fmt_highlight("Trace"));
    }
    
    //Setup Global Config: Thread Pool
    let mut pool_builder = ThreadPoolBuilder::new()
        .thread_name(|i| {
            "rayon-worker-".to_owned() + &i.to_string()
        });
    if config.rayon_cpu_count > 0 {
        pool_builder = pool_builder.num_threads(config.rayon_cpu_count as usize);
    }
    if let Err(e) = pool_builder.build_global() {
        warn!(" - Failed to build global thread pool {:?}", e);
        return None;
    } else {
        info!(" - {}:   {}", fmt_bold("Global Thread Pool"), fmt_enabled(true));
        info!(
            " - {}:     {}",
            fmt_bold("Thread Pool Size"),
            Colour::Cyan.paint(format!("{} Threads", current_num_threads()))
        );
    }
    
    //Setup Global Config: PrettyPrint Args
    info!(
        " - {}:      {}",
        fmt_bold("Setup RenderDoc"),
        fmt_enabled(config.setup_render_doc)
    );
    info!(" - {}: {}",
          fmt_bold("Graphics Debug Level"),
          fmt_cyan(config.graphics_debug.to_string())
    );
    match &config.use_vulkan {
        Some(true) => {
            info!(" - {}:        {}", fmt_bold("Requested API"), fmt_highlight("Vulkan"));
        },
        Some(false) => {
            info!(" - {}:        {}", fmt_bold("Requested API"), fmt_cyan("OpenGL"));
        },
        None => {
            info!(" - {}:        {}", fmt_bold("Requested API"), fmt_success("Auto"));
        }
    }
    info!("");
    return Some(config);
}


#[cfg(feature = "client_side")]
fn client_main_loop(config: ProgramConfig) {
    
    //Init Client State: Main
    let mut resource_manager = ResourceManager::create();
    let mut game_state = GameState::new();
    let mut render_state = match GlobalRender::new(
        config.use_vulkan.unwrap_or(true),
        &config
    ) {
        Ok(v) => v,
        Err(e) => {
            
            //Tidy Shutdown on Error
            error!("Failed to create global render: {}", e);
            drop(game_state);
            drop(resource_manager);
            return;
        }
    };
    
    //Init Client State: Interactive
    let mut user_interface = UserInterface::new();
    //TODO: INIT AUDIO,NETWORK,ATLAS,REGISTRY,BACKGROUND_WORLD,GUI
    
    //FIXME: CONVERT TO POLL-EVENTS-FUNCTION-LOOP
    
    //Main Loop: Start
    info!("{}",fmt_bold("Starting Main Loop:"));
    info!("");
    
    //Main Loop: Timing Utility
    let mut last_update_was_tick = false;//FIXME: Needed?
    
    render_state.main_loop(
        &mut user_interface,
        |
            meta,
            start,
            api,
            force_shutdown,
            force_redraw
        | {
            //Update: Handle Client Input [Mutate GuiState & GameState] (Game First then GuiAsync)
            
            
            //If required: Process Server Tick [Mutate GameState] (Can be asynchronous)
            // every [1] requires [3x3] surrounding area, so process in 3x3 sections asynchronously
            // with common merge points, so 9 waves in total (max parallelism)
            let _ = ();
            
            //Always Update: Update Local Player [Ensure Smooth] &async& Update GUI [Ensure Smooth] [Mutate GameState]
            // current player view location & direction must be smoothly updated
            // sending this update to other players can be performed on server-tick instances
            // just ensure that it is marked dirty
            let _ = ();
            
            //If required: Render State Switch: [Complex Render Mutate] {cold path}
            let draw_frame = if let Some(state) = &meta.render_config.state_change {
                match api.perform_state_change(state) {
                    Ok(()) => (),
                    Err(()) => {
                        error!("Failed to perform state change");
                        return ControlFlow::Exit
                    }
                }
                meta.render_config.state_change = None;
                true
            }else{
                force_redraw || true //FIXME: Allow for FrameRateLimiting
            };
            
            if draw_frame {
                match api.draw_frame(
                    &(),
                    &(),
                    &meta,
                ) {
                    Ok(()) => (),
                    Err(()) => {
                        error!("Failed to draw frame");
                        return ControlFlow::Exit
                    }
                }
                meta.render_config.screen_shot_request = false;
            }
            //FIXME: POTENTIALLY ASYNC Send Updates to Others?
            
            //If required: Graphics Tick [force & not rate limited & not api limited]
            //               [DrawGui] [w/ DrawDebugData]
            // [PrepareFrame]                     [SubmitFrame]
            //               [GenWorld][DrawWorld]   //CAN GenWorld concurrently with prepare frame?
            // [SendServerUpdates]
            
            ControlFlow::Poll
        }
    );
    
    
    /*
    loop {
        let mut should_exit = false;
        //TODO: HANDLE GAME STATE SWITCHES

        //TODO: SERVER TICK (async with inputs?)
        should_exit |= render_state.poll_events(&mut user_interface);
        //TODO: PREPARE FRAME
        //TODO: GENERATE CHUNKS (async with)
        //TODO: SERVER SEND IO (/ RECV IO?) (async with)
        //TODO: DRAW GUI (async with)
        //TODO: SUBMIT FRAME

        if should_exit {
            break;
        }
    }*/

    //Execute Shutdown Sequence
    info!("{}",fmt_bold("Starting Shutdown Sequence:"));
    info!("");
    drop(user_interface);
    drop(render_state);
    drop(game_state);
}

fn server_main_loop(config: ProgramConfig)  {
    
    /*
    let mut resource_manager = ResourceManager::create();
    let mut game_state = GameState::new();
    let mut network_manager = ...;
    let mut game_registry = ...;
    
    let mut current_game = ...;??? load_game_from_memory_or_create_from_config(...);
    
    loop {
        break;
    }
    */
    
    let _ = config;
    
}
