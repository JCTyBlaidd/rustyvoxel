/// Represents the packed (block_id, metadata)
/// pair stored in a single `u32`
/// with 8 bits for metadata and 24 bits for id
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug, Default)]
pub struct BlockData {
    data: u32,
}

impl BlockData {
    #[inline(always)]
    pub fn new(id: u32, meta: u8) -> BlockData {
        debug_assert!(id < 16777216);
        BlockData {
            data: (id << 8) | (meta as u32),
        }
    }

    #[inline(always)]
    pub fn new_raw(raw: u32) -> BlockData {
        BlockData { data: raw }
    }

    #[inline(always)]
    pub fn get_meta(&self) -> u8 {
        self.data as u8
    }

    #[inline(always)]
    pub fn get_block_id(&self) -> u32 {
        self.data >> 8
    }

    #[inline(always)]
    pub fn set_meta(&mut self, meta: u8) {
        let clear_bits = self.data & !255;
        self.data = clear_bits | (meta as u32);
    }

    #[inline(always)]
    pub fn set_block_id(&mut self, id: u32) {
        debug_assert!(id < 16777216);
        let clear_bits = self.data & 255;
        let shifted_id = id << 8;
        self.data = clear_bits | shifted_id;
    }

    #[inline(always)]
    pub fn set(&mut self, id: u32, meta: u8) {
        debug_assert!(id < 16777216);
        self.data = (id << 8) | (meta as u32);
    }

    #[inline(always)]
    pub fn set_raw(&mut self, raw: u32) {
        self.data = raw;
    }

    #[inline(always)]
    pub fn get_raw(&mut self) -> u32 {
        self.data
    }
}

/// Represents a packed (r,g,b,s) light
/// information for the current lighting
/// of a location
/// `r` = red
/// `g` = green
/// `b` = blue
/// `s` = sunlight
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug, Default)]
pub struct LightData {
    data: u16,
}

impl LightData {
    #[inline(always)]
    pub fn new(r: u8, g: u8, b: u8, s: u8) -> LightData {
        debug_assert!(r < 16);
        debug_assert!(g < 16);
        debug_assert!(b < 16);
        debug_assert!(s < 16);
        LightData {
            data: ((r as u16) << 12) | ((g as u16) << 8) | ((b as u16) << 4) | (s as u16),
        }
    }

    #[inline(always)]
    pub fn r(&self) -> u8 {
        ((self.data >> 12) as u8) & 0xF
    }

    #[inline(always)]
    pub fn g(&self) -> u8 {
        ((self.data >> 8) as u8) & 0xF
    }

    #[inline(always)]
    pub fn b(&self) -> u8 {
        ((self.data >> 4) as u8) & 0xF
    }

    #[inline(always)]
    pub fn s(&self) -> u8 {
        (self.data as u8) & 0xF
    }

    #[inline(always)]
    pub fn set_r(&mut self, r: u8) {
        debug_assert!(r < 16);
        let reset = self.data & 0x0FFF;
        self.data = reset | ((r as u16) << 12);
    }

    #[inline(always)]
    pub fn set_g(&mut self, g: u8) {
        debug_assert!(g < 16);
        let reset = self.data & 0xF0FF;
        self.data = reset | ((g as u16) << 8);
    }

    #[inline(always)]
    pub fn set_b(&mut self, b: u8) {
        debug_assert!(b < 16);
        let reset = self.data & 0xFF0F;
        self.data = reset | ((b as u16) << 4);
    }

    #[inline(always)]
    pub fn set_s(&mut self, s: u8) {
        debug_assert!(s < 16);
        let reset = self.data & 0xFFF0;
        self.data = reset | (s as u16);
    }

    #[inline(always)]
    pub fn set(&mut self, r: u8, g: u8, b: u8, s: u8) {
        debug_assert!(r < 16);
        debug_assert!(g < 16);
        debug_assert!(b < 16);
        debug_assert!(s < 16);
        self.data = ((r as u16) << 12) | ((g as u16) << 8) | ((b as u16) << 4) | (s as u16);
    }
}

/// Enum that represents the concept
/// of a direction in 3D space in
/// a single axis, with each
/// direction having an associated
/// numerical index as well
#[derive(Copy, Clone, Hash, Debug)]
#[repr(u8)]
pub enum Direction {
    /// positive X, number = 0
    EAST = 0b00_10_01_01,
    /// negative X, number = 1
    WEST = 0b00_00_01_01,
    /// positive Y, number = 2
    UP = 0b00_01_10_01,
    /// negative Y, number = 3
    DOWN = 0b00_01_00_01,
    /// positive Z, number = 4
    NORTH = 0b00_01_01_10,
    /// negative Z, number = 5
    SOUTH = 0b00_01_01_00,
    //SELF  = 0b00_01_01_01, //ID num=6
}

impl Direction {
    #[inline(always)]
    pub fn x(&self) -> i64 {
        let bits = ((*self as i8) >> 4) & 3;
        (bits as i64) - 1
    }

    #[inline(always)]
    pub fn y(&self) -> i64 {
        let bits = ((*self as i8) >> 2) & 3;
        (bits as i64) - 1
    }

    #[inline(always)]
    pub fn z(&self) -> i64 {
        let bits = (*self as i8) & 3;
        (bits as i64) - 1
    }

    #[inline(always)]
    pub fn num(&self) -> u64 {
        let bits = (*self as u8) ^ 0b00_01_01_01;
        (bits.leading_zeros() as u64) - 2
    }

    pub fn iter() -> DirectionIter {
        DirectionIter {
            state: [
                Direction::EAST,
                Direction::WEST,
                Direction::UP,
                Direction::DOWN,
                Direction::NORTH,
                Direction::SOUTH,
            ],
        }
    }
}

#[derive(Copy, Clone)]
pub struct DirectionIter {
    state: [Direction; 6],
}

use std::slice::Iter;

impl DirectionIter {
    pub fn iter(&self) -> Iter<Direction> {
        self.state.iter()
    }
}
