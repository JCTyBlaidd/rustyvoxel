use hashbrown::HashMap;
use nalgebra_glm::{I64Vec2, U8Vec4};
use std::collections::VecDeque;

mod state;
pub use self::state::{BlockData, Direction, LightData};
use super::GameRegistry;

pub const SUB_CHUNK_1D: usize = 16;
pub const SUB_CHUNK_2D: usize = SUB_CHUNK_1D * SUB_CHUNK_1D;
pub const SUB_CHUNK_3D: usize = SUB_CHUNK_2D * SUB_CHUNK_1D;

#[cfg(feature = "client_side")]
#[derive(Clone)]
pub struct SubChunkDrawMetadata {
    //TODO: IMPLEMENT
}

#[cfg(not(feature = "client_side"))]
#[derive(Clone)]
pub type SubChunkDrawMetadata = ();

/// Represents a 16x16x16 cuboid of world-state
/// with associated metadata for optimization
#[derive(Clone)]
pub struct SubChunk {
    ///Block information pointer
    pub block_data: Box<[BlockData; SUB_CHUNK_3D]>,

    ///Light information pointer
    pub light_data: Box<[LightData; SUB_CHUNK_3D]>,

    ///Number of non-air blocks currently stored in this sub-chunk
    pub non_air_count: u32,

    ///Number of blocks that require random updates stored in this sub-chunk
    pub random_update_count: u32,

    ///Number of lighting locations that are not full skylight
    pub non_skylight_count: u32,

    ///Flag that the lighting has to be re-calculated for this sub-chunk
    pub light_dirty: bool,

    ///Bit-flags that indicate that blocks near the direction have
    /// been changed and therefore an update may be required
    pub dirty_directions: u8,

    ///Flag that indicates that the chunk visibility needs updating
    pub visibility_dirty: bool,

    ///Flag that indicates that the chunk drawing state needs updating
    pub draw_info_dirty: bool,

    ///Stores extra information for the render path so that
    /// the chunks can be displayed without any additional
    /// look-up overhead
    pub draw_information: SubChunkDrawMetadata,

    ///Bit-flags that indicate if there is a visibility path
    /// from face1 to face2 if (face1 * 6 + face2) is set
    pub visibility_metadata: u64,
}

impl SubChunk {
    pub fn update_visibility(&mut self, registry: &GameRegistry) {
        //Reset Visibility Metadata
        self.visibility_metadata = 0;

        //Short-cut for empty sub-chunks
        if self.non_air_count == 0 {
            self.visibility_metadata = 0xfffffffff;
            self.visibility_dirty = false;
            return;
        }

        //Run total flood-fill calculation
        let mut flood_queue = VecDeque::with_capacity(SUB_CHUNK_3D);
        let mut has_visited = vec![false; SUB_CHUNK_3D];
        for i in 0..SUB_CHUNK_3D {
            if !has_visited[i] {
                let mut connected_bits = 0u64;
                self.flood_fill(
                    registry,
                    &mut has_visited,
                    &mut connected_bits,
                    i,
                    &mut flood_queue,
                );

                //Update Visibility - bitwise
                for m in 0..6 {
                    let flag = (connected_bits >> m) & 1;
                    let mask = flag * 0b111111;
                    self.visibility_metadata |= (mask & connected_bits) << m;
                }
                /*
                for m in 0..6 {
                    let m_bit = (connected_bits >> m) & 1;
                    if m_bit != 0 {
                        for n in 0..6 {
                            let n_bit = (connected_bits >> n) & 1;
                            if n_bit != 0 {
                                self.set_visible(m,n);
                            }
                        }
                    }
                }*/
            }
        }

        //Mark as complete
        self.visibility_dirty = false;
    }

    fn flood_fill(
        &self,
        registry: &GameRegistry,
        visited: &mut Vec<bool>,
        connect_bits: &mut u64,
        start_idx: usize,
        flood_queue: &mut VecDeque<usize>,
    ) {
        flood_queue.push_back(start_idx);
        visited[start_idx] = true;

        let faces = Direction::iter();

        while let Some(pos) = flood_queue.pop_front() {
            let x = (pos % SUB_CHUNK_1D) as i64;
            let y = ((pos / SUB_CHUNK_1D) % SUB_CHUNK_1D) as i64;
            let z = (pos / SUB_CHUNK_2D) as i64;

            let block_raw = self.block_data[pos];
            if let Some(block) = registry.get_block(block_raw.get_block_id()) {
                if block.params.complete_opaque {
                    continue;
                }

                for face in faces.iter() {
                    //TODO: FINISH IMPLEMENTATION
                    //if !block.dynamic.is_direction_opaque(*face,&access) {
                    if true {
                        let new_x = x + face.x();
                        let new_y = y + face.y();
                        let new_z = z + face.z();

                        let max_val = new_x.max(new_y.max(new_z));
                        let min_val = new_x.min(new_y.min(new_z));
                        if min_val < 0 || max_val >= (SUB_CHUNK_1D as i64) {
                            *connect_bits |= 1 << face.num();
                        } else {
                            let new_pos = ((new_x as usize) * SUB_CHUNK_2D)
                                + ((new_y as usize) * SUB_CHUNK_1D)
                                + (new_z as usize);

                            if !visited[new_pos] {
                                flood_queue.push_back(new_pos);
                                visited[new_pos] = true;
                            }
                        }
                    }
                }
            }
        }
    }

    #[inline(always)]
    pub fn check_visible(&mut self, a: Direction, b: Direction) -> bool {
        let mask = 1 << (a.num() * 6 + b.num());
        (self.visibility_metadata & mask) != 0
    }

    #[inline(always)]
    fn set_visible(&mut self, a: u64, b: u64) {
        let mask_1 = 1 << (a * 6 + b);
        let mask_2 = 1 << (b * 6 + a);
        self.visibility_metadata |= mask_1 | mask_2;
    }

    //TODO: set_block_raw that updates all required counts

    //TODO: recalculate_all that calculates all from scratch {& similarly for Chunk}
}

#[derive(Clone)]
pub enum SubChunkState {
    ///The Sub-Chunk is loaded and contains some data
    Loaded(SubChunk),

    ///The Sub-Chunk is loaded and contains only sky-lit air.
    /// BlockData = {id: 0, meta: 0} = Air
    /// LightData = {r: 0,g: 0,b: 0,s: 16} = Full Sky
    /// This saves space for the common sky case
    Empty,

    ///The Sub-Chunk has not been loaded and contains
    /// Unknown Information
    Unloaded,
}

///
/// Represents a 16x16 vertical slice of the world
pub struct Chunk {
    pub sections: HashMap<i64, SubChunkState>,
    pub biome_data: Box<[u32; SUB_CHUNK_2D]>,
    pub height_data: Box<[i64; SUB_CHUNK_2D]>,
    pub needs_update: bool,
}

//TODO: ADD ADDITIONAL STATE?
pub struct World {
    chunk_map: HashMap<I64Vec2, Chunk>,
}
