pub mod registry;
pub mod world;

use hashbrown::HashMap;

pub use self::registry::{Block, GameRegistry, Item, Registry, RegistryEntry};
pub use self::world::{BlockData, Chunk, Direction, LightData, SubChunk, World};

/// Global Game State
pub struct GameState {
    pub registry: GameRegistry,
    pub dimensions: HashMap<u32, World>,
}

impl GameState {
    pub fn new() -> GameState {
        GameState {
            registry: GameRegistry::new(),
            dimensions: HashMap::new(),
        }
    }
}
