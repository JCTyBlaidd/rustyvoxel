use super::super::world::Direction;
use ncollide3d::bounding_volume::AABB;
use ncollide3d::math::Point;

pub struct BlockAccess {}

/// Conditional Data about the block
///  that depends on information about the block
///  that requires arbitrary BlockAccess Information
pub trait DynBlock: Send + Sync {
    fn is_direction_opaque(&self, dir: Direction, access: &BlockAccess) -> bool;
    fn on_random_update(&self, access: &BlockAccess);
}

impl DynBlock for DefaultDynBlock {
    fn is_direction_opaque(&self, dir: Direction, access: &BlockAccess) -> bool {
        self.opaque[dir.num() as usize]
    }
    fn on_random_update(&self, access: &BlockAccess) {
        //NO OP
    }
}

pub struct DefaultDynBlock {
    opaque: [bool; 6],
}

impl Default for DefaultDynBlock {
    fn default() -> Self {
        DefaultDynBlock {
            opaque: [false, false, false, false, false, false],
        }
    }
}

/// Data about the block
///  that requires no additional
///  state, and such is stored
///  for easy access
pub struct ParamBlock {
    pub bounds: AABB<f32>,
    pub complete_opaque: bool,
    pub has_random_updates: bool,
    pub has_entity: bool,
    pub friction: f32,
    //TODO: ADD MORE INFO
    // ..
    // ..
    // All Edges {
    //      AirAnimate: Yes | Available | Fixed  (Available means that the space can be used even though the object is fixed and will not be animated)
    //      //Maybe Yes / No with custom animation code handled: store in a bitset? or make dynamic Option<bitset> with dynamic fallback??
    // ..
    //          This animates leaves/grass/... each point is mutated by some unknown continuous function so that nearby values are aligned
    //          // Max Displacement +0.1,+0.1,+0.1
    // ..
    //  WaterAnimate:
    //      // verticle displacement inside area: guaranteed ??? (interaction with air animate??)
    //  ..
    //      WaterMask(FluidMask)
    //  ..
    //}
    
}

impl Default for ParamBlock {
    fn default() -> Self {
        ParamBlock {
            bounds: AABB::new(Point::new(0.0, 0.0, 0.0), Point::new(1.0, 1.0, 1.0)),
            complete_opaque: false,
            has_random_updates: false,
            has_entity: false,
            friction: 0.0f32,
        }
    }
}
