use self::block::{DefaultDynBlock, DynBlock, ParamBlock};
use hashbrown::{hash_map::Entry, HashMap};

pub mod block;
pub mod item;

pub type Block = RegistryEntry<block::ParamBlock, block::DynBlock>;
pub type Item = RegistryEntry<item::ParamItem, item::DynItem>;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum RegistryError {
    IdInUse,
    NameInUse,
    InvalidId,
}

pub struct RegistryEntry<PARAM: Send + Sync, DYNAMIC: ?Sized + Send + Sync> {
    pub id: u32,
    pub name: String,
    pub dynamic: Box<DYNAMIC>,
    pub params: PARAM,
}

impl<PARAM: Send + Sync, DYNAMIC: ?Sized + Send + Sync> RegistryEntry<PARAM, DYNAMIC> {
    pub fn new(params: PARAM, dynamic: Box<DYNAMIC>) -> RegistryEntry<PARAM, DYNAMIC> {
        RegistryEntry {
            id: 0,
            name: String::new(),
            dynamic,
            params,
        }
    }
}

pub struct Registry<PARAM: Send + Sync, DYNAMIC: ?Sized + Send + Sync> {
    id_map: HashMap<u32, RegistryEntry<PARAM, DYNAMIC>>,
    name_map: HashMap<String, u32>,
    max_id: u32,
}

impl<PARAM: Send + Sync, DYNAMIC: ?Sized + Send + Sync> Registry<PARAM, DYNAMIC> {
    #[must_use]
    pub fn new() -> Registry<PARAM, DYNAMIC> {
        Registry {
            id_map: HashMap::new(),
            name_map: HashMap::new(),
            max_id: 0,
        }
    }

    #[must_use]
    pub fn next_free_id_after(&self, id: u32) -> Option<u32> {
        let range = id..(1 << 24);
        for value in range {
            if !self.id_map.contains_key(&value) {
                return Some(value);
            }
        }
        None
    }

    #[must_use]
    pub fn next_id_section(&self) -> Option<u32> {
        const BLOCK_SIZE: u32 = 4096;
        let next_block = (self.max_id + BLOCK_SIZE - 1) / BLOCK_SIZE;
        let next_block_id = next_block * BLOCK_SIZE;
        if next_block_id >= (1 << 24) {
            None
        } else {
            Some(next_block_id)
        }
    }

    #[must_use]
    pub fn register_at(
        &mut self,
        entry: RegistryEntry<PARAM, DYNAMIC>,
    ) -> Result<(), RegistryError> {
        if entry.id >= (1 << 24) {
            return Err(RegistryError::InvalidId);
        }
        let id = entry.id;
        let name = entry.name.clone();
        match self.id_map.entry(id) {
            Entry::Occupied(_) => {
                return Err(RegistryError::IdInUse);
            }
            Entry::Vacant(vac) => {
                vac.insert(entry);
            }
        }
        match self.name_map.entry(name) {
            Entry::Occupied(_) => {
                self.id_map.remove(&id);
                return Err(RegistryError::NameInUse);
            }
            Entry::Vacant(vac) => {
                vac.insert(id);
            }
        }
        self.max_id = self.max_id.max(id);
        Ok(())
    }

    #[inline]
    #[must_use]
    pub fn get_entry(&self, idx: u32) -> Option<&RegistryEntry<PARAM, DYNAMIC>> {
        self.id_map.get(&idx)
    }

    #[inline]
    #[must_use]
    pub fn get_entry_id(&self, name: &str) -> Option<u32> {
        self.name_map.get(name).map(|v| *v)
    }

    #[inline]
    #[must_use]
    pub fn get_entry_from_name(&self, name: &str) -> Option<&RegistryEntry<PARAM, DYNAMIC>> {
        self.get_entry_id(name).and_then(|id| self.get_entry(id))
    }
}

pub struct GameRegistry {
    block_registry: Registry<ParamBlock, DynBlock>,
}

impl GameRegistry {
    pub fn new() -> GameRegistry {
        let mut registry = GameRegistry {
            block_registry: Registry::new(),
        };

        //Register AIR
        let mut air = Block::new(ParamBlock::default(), Box::new(DefaultDynBlock::default()));
        //FIXME: SET AIR BOUNDS
        air.name = "core::air".to_owned();
        air.id = 0u32;
        registry.block_registry.register_at(air).unwrap();

        registry
    }

    #[inline]
    #[must_use]
    pub fn get_block(&self, idx: u32) -> Option<&Block> {
        self.block_registry.get_entry(idx)
    }

    #[inline]
    #[must_use]
    pub fn get_block_id(&self, name: &str) -> Option<u32> {
        self.block_registry.get_entry_id(name)
    }

    #[inline]
    #[must_use]
    pub fn get_block_from_name(&self, name: &str) -> Option<&Block> {
        self.block_registry.get_entry_from_name(name)
    }
}
