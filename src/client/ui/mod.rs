use super::render::{
	UserEventHandle,
	GlobalRenderConfig,
};
use winit::{
	event::{
		KeyboardInput,
		MouseScrollDelta,
		ElementState,
		MouseButton
	},
	dpi::LogicalPosition,
};

pub mod world_input;
pub mod screen;
pub mod widget;
pub mod raster;

pub use screen::Screen;
pub use world_input::WorldInput;

pub struct UserInterface {
	screen_stack: Vec<Screen>,
	use_cursor: Option<usize>,
	world_input: WorldInput,
}

impl UserInterface {
	pub fn new() -> UserInterface {
		UserInterface {
			screen_stack: Vec::new(),
			use_cursor: None,
			world_input: WorldInput::new()
		}
	}
	pub fn push_screen(&mut self, screen: Screen) {
		if screen.use_cursor {
			self.use_cursor = Some(self.screen_stack.len());
		}
		self.screen_stack.push(screen);
	}
	pub fn pop_screen(&mut self) -> Option<Screen> {
		let screen = self.screen_stack.pop();
		if self.use_cursor == Some(self.screen_stack.len()) {
			self.fix_use_cursor();
		}
		screen
	}
	pub fn mutate_screens<F: Fn(&mut Vec<Screen>) -> R,R>(
		&mut self,
		f: F
	) -> R {
		let r = f(&mut self.screen_stack);
		self.fix_use_cursor();
		r
	}
	fn fix_use_cursor(&mut self) {
		self.screen_stack.iter().enumerate()
			.filter_map(|(idx,screen)| {
			if screen.use_cursor {
				Some(idx)
			}else{
				None
			}
		}).next_back();
	}
}

impl UserEventHandle for UserInterface {
	fn on_window_char(&mut self, config: &mut GlobalRenderConfig, character: char) {
		for screen in self.screen_stack.iter_mut().rev() {
			if screen.on_char_press(config,character) {
				return;
			}
		}
	}
	
	fn on_window_key(&mut self, config: &mut GlobalRenderConfig, key: &KeyboardInput) {
		for screen in self.screen_stack.iter_mut().rev() {
			if screen.on_key_press(config,key) {
				return;
			}
		}
		self.world_input.on_key_press(config, key);
	}
	
	fn on_window_cursor_move(&mut self, config: &mut GlobalRenderConfig, pos: &LogicalPosition) {
		if let Some(idx) = self.use_cursor {
			let screen = &mut self.screen_stack[idx];
			screen.on_cursor_move(config,pos);
		}
	}
	
	fn on_window_cursor_enter(&mut self, config: &mut GlobalRenderConfig) {
		if let Some(idx) = self.use_cursor {
			let screen = &mut self.screen_stack[idx];
			screen.on_cursor_enter(config);
		}
	}
	
	fn on_window_cursor_leave(&mut self, config: &mut GlobalRenderConfig) {
		if let Some(idx) = self.use_cursor {
			let screen = &mut self.screen_stack[idx];
			screen.on_cursor_leave(config);
		}
	}
	
	fn on_window_cursor_scroll(&mut self, config: &mut GlobalRenderConfig, delta: &MouseScrollDelta) {
		for screen in self.screen_stack.iter_mut().rev() {
			if screen.on_cursor_scroll(config,delta) {
				return;
			}
		}
		self.world_input.on_cursor_scroll(config,delta);
	}
	
	fn on_window_cursor_button(&mut self, config: &mut GlobalRenderConfig, state: ElementState, button: MouseButton) {
		for screen in self.screen_stack.iter_mut().rev() {
			if screen.on_cursor_button(config,state,button) {
				return;
			}
		}
		self.world_input.on_cursor_button(config,state,button);
	}
	
	fn on_window_raw_delta(&mut self, config: &mut GlobalRenderConfig, delta: (f64, f64)) {
		if self.use_cursor.is_none() {
			self.world_input.on_cursor_delta(config, delta);
		}
	}
}
