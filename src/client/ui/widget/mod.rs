pub mod button;


pub trait Widget {
	fn get_width(&self, extern_width: Option<f32>) -> f32;
	fn get_height(&self, extern_width: Option<f32>) -> f32;
}