use super::GlobalRenderConfig;
use winit::{
	event::{
		KeyboardInput,
		MouseScrollDelta,
		ElementState,
		MouseButton,
	}
};

pub struct WorldInput {
	queued_delta: (f64,f64),
}

impl WorldInput {
	
	pub(super) fn new() -> WorldInput {
		WorldInput {
			queued_delta: (0.0,0.0)
		}
	}
	
	pub(super) fn on_key_press(&mut self, config: &mut GlobalRenderConfig, key: &KeyboardInput) {
		//TODO: USE FOR INTERACTION
	}
	
	pub(super) fn on_cursor_scroll(&mut self, config: &mut GlobalRenderConfig, delta: &MouseScrollDelta) {
		//TODO: USE FOR SCROLL INTERACTION
	}
	
	pub(super) fn on_cursor_button(&mut self, config: &mut GlobalRenderConfig, state: ElementState, button: MouseButton) {
		//TODO: USE FOR BUTTON INTERACTION
	}
	
	pub(super) fn on_cursor_delta(&mut self, _: &mut GlobalRenderConfig, delta: (f64, f64)) {
		self.queued_delta.0 += delta.0;
		self.queued_delta.1 += delta.1;
	}
	
	pub fn get_mouse_delta(&mut self) -> (f64,f64) {
		let delta = self.queued_delta;
		self.queued_delta = (0.0,0.0);
		delta
	}
}