use super::*;

pub struct Screen {
	pub use_cursor: bool,
}

impl Screen {
	pub fn on_cursor_move(&mut self, config: &mut GlobalRenderConfig, pos: &LogicalPosition) {
	
	}
	pub fn on_cursor_enter(&mut self, config: &mut GlobalRenderConfig) {
	
	}
	pub fn on_cursor_leave(&mut self, config: &mut GlobalRenderConfig) {
	
	}
	pub fn on_char_press(&mut self, config: &mut GlobalRenderConfig, character: char) -> bool {
		false
	}
	pub fn on_key_press(&mut self, config: &mut GlobalRenderConfig, key: &KeyboardInput) -> bool {
		false
	}
	pub fn on_cursor_scroll(&mut self, config: &mut GlobalRenderConfig, delta: &MouseScrollDelta) -> bool {
		false
	}
	pub fn on_cursor_button(&mut self, config: &mut GlobalRenderConfig, state: ElementState, button: MouseButton) -> bool {
		false
	}
}