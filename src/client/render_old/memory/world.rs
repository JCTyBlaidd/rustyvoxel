use super::{GpuBufferAlloc, GpuBufferCapabilities, GpuMemoryType};

use parking_lot::Mutex;
use std::ptr::NonNull;

/// Manage a single page allocation
///  alloc = allocation
///  usage = is section in use (0=free,255=IN_USE,else=FREE_COUNTDOWN)
struct WorldChunkPage {
    alloc: GpuBufferAlloc,
    usage: Vec<u8>,
    free_count: usize,
}

impl WorldChunkPage {
    fn new(alloc: GpuBufferAlloc, section_size: u64) -> WorldChunkPage {
        let usage = vec![0; alloc.size / section_size];
        WorldChunkPage {
            alloc,
            usage,
            free_count: usage.len()
        }
    }
    fn allocate_offset(&mut self, sec_num: usize) -> Option<usize> {
        if self.free_count < sec_num {
            //Skip Search
            return None;
        }
        let mut found_free = 0;
        let mut found_offset = 0;
        for (idx,val) in self.usage.iter().enumerate() {
            if *val == 0 {
                found_free += 1;
                if found_free == sec_num {
                    break;
                }
            }else {
                found_offset = idx + 1;
                found_free = 0;
            }
        }
        if found_free == sec_num {
            debug_assert!((found_offset + sec_num) < self.usage.len());
            
            //Update current usage stats
            self.usage_count -= sec_num;
            for idx in found_offset..(found_offset + sec_num) {
                //We already know the range is valid
                unsafe {
                    *self.usage.get_unchecked_mut(idx) = 255;
                }
            }
            Some(found_offset)
        }else {
            None
        }
    }
    unsafe fn free_offset(&mut self, sec_offset: usize, sec_num: usize, tick: u8) {
    
    }
}

/// Manage a local staging page allocation
///  no usages must be on free_countdown
pub struct WorldStagingPage {
    chunk: WorldChunkPage,
    mem_map: Option<NonNull<u8>>,
}

pub struct WorldChunkAllocator {
    chunks: Vec<WorldChunkPage>,
    staging: Vec<WorldStagingPage>,
}

//TODO: ALLOCATE_STREAMING
