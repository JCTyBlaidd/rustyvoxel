use nalgebra_glm::{U16Vec2, U8Vec2, U8Vec4, Vec3, Quat};

/// World Data Structure
/// Size = 32 Bytes
/// Alignment = 4 Bytes
/// `position` = (x,y,z) from chunk
/// `rotation` = packed quaternion rotation
/// `colour` = (r,g,b,a) colour change at vertex
/// `light` = (r,g,b,sky) lighting at vertex
/// `tex_coord` = (u,v) offset from texture
/// `tex_index` = (array_id,tex_id) texture location
/// `anim_count` = Number of animation frames
#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct WorldData {
	position: Vec3,
	rotation: U8Vec4,
	colour: U8Vec4,
	light: U8Vec4,
	tex_coord: U8Vec2,
	tex_index: U16Vec2,
	anim_count: u16,
}

#[cfg(test)]
mod test {
	use std::mem;
	fn test_size_align() {
		assert_eq!(mem::size_of::<super::WorldData>(), 32);
		assert_eq!(mem::align_of::<super::WorldData>(), 4);
	}
}
