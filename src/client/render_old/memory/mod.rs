use super::GraphicsAPI;

pub mod stream;
pub mod world;
pub mod util;

pub use self::util::*;

/// Memory usage for
///  GPU allocation
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub enum GpuMemoryType {
    GPUDedicated,  //Device Local (dedicated)
    CPUDedicated,  //Cpu Local (visible and coherent)
    TransferToGPU, //(visible, maybe coherent)
    TransferToCPU, //(visible, hopefully cached)
}

/// Buffer Capability Request
///  contains buffer usage
///  and buffer access sharing
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub struct GpuBufferCapabilities {
    pub transfer_src: bool,
    pub transfer_dst: bool,
    pub index_buf: bool,
    pub indirect_buf: bool,
    pub vertex_buf: bool,
    pub storage_buf: bool,
    pub storage_tex: bool,
    pub uniform_buf: bool,
    pub uniform_tex: bool,
    pub shared_access: bool,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub struct GpuBufferAlloc {
    pub size: u64,
    pub ident_mem: u64,
    pub ident_buf: u64,
    pub coherent: bool,
}

//FIXME: TODO IMAGE ALLOCATION
