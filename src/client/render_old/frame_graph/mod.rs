use nalgebra_glm::{vec2, vec3, Vec2, Vec3};

pub mod attachment;

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub enum FramePassType {
    Compute,
    Graphics,
}

pub struct FramePass {}

pub struct FrameGraphBuilder {}

impl FrameGraphBuilder {
    pub fn new() -> FrameGraphBuilder {
        unimplemented!()
    }

    pub fn new_pass(&mut self, name: &str, pass: FramePassType) -> &mut FramePass {
        unimplemented!()
    }

    pub fn set_final_output(&mut self, name: &str) {
        unimplemented!()
    }
}

#[repr(u32)]
enum RenderGraphQueueFlagBits {
    GraphicsBit = 1 << 0,
    ComputeBit = 1 << 1,
    AsyncComputeBit = 1 << 2,
    AsyncGraphicsBit = 1 << 3,
}
