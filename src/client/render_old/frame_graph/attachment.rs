use ash::vk;
use nalgebra_glm::{vec3, Vec3};
use std::mem::transmute;

/// Represents How the input size
///  is scaled relative to the swap size
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
#[repr(u8)]
pub enum SizeType {
    Absolute,
    SwapChainRelative,
    InputRelative,
}

/// Represents the formats that may or may not be supported
///  for the purposes of rendering state
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
#[repr(i32)]
pub enum ImageFormat {
    Undefined = unsafe { transmute(vk::Format::UNDEFINED) },

    UNormR8 = unsafe { transmute(vk::Format::R8_UNORM) },
    UNormR8G8 = unsafe { transmute(vk::Format::R8G8_UNORM) },
    UNormR8G8B8 = unsafe { transmute(vk::Format::R8G8B8_UNORM) },
    UNormR8G8B8A8 = unsafe { transmute(vk::Format::R8G8B8A8_UNORM) },

    SrgbR8G8B8A8 = unsafe { transmute(vk::Format::R8G8B8A8_SRGB) },

    SFloatR16 = unsafe { transmute(vk::Format::R16_SFLOAT) },
    SFloatR16G16 = unsafe { transmute(vk::Format::R16G16_SFLOAT) },
    SFloatR16G16B16 = unsafe { transmute(vk::Format::R16G16B16_SFLOAT) },
    SFloatR16G16B16A16 = unsafe { transmute(vk::Format::R16G16B16A16_SFLOAT) },

    SFloatR32 = unsafe { transmute(vk::Format::R32_SFLOAT) },
    SFloatR32G32 = unsafe { transmute(vk::Format::R32G32_SFLOAT) },
    SFloatR32G32B32 = unsafe { transmute(vk::Format::R32G32B32_SFLOAT) },
    SFloatR32G32B32A32 = unsafe { transmute(vk::Format::R32G32B32A32_SFLOAT) },

    SFloatR64 = unsafe { transmute(vk::Format::R64_SFLOAT) },
    SFloatR64G64 = unsafe { transmute(vk::Format::R64G64_SFLOAT) },
    SFloatR64G64B64 = unsafe { transmute(vk::Format::R64G64B64_SFLOAT) },
    SFloatR64G64B64A64 = unsafe { transmute(vk::Format::R64G64B64A64_SFLOAT) },

    DepthUNormD16 = unsafe { transmute(vk::Format::D16_UNORM) },
    DepthUNormD24AndUIntS8 = unsafe { transmute(vk::Format::D24_UNORM_S8_UINT) },
    DepthSFloatD32 = unsafe { transmute(vk::Format::D32_SFLOAT) },
    DepthSFloatD32AndUIntS8 = unsafe { transmute(vk::Format::D32_SFLOAT_S8_UINT) },
}

impl ImageFormat {
    pub fn as_vk_format(&self) -> vk::Format {
        vk::Format::from_raw(*self as i32)
    }
    pub fn get_fallback_list(&self) -> Vec<ImageFormat> {
        use self::ImageFormat::*;
        let slice: &[ImageFormat] = match *self {
            Undefined => panic!("Attempted to get fallback list for undefined"),
            UNormR8 => &[UNormR8G8, UNormR8G8B8, UNormR8G8B8A8, SFloatR32],
            UNormR8G8 => &[UNormR8G8B8, UNormR8G8B8A8, SFloatR32G32],
            UNormR8G8B8 => &[UNormR8G8B8A8, SFloatR32G32B32],
            UNormR8G8B8A8 => &[],
            SrgbR8G8B8A8 => &[],
            SFloatR16 => &[SFloatR16G16, SFloatR16G16B16, SFloatR16G16B16A16, SFloatR32],
            SFloatR16G16 => &[SFloatR16G16B16, SFloatR16G16B16A16, SFloatR32G32],
            SFloatR16G16B16 => &[SFloatR16G16B16A16, SFloatR32G32B32, SFloatR32G32B32A32],
            SFloatR16G16B16A16 => &[SFloatR32G32B32A32],
            SFloatR32 => &[SFloatR32G32, SFloatR32G32B32, SFloatR32G32B32A32],
            SFloatR32G32 => &[SFloatR32G32B32, SFloatR32G32B32A32],
            SFloatR32G32B32 => &[SFloatR32G32B32A32],
            SFloatR32G32B32A32 => &[],
            SFloatR64 => &[SFloatR64G64, SFloatR64G64B64, SFloatR64G64B64A64],
            SFloatR64G64 => &[SFloatR64G64B64, SFloatR64G64B64A64],
            SFloatR64G64B64 => &[SFloatR64G64B64A64],
            SFloatR64G64B64A64 => &[],
            DepthUNormD16 => &[
                DepthUNormD24AndUIntS8,
                DepthSFloatD32,
                DepthSFloatD32AndUIntS8,
            ],
            DepthUNormD24AndUIntS8 => &[DepthSFloatD32AndUIntS8],
            DepthSFloatD32 => &[DepthSFloatD32AndUIntS8],
            DepthSFloatD32AndUIntS8 => &[],
        };
        slice.to_vec()
    }
}

/// Represents an Attachment
///  for the input.
#[derive(Clone, PartialEq, Debug)]
pub struct AttachmentInfo {
    pub size: Vec3,
    pub size_type: SizeType,
    pub format: ImageFormat,
    pub samples: u32,
    pub levels: u32,
    pub layers: u32,
    pub persistent: bool,
}

impl Default for AttachmentInfo {
    fn default() -> Self {
        AttachmentInfo {
            size: vec3(1.0, 1.0, 0.0),
            size_type: SizeType::SwapChainRelative,
            format: ImageFormat::Undefined,
            samples: 1,
            levels: 1,
            layers: 1,
            persistent: true,
        }
    }
}

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct BufferInfo {
    size: u64,
    usage: vk::BufferUsageFlags,
    persistent: bool,
}
