#[derive(Copy, Clone, Eq, PartialEq)]
pub struct TextureAtlasIcon {
    array_index: usize,
    texture_index: u32,
    animation_count: u32,
    icon_size: u32,
}

pub struct TextureAtlasBuilder {}

struct Texture(pub u64);

pub struct TextureAtlas {
    array: Vec<(Texture, u32)>,
}

impl TextureAtlas {}
