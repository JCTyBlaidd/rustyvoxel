use super::{APIWindow, GpuBufferAlloc, GpuBufferCapabilities, GpuMemoryType, GraphicsAPI};
use glutin::GlWindow;
use std::ptr::NonNull;
use winit::Window;

pub struct GLRender {}

impl GraphicsAPI for GLRender {
    fn acquire_frame(&mut self) {
        unimplemented!()
    }

    fn submit_frame(&mut self) {
        unimplemented!()
    }

    unsafe fn allocate_buffer_page(
        &self,
        mem: GpuMemoryType,
        caps: &GpuBufferCapabilities,
    ) -> Option<GpuBufferAlloc> {
        unimplemented!()
    }

    unsafe fn free_buffer_page(&self, mem: GpuMemoryType, alloc: GpuBufferAlloc) {
        unimplemented!()
    }

    unsafe fn map_memory(&self, alloc: GpuBufferAlloc) -> NonNull<u8> {
        unimplemented!()
    }

    unsafe fn un_map_memory(&self, alloc: GpuBufferAlloc) {
        unimplemented!()
    }

    unsafe fn flush_non_coherent_ranges(&self, alloc: GpuBufferAlloc, offset: u64, size: u64) {
        unimplemented!()
    }

    unsafe fn invalidate_non_coherent_ranges(&self, alloc: GpuBufferAlloc, offset: u64, size: u64) {
        unimplemented!()
    }

    fn start_state_change(&mut self) {
        unimplemented!()
    }

    fn stop_state_change(&mut self, window: &APIWindow) {
        unimplemented!()
    }

    fn is_sync_supported(&self, state: super::StateSync) -> bool {
        unimplemented!()
    }

    fn get_sync_state(&self) -> super::StateSync {
        unimplemented!()
    }

    fn set_sync_state(&mut self, state: super::StateSync) {
        unimplemented!()
    }
}

pub fn load_open_gl(window: Window) -> (GlWindow, GLRender) {
    (unimplemented!(), unreachable!())
}
