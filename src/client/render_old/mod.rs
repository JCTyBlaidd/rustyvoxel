use crate::prelude::*;
use crate::util::{PerSecondTimer, ProgramConfig};

use glutin::GlWindow;
use std::{ptr::NonNull,thread,time::{Duration,Instant}};
use winit::dpi::LogicalSize;
use winit::{Event, EventsLoop, VirtualKeyCode, Window, WindowEvent, DeviceEvent};

mod atlas;
mod frame_graph;
mod memory;
mod open_gl;
mod vulkan;

use self::memory::{GpuBufferAlloc, GpuBufferCapabilities, GpuMemoryType};
use self::open_gl::load_open_gl;
use self::vulkan::{is_vulkan_supported, load_vulkan};

pub trait GraphicsAPI {
    //Frame Submission [Externally Synchronised]
    fn acquire_frame(&mut self);
    fn submit_frame(&mut self);

    //Memory Management - Buffer [Internally Synchronised]
    unsafe fn allocate_buffer_page(
        &self,
        mem: GpuMemoryType,
        caps: &GpuBufferCapabilities,
    ) -> Option<GpuBufferAlloc>;
    unsafe fn free_buffer_page(&self, mem: GpuMemoryType, alloc: GpuBufferAlloc);
    unsafe fn map_memory(&self, alloc: GpuBufferAlloc) -> NonNull<u8>;
    unsafe fn un_map_memory(&self, alloc: GpuBufferAlloc);
    unsafe fn flush_non_coherent_ranges(&self, alloc: GpuBufferAlloc, offset: u64, size: u64);
    unsafe fn invalidate_non_coherent_ranges(&self, alloc: GpuBufferAlloc, offset: u64, size: u64);

    //Frame Graph Management - [TODO: IMPL]
    //FIXME: ADD Management for Render Graph?

    //State Change [Externally Synchronised]
    fn start_state_change(&mut self);
    fn stop_state_change(&mut self, window: &APIWindow); //FIXME: Pass Optional Render Graph

    //V-Sync Configuration [Externally Synchronised]
    fn is_sync_supported(&self, state: StateSync) -> bool;
    fn get_sync_state(&self) -> StateSync;
    fn set_sync_state(&mut self, state: StateSync);
}

enum APIWindow {
    ContextFree(Window),
    GLContext(GlWindow),
}

impl APIWindow {
    fn get_window(&self) -> &Window {
        match self {
            APIWindow::ContextFree(window) => window,
            APIWindow::GLContext(window) => window.window(),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum StateSync {
    Disabled = 0,
    TripleBuffered = 1,
    Enabled = 2,
    Relaxed = 3,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum StateScreen {
    Windowed,
    BorderLess,
    FullScreen,
}

#[derive(Clone, Debug)]
pub struct StateChange {
    reload_resources: bool,
    update_graphics: (),
    update_sync: Option<StateSync>,
    update_screen: Option<StateScreen>,
}

impl Default for StateChange {
    fn default() -> Self {
        StateChange {
            reload_resources: false,
            update_graphics: (),
            update_sync: None,
            update_screen: None,
        }
    }
}

#[derive(Clone, Debug)]
pub struct GlobalRenderConfig {
    pub field_of_view: f32,
    pub draw_distance: u32,

    pub screen_shot_request: bool,
    pub state_change: Option<StateChange>,
}

impl GlobalRenderConfig {
    pub fn get_state_change(&mut self) -> &mut StateChange {
        self.state_change.get_or_insert_with(|| Default::default())
    }
}

pub struct GlobalRender {
    api: Box<dyn GraphicsAPI>,
    events: EventsLoop,
    window: APIWindow,

    //Frame Rate Limiting & Util
    frame_rate: PerSecondTimer,
    target_frame_rate: u32,
    last_frame_time: Instant,
    windowed_size: LogicalSize,
    window_state: StateScreen,
    window_focused: bool,

    pub config: GlobalRenderConfig,
}

impl GlobalRender {
    pub fn new(use_vulkan: bool, config: &ProgramConfig) -> GlobalRender {
        let events = EventsLoop::new();
        let builder = winit::WindowBuilder::new()
            .with_min_dimensions(LogicalSize::new(50.0, 50.0))
            .with_title("Rusty Voxel")
            .with_resizable(true)
            .with_maximized(true);
        let raw_window = builder.build(&events).unwrap_or_else(|fail| {
            error!("Failed to create window: {}", fail);
            panic!("Window Creation Failed")
        });

        let vulkan_load = if use_vulkan {
            is_vulkan_supported()
        } else {
            None
        };
        let size = raw_window
            .get_inner_size()
            .unwrap_or(LogicalSize::new(50.0, 50.0));

        let (window, api): (APIWindow, Box<dyn GraphicsAPI>) =
            if let Some(vulkan_entry) = vulkan_load {
                //Vulkan
                info!("Created {} Window", fmt_highlight("Vulkan"));
                if let Some(vulkan_context) = load_vulkan(&raw_window, vulkan_entry, config) {
                    info!("Created {} Instance", fmt_highlight("Vulkan"));
                    (APIWindow::ContextFree(raw_window), Box::new(vulkan_context))
                } else {
                    warn!("Failed to Create Vulkan Instance - Fallback");
                    let (gl_window, gl_instance) = load_open_gl(raw_window);
                    info!("Created {} Context", fmt_highlight("OpenGL"));
                    (APIWindow::GLContext(gl_window), Box::new(gl_instance))
                }
            } else {
                //OpenGL
                info!("Created {} Window", fmt_highlight("OpenGL"));
                let (gl_window, gl_instance) = load_open_gl(raw_window);
                info!("Created {} Context", fmt_highlight("OpenGL"));
                (APIWindow::GLContext(gl_window), Box::new(gl_instance))
            };

        GlobalRender {
            api,
            events,
            window,

            frame_rate: PerSecondTimer::new(32),
            target_frame_rate: 60,
            last_frame_time: Instant::now(),
            windowed_size: size,
            window_state: StateScreen::Windowed,
            window_focused: true,

            config: GlobalRenderConfig {
                field_of_view: 100.0,
                draw_distance: 16,

                screen_shot_request: false,
                state_change: None,
            },
        }
    }

    fn handle_state_change(&mut self, change: StateChange) {
        self.api.start_state_change();

        let handle = self.window.get_window();
        let monitor = handle.get_current_monitor();

        //Cache Window Size if Windowed
        if self.window_state == StateScreen::Windowed {
            if let Some(inner) = handle.get_inner_size() {
                self.windowed_size = inner;
            }
        }

        //Update Screen Mode
        match change.update_screen {
            None => {}
            Some(StateScreen::Windowed) => {
                handle.set_decorations(true);
                handle.set_inner_size(self.windowed_size);
            }
            Some(StateScreen::BorderLess) => {
                handle.set_decorations(false);
                handle.set_maximized(true);
            }
            Some(StateScreen::FullScreen) => {
                handle.set_fullscreen(Some(monitor));
            }
        }

        //Update VSync Mode
        match change.update_sync {
            None => {}
            Some(state) => {
                self.api.set_sync_state(state);
            }
        }

        //Reload Resources
        if change.reload_resources {
            warn!("Resource Reloading NYI!");
            //TODO: IMPLEMENT
        }

        //Finish state change & update api resources
        self.api.stop_state_change(&self.window);
    }

    fn await_fps_target(&mut self) {
        let current_frame_time = Instant::now();
        if self.target_frame_rate != !0 {
            let target_nano = 1_000_000_000 / self.target_frame_rate;
            let delta = current_frame_time.duration_since(self.last_frame_time);

            //Simplified delta <= target_nano given target_nano <= 1 second
            if delta.subsec_nanos() <= target_nano && delta.as_secs() < 1 {
                let nano_wait = target_nano - delta.subsec_nanos();
                thread::sleep(Duration::from_nanos(nano_wait as u64));
            }
        }
        self.last_frame_time = current_frame_time;
    }

    /// Consume standard inputs & forward others to closure
    /// Return if shutdown is required
    #[must_use]
    pub fn poll_events<F1, F2>(
        &mut self,
        mut f_window: F1,
        mut f_device: F2,
    ) -> bool
        where F1: FnMut(WindowEvent, &mut GlobalRenderConfig),
              F2: FnMut(DeviceEvent, &mut GlobalRenderConfig)
    {
        let mut config = &mut self.config;
        let mut should_shutdown = false;
        let mut current_focus = self.window_focused;
        self.events.poll_events(|raw_event| {
            match raw_event {
                Event::WindowEvent { window_id, event } => {
                    match event {
                        WindowEvent::Resized(_) | WindowEvent::HiDpiFactorChanged(_) => {
                            //Window Must Be Re-drawn
                            let _change = config.get_state_change();
                        }
                        WindowEvent::CloseRequested | WindowEvent::Destroyed => {
                            //Window Must be shutdown
                            info!("Starting Requested");
                            should_shutdown = true;
                        }
                        WindowEvent::Moved(_) | WindowEvent::Refresh => {
                            //NO OP
                        },
                        WindowEvent::Focused(is_focused) => {
                            current_focus = is_focused;
                        }
                        other_event => {
                            //Forward Event
                            f_window(other_event,config);
                        }
                    }
                }
                Event::DeviceEvent { device_id, event } => {
                    if current_focus {
                        f_device(event, config);
                    }
                }
                _ => {}
            }
        });
        self.window_focused = current_focus;
        should_shutdown
    }

    pub fn get_window_mode(&self) -> StateScreen {
        self.window_state
    }

    pub fn get_sync_mode(&self) -> StateSync {
        self.api.get_sync_state()
    }

    pub fn supported_window_mode(&self, _state: StateScreen) -> bool {
        true
    }

    pub fn supported_sync_mode(&self, state: StateSync) -> bool {
        self.api.is_sync_supported(state)
    }
}
