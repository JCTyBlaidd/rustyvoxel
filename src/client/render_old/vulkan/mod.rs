use crate::prelude::*;
use crate::util::ProgramConfig;

use super::{GpuBufferAlloc, GpuBufferCapabilities, GpuMemoryType, GraphicsAPI, StateSync};

use ash::version::DeviceV1_0;
use ash::*;
use std::ptr::NonNull;
use winit::Window;

mod handle;
mod memory;
mod util;

//TODO: DOES THIS NEED TO BE REMOVED?!?!?!
pub struct ConditionalScope<F: Fn()> {
    drop_fn: Option<F>,
}
impl<F: Fn()> ConditionalScope<F> {
    pub fn new(f: F) -> ConditionalScope<F> {
        ConditionalScope { drop_fn: Some(f) }
    }
    pub fn cancel(mut self) {
        self.drop_fn = None;
    }
}
impl<F: Fn()> Drop for ConditionalScope<F> {
    fn drop(&mut self) {
        if let Some(ref f) = self.drop_fn {
            f()
        }
    }
}

pub fn is_vulkan_supported() -> Option<Entry> {
    let result: Result<Entry, LoadingError> = Entry::new();
    match result {
        Ok(entry) => {
            //entry.enumerate_instance_version();
            //TODO: VERIFY MINIMUM VULKAN VERSION
            info!("Successfully Loaded {} Library", fmt_highlight("Vulkan"));
            Some(entry)
        }
        Err(load_error) => {
            warn!(
                "Failed to Load Vulkan: {}",
                fmt_failure(format!("{:?}", load_error))
            );
            info!("Using OpenGL Fallback");
            None
        }
    }
}

pub struct VulkanRender {
    state: handle::VulkanGlobalState,
    memory: memory::VkPageAllocator,
}

impl GraphicsAPI for VulkanRender {
    fn acquire_frame(&mut self) {
        unimplemented!()
    }

    fn submit_frame(&mut self) {
        unimplemented!()
    }

    unsafe fn allocate_buffer_page(
        &self,
        mem: GpuMemoryType,
        caps: &GpuBufferCapabilities,
    ) -> Option<GpuBufferAlloc> {
        self.memory.allocate(mem, caps, &self.state.device)
    }
    unsafe fn free_buffer_page(&self, mem: GpuMemoryType, alloc: GpuBufferAlloc) {
        self.memory.free(mem, alloc, &self.state.device);
    }
    unsafe fn map_memory(&self, alloc: GpuBufferAlloc) -> NonNull<u8> {
        self.memory.map(alloc, &self.state.device)
    }
    unsafe fn un_map_memory(&self, alloc: GpuBufferAlloc) {
        self.memory.un_map(alloc, &self.state.device);
    }
    unsafe fn flush_non_coherent_ranges(&self, alloc: GpuBufferAlloc, offset: u64, size: u64) {
        unimplemented!()
    }
    unsafe fn invalidate_non_coherent_ranges(&self, alloc: GpuBufferAlloc, offset: u64, size: u64) {
        unimplemented!()
    }

    fn start_state_change(&mut self) {
        //Device Wait Idle
        unsafe {
            self.state.device.device.device_wait_idle().unwrap();
        }
    }

    fn stop_state_change(&mut self, window: &super::APIWindow) {
        //Re-create swap_chain
        self.state.swap_chain.recreate_swap_chain(
            &mut self.state.device,
            &window.get_window(),
            &self.state.surface,
        )
    }

    fn is_sync_supported(&self, state: StateSync) -> bool {
        let vk_state = match state {
            StateSync::Enabled => vk::PresentModeKHR::FIFO,
            StateSync::Disabled => vk::PresentModeKHR::IMMEDIATE,
            StateSync::Relaxed => vk::PresentModeKHR::FIFO_RELAXED,
            StateSync::TripleBuffered => vk::PresentModeKHR::MAILBOX,
        };
        self.state
            .device
            .physical_device
            .surface_present
            .contains(&vk_state)
    }

    fn get_sync_state(&self) -> StateSync {
        let vk_state = self.state.swap_chain.swap_config.chosen_present_mode;
        match vk_state {
            vk::PresentModeKHR::FIFO => StateSync::Enabled,
            vk::PresentModeKHR::IMMEDIATE => StateSync::Disabled,
            vk::PresentModeKHR::FIFO_RELAXED => StateSync::Relaxed,
            vk::PresentModeKHR::MAILBOX => StateSync::TripleBuffered,
            _ => panic!("Unsupported Vulkan Present Mode!"),
        }
    }

    fn set_sync_state(&mut self, state: StateSync) {
        let vk_state = match state {
            StateSync::Enabled => vk::PresentModeKHR::FIFO,
            StateSync::Disabled => vk::PresentModeKHR::IMMEDIATE,
            StateSync::Relaxed => vk::PresentModeKHR::FIFO_RELAXED,
            StateSync::TripleBuffered => vk::PresentModeKHR::MAILBOX,
        };
        self.state.swap_chain.swap_config.chosen_present_mode = vk_state;
    }
}

pub fn load_vulkan(window: &Window, entry: Entry, config: &ProgramConfig) -> Option<VulkanRender> {
    let state = handle::VulkanGlobalState::init(window, entry, config)?;
    let page_alloc = memory::VkPageAllocator::create(&state.device);

    Some(VulkanRender {
        state,
        memory: page_alloc,
    })
}
