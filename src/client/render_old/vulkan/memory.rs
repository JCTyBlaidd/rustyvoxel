use super::util::VkDevice;
use super::{GpuBufferAlloc, GpuBufferCapabilities, GpuMemoryType};
use ash::version::DeviceV1_0;
use ash::vk::Handle;
use ash::*;
use log::info;
use smallvec::*;
use std::ptr::NonNull;
use std::sync;

#[cfg(target_has_atomic = "32")]
type CountU32 = sync::atomic::AtomicU32;

#[cfg(all(not(target_has_atomic = "32"), target_has_atomic = "ptr"))]
type Count32 = sync::atomic::AtomicUsize;

pub struct VkPageAllocator {
    page_size: u64,
    alloc_limit: u32,
    alloc_count: CountU32,
    buffer_type_index: u32,
}

impl VkPageAllocator {
    pub fn create(device: &VkDevice) -> VkPageAllocator {
        let limits = &device.physical_device.props.limits;
        let alloc_limit = limits.max_memory_allocation_count;
        let page_size = 1024 * 1024 * 32; //32MB Page Size
        info!(
            "Allocation Limit: {}x{}MB",
            alloc_limit,
            page_size / (1024 * 1024)
        );
        VkPageAllocator {
            page_size,
            alloc_limit,
            alloc_count: CountU32::new(0),
            buffer_type_index: 0,
        }
    }

    pub fn find_memory_type(
        device: &VkDevice,
        type_filter: u32,
        required_props: vk::MemoryPropertyFlags,
        optional_props: vk::MemoryPropertyFlags,
    ) -> Option<u32> {
        let property_list = [required_props | optional_props, required_props];
        for property in property_list.iter() {
            let memory_list = &device.physical_device.memory.memory_types;
            for (idx, mem_type) in memory_list.iter().enumerate() {
                let allowed_type = (type_filter & (1 << idx as u32)) != 0;
                let contains_props = mem_type.property_flags.contains(*property);
                if allowed_type && contains_props {
                    return Some(idx as u32);
                }
            }
        }
        None
    }

    pub unsafe fn allocate(
        &self,
        mem: GpuMemoryType,
        caps: &GpuBufferCapabilities,
        device: &VkDevice,
    ) -> Option<GpuBufferAlloc> {
        //Limit Allocation Count
        use std::sync::atomic::Ordering;
        let new_count = self.alloc_count.fetch_add(1, Ordering::SeqCst);
        if new_count >= self.alloc_limit {
            self.alloc_count.fetch_sub(1, Ordering::SeqCst);
            return None;
        }

        //Configure Buffer
        let mut family_list = SmallVec::<[u32; 3]>::new();
        family_list.push(device.queue_main.family);
        if let Some(queue) = &device.queue_compute {
            family_list.push(queue.family);
        }
        if let Some(queue) = &device.queue_transfer {
            family_list.push(queue.family);
        }
        let (queue_list, share_mode) = if caps.shared_access {
            (&family_list[..], vk::SharingMode::CONCURRENT)
        } else {
            (Default::default(), vk::SharingMode::EXCLUSIVE)
        };
        let mut usage = vk::BufferUsageFlags::default();
        if caps.index_buf {
            usage |= vk::BufferUsageFlags::INDEX_BUFFER
        }
        if caps.indirect_buf {
            usage |= vk::BufferUsageFlags::INDIRECT_BUFFER
        }
        if caps.storage_buf {
            usage |= vk::BufferUsageFlags::STORAGE_BUFFER
        }
        if caps.storage_tex {
            usage |= vk::BufferUsageFlags::STORAGE_TEXEL_BUFFER
        }
        if caps.transfer_src {
            usage |= vk::BufferUsageFlags::TRANSFER_SRC
        }
        if caps.transfer_dst {
            usage |= vk::BufferUsageFlags::TRANSFER_DST
        }
        if caps.vertex_buf {
            usage |= vk::BufferUsageFlags::VERTEX_BUFFER
        }
        if caps.uniform_buf {
            usage |= vk::BufferUsageFlags::UNIFORM_BUFFER
        }
        if caps.uniform_tex {
            usage |= vk::BufferUsageFlags::UNIFORM_TEXEL_BUFFER
        }
        let buffer_info = vk::BufferCreateInfo::builder()
            .size(self.page_size)
            .flags(Default::default())
            .queue_family_indices(queue_list)
            .sharing_mode(share_mode)
            .usage(usage);

        //Create Buffer
        let buffer = device.device.create_buffer(&buffer_info, None).ok()?;

        //Get Requirements
        let buffer_types = device.device.get_buffer_memory_requirements(buffer);

        //Acquire Required Properties
        let (req_props, opt_props) = match mem {
            GpuMemoryType::GPUDedicated => {
                (vk::MemoryPropertyFlags::DEVICE_LOCAL, Default::default())
            }
            GpuMemoryType::TransferToGPU => (
                vk::MemoryPropertyFlags::HOST_VISIBLE,
                vk::MemoryPropertyFlags::HOST_COHERENT,
            ),
            GpuMemoryType::CPUDedicated => (
                vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
                Default::default(),
            ),
            GpuMemoryType::TransferToCPU => (
                vk::MemoryPropertyFlags::HOST_VISIBLE,
                vk::MemoryPropertyFlags::HOST_CACHED,
            ),
        };
        let type_index = VkPageAllocator::find_memory_type(
            device,
            buffer_types.memory_type_bits,
            req_props,
            opt_props,
        )
        .or_else(|| {
            //Destroy Buffer
            device.device.destroy_buffer(buffer, None);
            None
        })?;

        //Memory info
        let memory_info = vk::MemoryAllocateInfo::builder()
            .allocation_size(buffer_types.size)
            .memory_type_index(type_index);

        //Allocate Memory
        let device_memory = device
            .device
            .allocate_memory(&memory_info, None)
            .map_err(|e| {
                //Destroy Buffer
                device.device.destroy_buffer(buffer, None);
                e
            })
            .ok()?;

        let type_slice = &device.physical_device.memory.memory_types[..];
        let coherent = type_slice[type_index as usize]
            .property_flags
            .contains(vk::MemoryPropertyFlags::HOST_COHERENT);
        Some(GpuBufferAlloc {
            size: buffer_info.size,
            ident_mem: device_memory.as_raw(),
            ident_buf: buffer.as_raw(),
            coherent,
        })
    }
    pub unsafe fn free(&self, _mem: GpuMemoryType, buffer: GpuBufferAlloc, device: &VkDevice) {
        device
            .device
            .destroy_buffer(vk::Buffer::from_raw(buffer.ident_buf), None);
        device
            .device
            .free_memory(vk::DeviceMemory::from_raw(buffer.ident_mem), None);
    }
    pub unsafe fn map(&self, buffer: GpuBufferAlloc, device: &VkDevice) -> NonNull<u8> {
        let ptr = device
            .device
            .map_memory(
                vk::DeviceMemory::from_raw(buffer.ident_mem),
                0,
                buffer.size,
                vk::MemoryMapFlags::from_raw(0),
            )
            .unwrap();

        //Result was OK => ptr is not null
        NonNull::new_unchecked(ptr as _)
    }
    pub unsafe fn un_map(&self, buffer: GpuBufferAlloc, device: &VkDevice) {
        device
            .device
            .unmap_memory(vk::DeviceMemory::from_raw(buffer.ident_mem));
    }
}
