use ash::version::DeviceV1_0;
use ash::*;
use log::{error, info};
use smallvec::*;
use std::option::NoneError;
use winit::Window;

use super::{VkDevice, VkPhysicalDevice, VkSurface};

/// Handles Swap Chain
///  No Drop Implementation
///  SwapChain must be
///  destroyed manually
pub struct VkSwapChain {
    pub swap_chain: vk::SwapchainKHR,
    pub swap_current: u32,
    pub swap_images: SmallVec<[(vk::Image, vk::ImageView); 2]>,
    pub swap_config: SwapConfig,
}

impl VkSwapChain {
    pub fn create_swap_chain(
        device: &VkDevice,
        window: &Window,
        surface: &VkSurface,
    ) -> Option<VkSwapChain> {
        let config = SwapConfig::choose_default(&device.physical_device, window);

        //Get Swap Config
        let info = config.swap_chain_info(surface, &device.physical_device);

        //Create Swap Chain
        let swap_chain =
            unsafe { device.ext_swap_chain.create_swapchain(&info, None) }.map_err(|e| {
                error!("Failed to create swap-chain: {}", e);
                NoneError
            })?;

        //Get Swap Images
        let images =
            unsafe { device.ext_swap_chain.get_swapchain_images(swap_chain) }.map_err(|e| {
                error!("Failed to get swap-chain images: {}", e);
                unsafe {
                    device.ext_swap_chain.destroy_swapchain(swap_chain, None);
                }
                NoneError
            })?;

        //Create Image Views
        let mut view_info = config.default_image_view();
        let swap_images = images
            .into_iter()
            .map(|image| {
                view_info.image = image;
                let image_view =
                    unsafe { device.device.create_image_view(&view_info, None) }.unwrap();
                (image, image_view)
            })
            .collect();

        Some(VkSwapChain {
            swap_chain,
            swap_current: 0,
            swap_images,
            swap_config: config,
        })
    }

    pub fn recreate_swap_chain(
        &mut self,
        device: &mut VkDevice,
        window: &Window,
        surface: &VkSurface,
    ) {
        self.swap_config
            .update(&mut device.physical_device, window, surface);

        //Destroy Old Image Views
        for (_, view) in self.swap_images.iter() {
            unsafe {
                device.device.destroy_image_view(*view, None);
            }
        }

        //Create New Swap-Chain
        let info = self
            .swap_config
            .swap_chain_info(surface, &device.physical_device)
            .old_swapchain(self.swap_chain);
        self.swap_chain = unsafe { device.ext_swap_chain.create_swapchain(&info, None) }.unwrap();

        //Get Swap Images
        let images =
            unsafe { device.ext_swap_chain.get_swapchain_images(self.swap_chain) }.unwrap();

        //Create Image Views
        let mut view_info = self.swap_config.default_image_view();
        self.swap_images = images
            .into_iter()
            .map(|image| {
                view_info.image = image;
                let image_view =
                    unsafe { device.device.create_image_view(&view_info, None) }.unwrap();
                (image, image_view)
            })
            .collect();
    }

    pub fn destroy_swap_chain(&mut self, device: &VkDevice) {
        unsafe {
            device
                .ext_swap_chain
                .destroy_swapchain(self.swap_chain, None);
        }
    }

    #[inline(always)]
    pub fn get_swap_image(&self) -> vk::Image {
        self.swap_images[self.get_swap_index()].0
    }

    #[inline(always)]
    pub fn get_swap_image_view(&self) -> vk::ImageView {
        self.swap_images[self.get_swap_index()].1
    }

    #[inline(always)]
    pub fn get_swap_index(&self) -> usize {
        self.swap_current as usize
    }

    #[inline(always)]
    pub fn get_swap_size(&self) -> usize {
        self.swap_images.len()
    }
}

pub struct SwapConfig {
    pub chosen_swap_extent: vk::Extent2D,
    pub chosen_present_mode: vk::PresentModeKHR,
    pub chosen_format: vk::SurfaceFormatKHR,
    pub chosen_image_count: u32,
}

fn choose_present_mode(physical: &VkPhysicalDevice) -> vk::PresentModeKHR {
    use ash::vk::PresentModeKHR;
    if physical
        .surface_present
        .contains(&vk::PresentModeKHR::MAILBOX)
    {
        PresentModeKHR::MAILBOX
    } else {
        PresentModeKHR::IMMEDIATE
    }
}

fn choose_surface_format(physical: &VkPhysicalDevice) -> vk::SurfaceFormatKHR {
    use ash::vk::{ColorSpaceKHR, Format, SurfaceFormatKHR};
    let formats = &physical.surface_formats;
    if formats.len() == 1 && formats[0].format == Format::UNDEFINED {
        info!("Chosen Surface Format = Universal");
        SurfaceFormatKHR {
            format: Format::B8G8R8A8_UNORM,
            color_space: ColorSpaceKHR::SRGB_NONLINEAR,
        }
    } else {
        for format in formats {
            if format.format == Format::B8G8R8A8_UNORM
                && format.color_space == ColorSpaceKHR::SRGB_NONLINEAR
            {
                info!("Chosen Surface Format = Found");
                return *format;
            }
        }
        info!("Chosen Surface Format = Fallback ({:?})", formats[0]);
        formats[0]
    }
}

fn choose_swap_extent(physical: &VkPhysicalDevice, window: &Window) -> vk::Extent2D {
    if physical.surface_caps.current_extent.width != 0xFFFFFFFF {
        physical.surface_caps.current_extent
    } else {
        match window.get_inner_size() {
            Some(size) => {
                let width = (size.width as u32)
                    .min(physical.surface_caps.max_image_extent.width)
                    .max(physical.surface_caps.min_image_extent.width);
                let height = (size.height as u32)
                    .min(physical.surface_caps.max_image_extent.height)
                    .max(physical.surface_caps.min_image_extent.height);
                vk::Extent2D { width, height }
            }
            None => physical.surface_caps.min_image_extent,
        }
    }
}

fn choose_image_count(physical: &VkPhysicalDevice) -> u32 {
    2.max(physical.surface_caps.min_image_count)
        .min(physical.surface_caps.max_image_count)
}

impl SwapConfig {
    pub fn choose_default(physical: &VkPhysicalDevice, window: &Window) -> SwapConfig {
        SwapConfig {
            chosen_swap_extent: choose_swap_extent(physical, window),
            chosen_present_mode: choose_present_mode(physical),
            chosen_format: choose_surface_format(physical),
            chosen_image_count: choose_image_count(physical),
        }
    }

    pub fn update(
        &mut self,
        physical: &mut VkPhysicalDevice,
        window: &Window,
        surface: &VkSurface,
    ) {
        physical.reload_surface_caps(surface.surface);
        self.chosen_swap_extent = choose_swap_extent(physical, window);

        //We do not support changing image count.
        assert_eq!(self.chosen_image_count, choose_image_count(physical));
    }

    pub fn default_image_view(&self) -> vk::ImageViewCreateInfo {
        vk::ImageViewCreateInfo::builder()
            .view_type(vk::ImageViewType::TYPE_2D)
            .format(self.chosen_format.format)
            .subresource_range(vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            })
            .build()
    }

    pub fn swap_chain_info(
        &self,
        surface: &VkSurface,
        physical: &VkPhysicalDevice,
    ) -> vk::SwapchainCreateInfoKHRBuilder {
        vk::SwapchainCreateInfoKHR::builder()
            .surface(surface.surface)
            .min_image_count(self.chosen_image_count)
            .image_format(self.chosen_format.format)
            .image_color_space(self.chosen_format.color_space)
            .image_extent(self.chosen_swap_extent)
            .image_array_layers(1)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::TRANSFER_SRC)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .pre_transform(physical.surface_caps.current_transform)
            .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(self.chosen_present_mode)
            .clipped(true)
    }
}
