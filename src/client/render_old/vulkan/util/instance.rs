use ash::version::{EntryV1_0, InstanceV1_0, InstanceV1_1};
use ash::*;
use log::{error, info, warn};
use std::ffi::CStr;
use std::option::NoneError;
use std::sync::Arc;

//FIXME: instance handle is stored multiple times!!!
/// Global ref-counted vulkan instance
///  Contains entry-point, instance, instance_extensions
pub struct VkInstance {
    pub entry: Entry,
    pub instance: Instance,
    pub ext_debug: Option<(extensions::ext::DebugUtils, vk::DebugUtilsMessengerEXT)>,
    pub ext_surface: extensions::khr::Surface,
    __private: (),
}

impl VkInstance {
    pub fn create(
        entry: Entry,
        info: &vk::InstanceCreateInfoBuilder,
        debug: bool,
    ) -> Result<Arc<VkInstance>, InstanceError> {
        let instance = unsafe { entry.create_instance(info, None) }.map_err(|e| {
            error!("Failed to create Vulkan Instance: {}", e);
            e
        })?;

        let ext_debug = if debug {
            let messenger_info = vk::DebugUtilsMessengerCreateInfoEXT::builder()
                .message_severity(vk::DebugUtilsMessageSeverityFlagsEXT::WARNING)
                .message_type(
                    vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE
                        | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION,
                )
                .pfn_user_callback(Some(debug_messenger_callback));

            //Create Messenger
            let debug_utils = extensions::ext::DebugUtils::new(&entry, &instance);
            let messenger =
                unsafe { debug_utils.create_debug_utils_messenger(&messenger_info, None) };

            //Handle Error Case
            match messenger {
                Ok(handle) => {
                    info!(" Created Debug Callback");
                    Some((debug_utils, handle))
                }
                Err(_) => None,
            }
        } else {
            None
        };

        info!(" Loading Surface Extension Pointers");
        let ext_surface = extensions::khr::Surface::new(&entry, &instance);

        Ok(Arc::new(VkInstance {
            entry,
            instance,
            ext_debug,
            ext_surface,
            __private: (),
        }))
    }
}

impl Drop for VkInstance {
    fn drop(&mut self) {
        if let Some((ext_debug, messenger)) = &self.ext_debug {
            info!(" Destroy Debug Messenger");
            unsafe {
                ext_debug.destroy_debug_utils_messenger(*messenger, None);
            }
        }
        info!(" Destroy Instance");
        unsafe {
            self.instance.destroy_instance(None);
        }
    }
}

unsafe extern "system" fn debug_messenger_callback(
    message_severity: vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    p_user_data: *mut std::ffi::c_void,
) -> vk::Bool32 {
    let raw_message = CStr::from_ptr((*p_callback_data).p_message);
    let message = raw_message.to_string_lossy();
    match message_severity {
        vk::DebugUtilsMessageSeverityFlagsEXT::ERROR => {
            error!("VK Debug Callback Error {}: {}", message_type, &message);
            vk::FALSE
        }
        vk::DebugUtilsMessageSeverityFlagsEXT::WARNING => {
            warn!("VK Debug Callback Warning {}: {}", message_type, &message);
            vk::TRUE
        }
        other => {
            info!("VK Debug Callback {} {}: {}", other, message_type, &message);
            vk::TRUE
        }
    }
}
