use crate::util::logging::highlight;
use ash::version::{DeviceV1_0, DeviceV1_1, InstanceV1_0, InstanceV1_1};
use ash::*;
use log::{debug, error, info, warn};
use smallvec::*;
use std::option::NoneError;
use std::os::raw::c_char;
use std::slice;
use std::sync::Arc;

use super::select_extensions;
use super::VkInstance;

/// Wrapper around an initialized device
///  contains reference to physical device
///  and instance
/// Drop Order:
///  self then instance
pub struct VkDevice {
    //Device
    pub device: Device,

    //Queues
    pub queue_main: VkQueue,
    pub queue_compute: Option<VkQueue>,
    pub queue_transfer: Option<VkQueue>,

    //Extensions
    pub ext_swap_chain: extensions::khr::Swapchain,

    //Device Info (instance ref in physical device)
    pub physical_device: Box<VkPhysicalDevice>,
}

impl VkDevice {
    fn check_queue(
        physical: &VkPhysicalDevice,
        list: &[vk::DeviceQueueCreateInfo],
        data: (u32, u32),
    ) -> Option<()> {
        let info = list
            .iter()
            .filter(|info| info.queue_family_index == data.0)
            .next()
            .or_else(|| {
                error!("Creating Queue with invalid family index(not created)");
                None
            })?;
        if data.1 < info.queue_count {
            info!(" - Queue Flags:");
            let flags = physical
                .queues
                .get(data.0 as usize)
                .or_else(|| {
                    error!("Creating Queue with invalid family index(large)");
                    None
                })?
                .queue_flags;
            if flags.contains(vk::QueueFlags::GRAPHICS) {
                info!("   - {}", highlight("GRAPHICS"));
            }
            if flags.contains(vk::QueueFlags::COMPUTE) {
                info!("   - {}", highlight("COMPUTE"));
            }
            if flags.contains(vk::QueueFlags::TRANSFER) {
                info!("   - {}", highlight("TRANSFER"));
            }
            if flags.contains(vk::QueueFlags::SPARSE_BINDING) {
                info!("   - {}", highlight("SPARSE"));
            }
            info!(
                " - Family Index: {}",
                highlight(info.queue_family_index.to_string())
            );
            info!(" - Queue  Index: {}", highlight(data.1.to_string()));
            Some(())
        } else {
            error!("Creating Queue with invalid count index");
            None
        }
    }

    fn create_queue(queue: vk::Queue, family: u32, info: &VkPhysicalDevice) -> VkQueue {
        let data = &info.queues[family as usize];
        VkQueue {
            queue,
            family,
            transfer_granularity: data.min_image_transfer_granularity,
            timestamp_bits: data.timestamp_valid_bits,
        }
    }

    pub fn create(
        physical: Box<VkPhysicalDevice>,
        surface: vk::SurfaceKHR,
        layers: &[*const c_char],
        required_extensions: &[&'static str],
    ) -> Option<VkDevice> {
        //Choose Extensions
        let extension_list = unsafe {
            physical
                .get_instance()
                .enumerate_device_extension_properties(physical.physical_device)
        }
        .map_err(|_| NoneError)?;
        let (extensions, _) = select_extensions(&extension_list, &required_extensions, &[])?;

        //Choose Main Queue
        let queue_main = physical
            .queues
            .iter()
            .enumerate()
            .find(|(idx, queue)| {
                let is_main = queue.queue_flags.contains(
                    vk::QueueFlags::GRAPHICS
                        | vk::QueueFlags::COMPUTE
                        | vk::QueueFlags::TRANSFER
                        | vk::QueueFlags::SPARSE_BINDING,
                );
                if is_main {
                    unsafe {
                        physical
                            .instance
                            .ext_surface
                            .get_physical_device_surface_support(
                                physical.physical_device,
                                *idx as u32,
                                surface,
                            )
                    }
                } else {
                    false
                }
            })
            .unwrap();
        let family_main = queue_main.0 as u32;
        let index_main = 0u32;
        let count_main = queue_main.1.queue_count;

        //Choose Compute Queue
        let queue_compute = physical
            .queues
            .iter()
            .enumerate()
            .find(|(idx, queue)| {
                if *idx as u32 == family_main {
                    false
                } else {
                    queue.queue_flags.contains(vk::QueueFlags::COMPUTE)
                }
            })
            .map(|(family, queue)| (family as u32, 0u32))
            .or_else(|| {
                if count_main > 1 {
                    Some((family_main, 1u32))
                } else {
                    None
                }
            });

        //Choose Transfer Queue
        let queue_transfer = physical
            .queues
            .iter()
            .enumerate()
            .find(|(idx, queue)| {
                if *idx as u32 == family_main {
                    false
                } else {
                    let is_compute =
                        queue_compute.map_or(false, |compute| *idx as u32 == compute.0);
                    if is_compute {
                        false
                    } else {
                        queue.queue_flags.contains(vk::QueueFlags::TRANSFER)
                    }
                }
            })
            .map(|(family, queue)| (family as u32, 0u32))
            .or_else(|| {
                let req_count =
                    queue_compute.map_or(1, |compute| if compute.0 == family_main { 2 } else { 1 });
                if count_main > req_count {
                    Some((family_main, req_count))
                } else {
                    None
                }
            });

        //Build PQueue Information
        let mut priority_main = SmallVec::<[f32; 2]>::new();
        let mut priority_compute = SmallVec::<[f32; 2]>::new();
        let mut priority_transfer = SmallVec::<[f32; 2]>::new();
        priority_main.push(1.0f32);
        if let Some(compute) = queue_compute {
            if compute.0 == family_main {
                priority_main.push(1.0f32)
            } else {
                priority_compute.push(1.0f32)
            }
        }
        if let Some(transfer) = queue_transfer {
            if transfer.0 == family_main {
                priority_main.push(0.75f32);
            } else {
                priority_transfer.push(1.0f32);
            }
        }

        let mut queue_infos = Vec::new();
        queue_infos.push(
            vk::DeviceQueueCreateInfo::builder()
                .queue_family_index(family_main)
                .queue_priorities(&priority_main[..])
                .build(),
        );
        if priority_compute.len() > 0 {
            queue_infos.push(
                vk::DeviceQueueCreateInfo::builder()
                    .queue_family_index(queue_compute.unwrap().0)
                    .queue_priorities(&priority_compute[..])
                    .build(),
            )
        }
        if priority_transfer.len() > 0 {
            queue_infos.push(
                vk::DeviceQueueCreateInfo::builder()
                    .queue_family_index(queue_transfer.unwrap().0)
                    .queue_priorities(&priority_transfer[..])
                    .build(),
            )
        }

        let features = physical.features.clone();

        let info = vk::DeviceCreateInfo::builder()
            .enabled_extension_names(&extensions)
            .enabled_layer_names(layers)
            .enabled_features(&features)
            .queue_create_infos(&queue_infos);

        return VkDevice::new(
            &info,
            physical,
            (family_main, index_main),
            queue_compute,
            queue_transfer,
        );
    }

    pub fn new(
        info: &vk::DeviceCreateInfoBuilder,
        physical: Box<VkPhysicalDevice>,
        main_idx: (u32, u32),
        compute_idx: Option<(u32, u32)>,
        transfer_idx: Option<(u32, u32)>,
    ) -> Option<VkDevice> {
        //Load Queue Info
        let queue_info = unsafe {
            slice::from_raw_parts(
                info.p_queue_create_infos,
                info.queue_create_info_count as usize,
            )
        };

        //Assert That Queue is valid
        info!("Main Queue:");
        let _ = VkDevice::check_queue(&physical, queue_info, main_idx)?;
        if let Some(idx) = compute_idx {
            info!("Compute Queue:");
            let _ = VkDevice::check_queue(&physical, queue_info, idx)?;
        }
        if let Some(idx) = transfer_idx {
            info!("Transfer Queue:");
            let _ = VkDevice::check_queue(&physical, queue_info, idx)?;
        }

        //Create Device
        let device = unsafe {
            physical
                .get_instance()
                .create_device(physical.physical_device, &info, None)
        }
        .map_err(|e| {
            error!("Failed to create Vulkan Device: {}", e);
            NoneError
        })?;

        //Build Swap Chain
        let ext_swap_chain = extensions::khr::Swapchain::new(physical.get_instance(), &device);

        //Get Queue Info
        let queue_main = unsafe {
            let queue = device.get_device_queue(main_idx.0, main_idx.1);
            VkDevice::create_queue(queue, main_idx.0, &physical)
        };
        let queue_compute = compute_idx.map(|idx| unsafe {
            let queue = device.get_device_queue(idx.0, idx.1);
            VkDevice::create_queue(queue, idx.0, &physical)
        });
        let queue_transfer = transfer_idx.map(|idx| unsafe {
            let queue = device.get_device_queue(idx.0, idx.1);
            VkDevice::create_queue(queue, idx.0, &physical)
        });

        //Create
        Some(VkDevice {
            device,

            queue_main,
            queue_compute,
            queue_transfer,

            ext_swap_chain,

            physical_device: physical,
        })
    }
}

impl Drop for VkDevice {
    fn drop(&mut self) {
        unsafe {
            info!(" Destroy Device");
            self.device.destroy_device(None);
        }
    }
}

/// Wrapper around a vulkan queue
///  contains handle as well as
///  configuration information
pub struct VkQueue {
    pub queue: vk::Queue,
    pub family: u32,
    pub timestamp_bits: u32,
    pub transfer_granularity: vk::Extent3D,
}

/// Wrapper around instance specific physical device
///  information, and properties
///  contains an owning reference to an instance
/// Drop Order:
///  only instance drops
pub struct VkPhysicalDevice {
    pub instance: Arc<VkInstance>,
    pub physical_device: vk::PhysicalDevice,

    pub props: vk::PhysicalDeviceProperties,
    pub memory: vk::PhysicalDeviceMemoryProperties,
    pub features: vk::PhysicalDeviceFeatures,
    pub queues: Vec<vk::QueueFamilyProperties>,

    pub surface_caps: vk::SurfaceCapabilitiesKHR,
    pub surface_formats: Vec<vk::SurfaceFormatKHR>,
    pub surface_present: Vec<vk::PresentModeKHR>,
}

impl VkPhysicalDevice {
    pub fn new(
        instance: Arc<VkInstance>,
        physical: vk::PhysicalDevice,
        surface: vk::SurfaceKHR,
    ) -> Box<VkPhysicalDevice> {
        let mut prop2 = vk::PhysicalDeviceProperties2::default();
        unsafe {
            //instance.instance.get_physical_device_properties2(physical,&mut prop2);
            prop2.properties = instance.instance.get_physical_device_properties(physical);
        }

        let mut memory2 = vk::PhysicalDeviceMemoryProperties2::default();
        unsafe {
            //instance.instance.get_physical_device_memory_properties2(physical, &mut memory2);
            memory2.memory_properties = instance
                .instance
                .get_physical_device_memory_properties(physical);
        }

        let features = unsafe { instance.instance.get_physical_device_features(physical) };

        let queues = unsafe {
            instance
                .instance
                .get_physical_device_queue_family_properties(physical)
        };

        let surface_caps = unsafe {
            instance
                .ext_surface
                .get_physical_device_surface_capabilities(physical, surface)
        }
        .unwrap();

        let surface_formats = unsafe {
            instance
                .ext_surface
                .get_physical_device_surface_formats(physical, surface)
        }
        .unwrap();

        let surface_present = unsafe {
            instance
                .ext_surface
                .get_physical_device_surface_present_modes(physical, surface)
        }
        .unwrap();

        Box::new(VkPhysicalDevice {
            instance,
            physical_device: physical,

            props: prop2.properties,
            memory: memory2.memory_properties,
            features,
            queues,

            surface_caps,
            surface_formats,
            surface_present,
        })
    }

    pub fn reload_surface_caps(&mut self, surface: vk::SurfaceKHR) {
        let surface_caps = unsafe {
            self.instance
                .ext_surface
                .get_physical_device_surface_capabilities(self.physical_device, surface)
        };
        if let Ok(cap) = surface_caps {
            self.surface_caps = cap
        }
    }

    pub fn get_instance(&self) -> &Instance {
        &self.instance.instance
    }

    pub fn is_dedicated(&self) -> bool {
        self.props.device_type == vk::PhysicalDeviceType::DISCRETE_GPU
    }

    pub fn score_gpu(&self) -> i64 {
        let mut score = 0;
        score += self.props.limits.max_compute_work_group_invocations as i64;
        score += self
            .memory
            .memory_heaps
            .iter()
            .map(|heap| heap.size as i64)
            .sum::<i64>();
        if self.is_dedicated() {
            score
        } else {
            score - (1 << 48)
        }
    }

    pub fn can_support_surface(&self, surface: vk::SurfaceKHR) -> bool {
        self.queues.iter().enumerate().any(|(idx, queue)| {
            let is_main = queue.queue_flags.contains(
                vk::QueueFlags::GRAPHICS
                    | vk::QueueFlags::COMPUTE
                    | vk::QueueFlags::TRANSFER
                    | vk::QueueFlags::SPARSE_BINDING,
            );
            if is_main {
                unsafe {
                    self.instance
                        .ext_surface
                        .get_physical_device_surface_support(
                            self.physical_device,
                            idx as u32,
                            surface,
                        )
                }
            } else {
                false
            }
        })
    }
}
