use crate::util::logging::success;
use ash::*;
use log::{info, warn};
use std::borrow::Cow;
use std::ffi::{CStr, CString};
use std::os::raw::c_char;

pub mod device;
pub mod instance;
pub mod surface;
pub mod swap_chain;

pub use self::device::{VkDevice, VkPhysicalDevice, VkQueue};
pub use self::instance::VkInstance;
pub use self::surface::VkSurface;
pub use self::swap_chain::VkSwapChain;

pub fn select_layers(
    entries: &[vk::LayerProperties],
    required: &[&'static str],
    optional: &[&'static str],
) -> Option<(Vec<*const c_char>, Vec<bool>)> {
    select_entries(entries, required, optional, |l| &l.layer_name)
}

pub fn select_extensions(
    entries: &[vk::ExtensionProperties],
    required: &[&'static str],
    optional: &[&'static str],
) -> Option<(Vec<*const c_char>, Vec<bool>)> {
    select_entries(entries, required, optional, |e| &e.extension_name)
}

fn convert(val: &str) -> Cow<CStr> {
    match CStr::from_bytes_with_nul(val.as_bytes()) {
        Ok(value) => Cow::Borrowed(value),
        Err(_) => {
            let mut bytes: Vec<u8> = val
                .as_bytes()
                .iter()
                .filter_map(|b| if *b != 0 { Some(*b) } else { None })
                .collect();
            Cow::Owned(CString::new(bytes).unwrap())
        }
    }
}

fn compare_str(val: &[c_char], other: &str) -> bool {
    let mut byte_vec: Vec<u8> = val
        .iter()
        .filter_map(|i| if *i == 0 { None } else { Some(*i as u8) })
        .collect();
    byte_vec.push(0);
    let c_str = CStr::from_bytes_with_nul(&byte_vec).unwrap();
    c_str.to_str().map(|v| v == other).unwrap_or(false)
}

fn get_ident(val: &'static str) -> *const c_char {
    match convert(val) {
        Cow::Borrowed(borrow) => borrow.as_ptr(),
        Cow::Owned(own) => own.into_raw() as *const c_char,
    }
}

fn select_entries<'a, T>(
    entries: &'a [T],
    required: &[&'static str],
    optional: &[&'static str],
    get_name: fn(&T) -> &[c_char],
) -> Option<(Vec<*const c_char>, Vec<bool>)> {
    let mut chosen = Vec::with_capacity(required.len() + optional.len());

    //Handle Required
    for req in required {
        let found = entries
            .iter()
            .any(|entry| compare_str(get_name(entry), req));
        if found {
            info!(" Enabled[Required]: {}", success(*req));
            chosen.push(get_ident(req));
        } else {
            warn!(" Required Entry Missing: {}", req);
            return None;
        }
    }

    //Handle Optional
    let mut enabled = vec![false; optional.len()];
    for (idx, opt) in optional.iter().enumerate() {
        let found = entries
            .iter()
            .any(|entry| compare_str(get_name(entry), opt));
        if found {
            info!(" Enabled[Optional]: {}", success(*opt));
            chosen.push(get_ident(opt));
            enabled[idx] = true;
        } else {
            info!(" Missing[Optional]: {}", *opt);
        }
    }

    //Return Success
    Some((chosen, enabled))
}
