use super::VkInstance;
use ash::*;
use log::{error, info};
use std::sync::Arc;
use winit::Window;

/// Information about Chosen Surface Extension
pub struct SurfaceMetadata(SurfaceType);

pub struct VkSurface {
    pub instance: Arc<VkInstance>,
    pub surface: vk::SurfaceKHR,
}

impl VkSurface {
    pub fn required_extensions(
        ext_list: &[vk::ExtensionProperties],
    ) -> (SurfaceMetadata, Vec<&'static str>) {
        let (meta, ext): (SurfaceMetadata, &'static str) = get_extensions(ext_list);
        let core_ext = extensions::khr::Surface::name().to_str().unwrap();
        (meta, vec![ext, core_ext])
    }

    pub fn new(
        instance: Arc<VkInstance>,
        window: &Window,
        meta: SurfaceMetadata,
    ) -> Option<VkSurface> {
        let res: Result<vk::SurfaceKHR, vk::Result> = new_surface(&instance, window, meta);
        match res {
            Ok(surface) => {
                info!(" OS Specific Loaded");
                Some(VkSurface { instance, surface })
            }
            Err(err) => {
                error!("Failed to create surface: {}", err);
                None
            }
        }
    }
}

impl Drop for VkSurface {
    fn drop(&mut self) {
        unsafe {
            info!(" Destroy Surface");
            self.instance
                .ext_surface
                .destroy_surface(self.surface, None);
        }
    }
}

#[cfg(any(
    target_os = "linux",
    target_os = "dragonfly",
    target_os = "freebsd",
    target_os = "netbsd",
    target_os = "openbsd"
))]
use self::unix::*;

#[cfg(target_os = "windows")]
use self::windows::*;

#[cfg(target_os = "macos")]
use self::macos::*;

#[cfg(any(target_os = "android"))]
use self::android::*;

#[cfg(target_os = "ios")]
use self::ios::*;

#[cfg(any(
    target_os = "linux",
    target_os = "dragonfly",
    target_os = "freebsd",
    target_os = "netbsd",
    target_os = "openbsd"
))]
mod unix {
    use super::super::compare_str;
    use super::*;
    use std::ffi::CStr;
    use winit::os::unix::WindowExt;

    #[repr(u8)]
    pub enum SurfaceType {
        XcbSurface,
        XLibSurface,
        WaylandSurface,
    }

    pub fn get_extensions(ext_list: &[vk::ExtensionProperties]) -> (SurfaceMetadata, &'static str) {
        let wayland_found = ext_list.iter().any(|entry| {
            compare_str(
                &entry.extension_name,
                extensions::khr::WaylandSurface::name().to_str().unwrap(),
            )
        });
        let x_lib_found = ext_list.iter().any(|entry| {
            compare_str(
                &entry.extension_name,
                extensions::khr::XlibSurface::name().to_str().unwrap(),
            )
        });
        if wayland_found {
            (
                SurfaceMetadata(SurfaceType::WaylandSurface),
                extensions::khr::WaylandSurface::name().to_str().unwrap(),
            )
        } else if x_lib_found {
            (
                SurfaceMetadata(SurfaceType::XLibSurface),
                extensions::khr::XlibSurface::name().to_str().unwrap(),
            )
        } else {
            (
                SurfaceMetadata(SurfaceType::XcbSurface),
                extensions::khr::XcbSurface::name().to_str().unwrap(),
            )
        }
    }

    pub fn new_surface(
        instance: &VkInstance,
        window: &Window,
        meta: SurfaceMetadata,
    ) -> Result<vk::SurfaceKHR, vk::Result> {
        match meta.0 {
            SurfaceType::XcbSurface => {
                let ext = extensions::khr::XcbSurface::new(&instance.entry, &instance.instance);
                let info = vk::XcbSurfaceCreateInfoKHR::builder()
                    .connection(window.get_xcb_connection().unwrap() as _)
                    .window(window.get_xlib_window().unwrap() as _);
                unsafe { ext.create_xcb_surface(&info, None) }
            }
            SurfaceType::XLibSurface => {
                let ext = extensions::khr::XlibSurface::new(&instance.entry, &instance.instance);
                let info = vk::XlibSurfaceCreateInfoKHR::builder()
                    .dpy(window.get_xlib_display().unwrap() as _)
                    .window(window.get_xlib_window().unwrap());
                unsafe { ext.create_xlib_surface(&info, None) }
            }
            SurfaceType::WaylandSurface => {
                let ext = extensions::khr::WaylandSurface::new(&instance.entry, &instance.instance);
                let info = vk::WaylandSurfaceCreateInfoKHR::builder()
                    .surface(window.get_wayland_surface().unwrap())
                    .display(window.get_wayland_display().unwrap());
                unsafe { ext.create_wayland_surface(&info, None) }
            }
        }
    }
}

#[cfg(target_os = "windows")]
mod windows {
    use super::*;
    use winit::os::windows::WindowExt;
    pub type SurfaceType = ();

    pub fn get_extensions(_: &[vk::ExtensionProperties]) -> (SurfaceMetadata, &'static str) {
        (
            SurfaceMetadata(()),
            extensions::khr::Win32Surface::name().to_str().unwrap(),
        )
    }

    pub fn new_surface(
        instance: &VkInstance,
        window: &Window,
        meta: SurfaceMetadata,
    ) -> Result<vk::SurfaceKHR, vk::Result> {
        let ext = extensions::khr::Win32Surface::new(&instance.entry, &instance.instance);
        let info = vk::Win32SurfaceCreateInfoKHR::builder().hwnd(window.get_hwnd() as _);
        unsafe { ext.create_win32_surface(&info, None) }
    }
}

#[cfg(target_os = "macos")]
mod macos {
    use super::*;
    use winit::os::macos::WindowExt;
    type SurfaceType = ();

    pub fn get_extensions(_: &[vk::ExtensionProperties]) -> (SurfaceMetadata, &'static str) {
        (
            SurfaceMetadata(()),
            extensions::mvk::MacOSSurface::name().to_str().unwrap(),
        )
    }

    pub fn new_surface(
        instance: &VkInstance,
        window: &Window,
        meta: SurfaceMetadata,
    ) -> Result<vk::SurfaceKHR, vk::Result> {
        let ext = extensions::mvk::MacOSSurface::new(&instance.entry, &instance.instance);
        //TODO: MAY NEED MORE CONFIGURATION?
        let info = vk::MacOSSurfaceCreateInfoMVK::builder().view(window.get_nsview() as _);
        unsafe { ext.create_mac_os_surface_mvk(&info, None) }
    }
}

#[cfg(any(target_os = "android"))]
mod android {
    use super::*;
    use winit::os::android::WindowExt;
    type SurfaceType = ();

    pub fn get_extensions(_: &[vk::ExtensionProperties]) -> (SurfaceMetadata, &'static str) {
        (
            SurfaceMetadata(()),
            extensions::AndroidSurface::name().to_str().unwrap(),
        )
    }

    pub fn new_surface(
        instance: &VkInstance,
        window: &Window,
        meta: SurfaceMetadata,
    ) -> Result<vk::SurfaceKHR, vk::Result> {
        let ext = extensions::khr::AndroidSurface::new(&instance.entry, &instance.instance);
        let info =
            vk::AndroidSurfaceCreateInfoKHR::builder().window(window.get_native_window() as _);
        unsafe { ext.create_android_surface(&info, None) }
    }
}

#[cfg(target_os = "ios")]
mod ios {
    use super::*;
    use winit::os::ios::WindowExt;
    type SurfaceType = ();

    pub fn get_extensions(_: &[vk::ExtensionProperties]) -> (SurfaceMetadata, &'static str) {
        (
            SurfaceMetadata(()),
            extensions::mvk::IOSSurface::name().to_str().unwrap(),
        )
    }

    pub fn new_surface(
        instance: &VkInstance,
        window: &Window,
        meta: SurfaceMetadata,
    ) -> Result<vk::SurfaceKHR, vk::Result> {
        let ext = extensions::mvk::IOSSurface::new(&instance.entry, &instance.instance);
        let info = vk::IOSSurfaceCreateInfoMVK::builder().view(window.get_uiview() as _);
        unsafe { ext.create_ios_surface_mvk(&info, None) }
    }
}
