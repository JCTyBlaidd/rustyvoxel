use crate::util::ProgramConfig;
use super::util::*;

use ash::version::{EntryV1_0, InstanceV1_0};
use ash::*;
use log::{debug, info, warn};
use std::ffi::CString;
use std::option::NoneError;
use std::sync::Arc;
use winit::Window;

pub struct VulkanGlobalState {
    pub swap_chain: VkSwapChain,
    pub surface: VkSurface,
    pub device: VkDevice,
    pub instance: Arc<VkInstance>,
    __private: (),
}

impl VulkanGlobalState {
    pub fn init(
        window: &Window,
        entry: Entry,
        config: &ProgramConfig,
    ) -> Option<VulkanGlobalState> {
        let application_name = CString::new("Rusty Voxel").unwrap();
        let engine_name = CString::new("Rusty Voxel Engine").unwrap();

        let ext_list = entry
            .enumerate_instance_extension_properties()
            .map_err(|e| {
                warn!("Failed to enumerate instance extensions: {}", e);
                NoneError
            })?;
        let layer_list = entry.enumerate_instance_layer_properties().map_err(|e| {
            warn!("Failed to enumerate instance layers: {}", e);
            NoneError
        })?;

        let mut optional_ext = Vec::new();
        if config.graphics_debug > 0 {
            debug!("Vulkan Requested: Debug Utils");
            optional_ext.push(extensions::ext::DebugUtils::name().to_str().unwrap());
        }

        info!("Selecting Extensions:");
        let (surface_meta, required_ext) = VkSurface::required_extensions(&ext_list);
        let (extensions, ext_enabled) = select_extensions(&ext_list, &required_ext, &optional_ext)?;

        let mut optional_layers = Vec::new();
        if config.graphics_debug > 0 {
            debug!("Vulkan Requested: Standard Validation");
            optional_layers.push("VK_LAYER_LUNARG_standard_validation");
        }
        if config.graphics_debug > 1 {
            debug!("Vulkan Requested: Assistant Layer");
            optional_layers.push("VK_LAYER_LUNARG_assistant_layer");
        }
        if config.graphics_debug > 2 {
            debug!("Vulkan Requested: API DUMP");
            optional_layers.push("VK_LAYER_LUNARG_api_dump");
        }
        if config.setup_render_doc {
            debug!("Vulkan Requested: RenderDoc Capture");
            optional_layers.push("VK_LAYER_RENDERDOC_Capture");
        }
        info!("Selecting Layers:");
        let (layers, _) = select_layers(&layer_list, &[], &optional_layers)?;

        let use_debug = if config.graphics_debug > 0 {
            ext_enabled[0]
        } else {
            false
        };

        let application_info = vk::ApplicationInfo::builder()
            .engine_name(&engine_name)
            .engine_version(vk_make_version!(0, 1, 0))
            .application_name(&application_name)
            .application_version(vk_make_version!(0, 1, 0))
            .api_version(vk_make_version!(1, 1, 70));

        let instance_info = vk::InstanceCreateInfo::builder()
            .application_info(&application_info)
            .enabled_layer_names(&layers)
            .enabled_extension_names(&extensions);

        //Create Instance
        info!("Creating Instance:");
        let instance =
            VkInstance::create(entry, &instance_info, use_debug).map_err(|_| NoneError)?;

        //Create Surface
        info!("Creating Surface:");
        let surface = VkSurface::new(Arc::clone(&instance), window, surface_meta)?;

        //List Physical Devices
        info!("Loading Physical Devices:");
        let mut physical_devices = unsafe { instance.instance.enumerate_physical_devices() }
            .map_err(|e| {
                warn!("Failed to Enumerate Physical Devices: {}", e);
                NoneError
            })?
            .into_iter()
            .filter_map(|dev| {
                let device = VkPhysicalDevice::new(Arc::clone(&instance), dev, surface.surface);
                debug!(" Loaded Physical Device Metadata");
                if device.can_support_surface(surface.surface) {
                    debug!(" Found Valid Device");
                    Some(device)
                } else {
                    debug!(" Found Invalid Device");
                    None
                }
            })
            .collect::<Vec<_>>();

        //Ensure we have at least 1 valid device
        info!(" Found {} Valid Physical Devices", physical_devices.len());
        if physical_devices.len() == 0 {
            warn!("No Physical Devices Found");
            return None;
        }

        //Choose Physical Device
        let mut dedicated_count = physical_devices
            .iter()
            .filter(|dev| dev.is_dedicated())
            .count();
        let physical = if dedicated_count == 1 {
            physical_devices
                .into_iter()
                .filter(|dev| dev.is_dedicated())
                .next()
                .unwrap()
        } else {
            physical_devices
                .into_iter()
                .max_by_key(|dev| dev.score_gpu())
                .unwrap()
        };

        //Create Device
        info!("Creating Device:");
        let mut device_ext = vec![extensions::khr::Swapchain::name().to_str().unwrap()];
        let device = VkDevice::create(physical, surface.surface, &layers, &device_ext)?;

        //Create SwapChain
        info!("Creating SwapChain:");
        let swap_chain = VkSwapChain::create_swap_chain(&device, window, &surface)?;

        //Create Global State
        Some(VulkanGlobalState {
            swap_chain,
            surface,
            device,
            instance,
            __private: (),
        })
    }
}

impl Drop for VulkanGlobalState {
    fn drop(&mut self) {
        info!("Starting Vulkan Shutdown");
        info!(" Destroy SwapChain");
        self.swap_chain.destroy_swap_chain(&self.device);
    }
}
