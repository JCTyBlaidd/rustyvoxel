use crate::util::ProgramConfig;
use crate::prelude::*;
use winit::{
	window::{
		Window,
		WindowBuilder
	},
	event_loop::{
		EventLoop,
		ControlFlow
	},
	event::StartCause,
	dpi::LogicalSize
};
use std::time::Instant;

pub mod atlas;
pub use self::atlas::*;

//#deprecated FIXME: REMOVE COMPLETELY
//pub mod shader_old;

pub mod shader;

mod common;
mod vulkan;
//mod opengl
mod world;
mod gui;
mod render;
mod window;
use self::{
	common::BlockVertex,
	vulkan::VulkanBackend,
	//opengl::OpenGLBackend
	gui::{
		GuiBackend,
	},
	world::{
		WorldBackend,
		WorldRender
	},
	render::{
		Backend,
		GlobalRenderBackend,
		GlobalRenderAPI,
		RenderAPI,
	},
	window::{
		WindowBackend,
		WindowHandle,
		WindowMetadata,
	}
};


///IDEAS FOR SHADERS:
///  - optional skybox texture
///  - optional N usages of grid draws
///    - frustrum (no-back prop / with-back-prop) {uses flood fill}
///    - cube around player (require flood fill path / not)
///    - Size for both configurable as optione
///  - each can either just gen tex || gen command buffer
///  - configure draw order in shader description file
///    - with dependencies
///  - allow for generation of tree structure for ray-tracing:
///   - Grid with 1+ each side space for each value
///   - Flag if empty so can skip, else point to data
///   - Maybe split data into grid as well??
///  - Split Opaque & Transparent Draws (Allow Merging As Well)
///  - Generate RenderPass and Shaders Async until Ready
///    - Prevents Freezing
///  - Constants in config file and shaders so simple changes can be used
///    - Allow for removing of conditionals with constants
///      - And detect usage of uniforms etc.., don't generate if necessary

pub use self::window::{
	StateScreen,
	StateSync,
	StateChange,
	GlobalRenderConfig,
	UserEventHandle
};

pub struct GlobalRender {
	backend: Box<dyn GlobalRenderAPI>,
}

impl GlobalRender {
	pub fn new(use_vulkan: bool, config: &ProgramConfig) -> Result<GlobalRender,String> {
		info!("{}",fmt_bold("Creating Window:"));
		let event_loop = EventLoop::new();
		let window: Window = WindowBuilder::new()
			.with_title("Rusty Voxel")
			.with_min_inner_size(LogicalSize::new(50.0,50.0))
			.with_resizable(true)
			.with_visible(true)
			.with_maximized(true) //FIXME: with_window_icon
			.build(&event_loop)
			.map_err(|e| {
				format!("Failed to create window: {:?}",e)
			})?;
		info!("");
		
		//Try Create Vulkan Backend
		let (window,event_loop) = if use_vulkan {
			if let Some(vulkan) = VulkanBackend::is_supported() {
				match VulkanBackend::new(
					vulkan,
					event_loop,
					window,
					config
				) {
					Ok(backend) => {
						return Ok(GlobalRender {
							backend,
						})
					}
					Err(window_and_events) => {
						window_and_events
					}
				}
			}else {
				(window,event_loop)
			}
		}else{
			(window, event_loop)
		};
		
		//Try Create OpenGL Backend
		let _ = (window, event_loop);
		unimplemented!()
	}
	
	
	#[inline]
	pub fn main_loop<H: UserEventHandle, F> (
		&mut self,
		event_handle: &mut H,
		loop_handle: F
	) where F: FnMut(&mut WindowMetadata,&mut StartCause,&mut dyn RenderAPI, bool, bool) -> ControlFlow {
		let (
			event_loop,
			meta,
			render_api
		) = self.backend.get_event_loop_and_meta();
		
		meta.main_loop(
			event_loop,
			event_handle,
			loop_handle,
			render_api
		)
	}
}