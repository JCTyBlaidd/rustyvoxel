use petgraph::{
	Graph,
	graph::NodeIndex
};
use hashbrown::HashMap;
use super::shader_desc::ShaderDescription;
use crate::prelude::*;

/// Resource Format Assignment
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ResourceFormat {
	UNorm8(u8),
	UNorm16(u8),
	SFloat16(u8),
	SFloat32(u8),
	Depth24,
	Depth32,
	DepthStencil,
}

impl ResourceFormat {
	pub fn is_depth_format(&self) -> bool {
		use self::ResourceFormat::*;
		match self {
			Depth24
			| Depth32
			| DepthStencil => true,
			_ => false
		}
	}
}

/// Assigned Size of a resources
///  dimension:
///  scaled to swap-chain or fixed
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ResourceDimension {
	ScaledSwapWidth(u32,u32),
	ScaledSwapHeight(u32,u32),
	FixedSize(u32),
}

impl ResourceDimension {
	pub fn minimize(&mut self) {
		use self::ResourceDimension::*;
		match self {
			ScaledSwapHeight(mul,div)
			| ScaledSwapWidth(mul,div) => {
				//let gcd = u32::gcd(mul,div);
				//mul /= gcd;
				//div /= gcd;
				//FIXME: IMPLEMENT
				unimplemented!()
			},
			FixedSize(_) => ()
		}
	}
	pub fn resolve_size(&self, swap_width: u32, swap_height: u32) -> u32 {
		use self::ResourceDimension::*;
		match self {
			ScaledSwapWidth(mul,div) => (swap_width * mul) / div,
			ScaledSwapHeight(mul,div) => (swap_height * mul) / div,
			FixedSize(size) => *size
		}
	}
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ResourceType {
	/// Import a frame-buffer result
	///  as a dependency, with local
	///  or global scope
	/// Or import an image generated elsewhere
	ImageResource {
		width: ResourceDimension,
		height: ResourceDimension,
		layers_depth: ResourceDimension,
		format: ResourceFormat,
		samples: Option<u32>,
	},
	/// Import a global buffer
	///  FIXME: SUPPORT PROPERLY
	BufferResource {
		buffer_size: u32,
		//usage // persistent?
	}
}

#[repr(u8)]
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum RWUsage {
	NoUsage = 0,
	ReadOnlyUsage = 1,
	WriteOnlyUsage = 2,
	ReadWriteUsage = 1 | 2,
}
pub use RWUsage::*;

impl RWUsage {
	pub fn is_read(&self) -> bool {
		match self {
			ReadOnlyUsage | ReadWriteUsage => true,
			NoUsage | WriteOnlyUsage => false,
		}
	}
	pub fn is_write(&self) -> bool {
		match self {
			WriteOnlyUsage | ReadWriteUsage => true,
			NoUsage | ReadOnlyUsage => false
		}
	}
	pub fn apply(&mut self, other: RWUsage) {
		let self_bits = *self as u8;
		let other_bits = other as u8;
		let merge_bits = self_bits | other_bits;
		use std::mem::transmute;
		//This is safe since the only valid values are 0,1,2
		*self = unsafe {
			transmute(merge_bits)
		}
	}
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct ShaderStagesUsage {
	pub stage_vertex: RWUsage,
	pub stage_geometry: RWUsage,
	pub stage_tess_control: RWUsage,
	pub stage_tess_eval: RWUsage,
	pub stage_fragment: RWUsage,
	pub stage_compute: RWUsage,
	pub transfer_usage: RWUsage,
	pub sampled_image_usage: bool,
	pub storage_image_usage: bool,
	pub colour_attachment_usage: bool,
	pub depth_attachment_usage: bool,
	pub input_attachment_usage: bool,
}

impl Default for ShaderStagesUsage {
	fn default() -> Self {
		ShaderStagesUsage {
			stage_vertex: NoUsage,
			stage_geometry: NoUsage,
			stage_tess_control: NoUsage,
			stage_tess_eval: NoUsage,
			stage_fragment: NoUsage,
			stage_compute: NoUsage,
			transfer_usage: NoUsage,
			sampled_image_usage: false,
			storage_image_usage: false,
			colour_attachment_usage: false,
			depth_attachment_usage: false,
			input_attachment_usage: false
		}
	}
}

impl ShaderStagesUsage {
	pub fn add_usage(&mut self, other: &ShaderStagesUsage) {
		self.stage_vertex.apply(other.stage_vertex);
		self.stage_geometry.apply(other.stage_geometry);
		self.stage_tess_control.apply(other.stage_tess_control);
		self.stage_tess_eval.apply(other.stage_tess_eval);
		self.stage_fragment.apply(other.stage_fragment);
		self.stage_compute.apply(other.stage_compute);
		self.transfer_usage.apply(other.transfer_usage);
		self.sampled_image_usage |= other.sampled_image_usage;
		self.storage_image_usage |= other.storage_image_usage;
		self.colour_attachment_usage |= other.colour_attachment_usage;
		self.depth_attachment_usage |= other.depth_attachment_usage;
		self.input_attachment_usage |= other.input_attachment_usage;
	}
}

/// Resource that is imported for use by the shader,
///  the resource may be used to read and/or write
///  depending on its type, handles the conversion
///  of the resource to the new value given in
///  certain cases.
/// A resource consumed in a write_id=Some(...)
///  shader multiple times will instead be duplicated
///  via a shader copy, so that the assigned resource
///  can be written to correctly
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct ShaderImport {
	/// Id of the resource being imported
	pub id: String,
	
	/// Type of the resource being imported
	pub resource: ResourceType,
	
	/// True if the resource is depended on
	///  in a local manner (can use subpassLoad)
	pub local_dependency: bool,
	
	/// If this resource will be modified,
	///  and if so, what is the identifier of
	///  the new resource given by the new
	///  data in the resource.
	/// This is only valid for non frame
	///  and non buffer resource generation
	pub write_id: Option<String>,
	
	/// Map of shader stage usage,
	///  compute and all the other
	///  shader stages are not
	///  compatible
	pub shader_stages: ShaderStagesUsage,
}

/// Meta-Type for value that will be used
///  to clear a resource
#[derive(Copy, Clone, PartialEq, Debug)]
pub enum ClearValue {
	BoolValue(bool),
	FloatValue(f64),
	SignedValue(i64),
	UnsignedValue(u64),
}

/// Shader Write Target,
///  is only accessed using the fragment
///  shader stage, and is only applicable
///  for graphics pipelines, that write
///  to targets
#[derive(Clone, PartialEq, Debug)]
pub struct ShaderExport {
	pub id: String,
	pub load_from: Option<String>,
	pub clear_with: Option<Vec<ClearValue>>,
	pub export_value: bool,
}

#[derive(Clone, PartialEq, Debug)]
pub struct ShaderMeta {
	shader_src: ShaderDescription,
	shader_imports: Vec<ShaderImport>,
	shader_exports: Vec<ShaderExport>,
}

#[derive(Clone, PartialEq, Debug)]
pub enum PipelineNode {
	/// Execute a shader to
	///  generate information
	ShaderNode {
		shader: ShaderMeta,
		is_compute: bool,
		assigned_queue: Option<u16>,
		assigned_pass: Option<(u16,u16)>
	},
	/// External dependency on
	///  resource generated outside
	///  of this pipeline
	ExternalDependencyNode {
		resource: String,
		resource_type: ResourceType,
	},
	/// A operation to clear a resource
	///  that is not part of a framebuffer
	///  and so cannot be cleared implicitly
	ClearResourceNode {
		resource: String,
		clear_with: Vec<ClearValue>,
		assigned_queue: Option<u16>,
	},
	/// A operation to clone an assigned
	///  resource, creating a new resource
	///  that can be used
	CloneResourceNode {
		src_resource: String,
		src_assignment: Option<u16>,
		dst_resource: String,
		dst_assignment: Option<u16>,
		assigned_queue: Option<u16>,
	},
	/// A node that represents the state
	///  of a resource that is in use
	ResourceNode {
		resource: String,
		resource_type: ResourceType,
		resource_usage: ShaderStagesUsage,
		resource_assignment: Option<u16>,
	}
}

#[derive(Clone, Debug)]
pub struct RenderGraph {
	//edge = is_local_dependency (and hence can be merged)
	pub graph: Graph<PipelineNode,bool>,
	//pub resources: HashMap<u16,NodeIndex>,
	//pub render_passes: HashMap<u16,RenderPassInfo>
	//pub submissions: HashMap<u16, SubmissionInfo>
}

pub enum RenderGraphError {
	DifferentFormatSizes(String),
	UnspecifiedResource(String),
	DifferingResourceSizes(String),
	IncorrectResourceType(String),
	MultipleDepthTypes(String),
}


/// Generate & Schedule Dependency Graph
///  for the given Frame with Metadata:
///  Shaders: the set of shaders being processed
///
pub fn generate_shader_graph(
	shaders: Vec<ShaderMeta>,
	resource_types: HashMap<String,ResourceType>,
) -> Result<RenderGraph,RenderGraphError> {
	let mut graph = Graph::new();
	
	//List all resources: [generate where necessary & validate shader exports]
	let mut resource_nodes: HashMap<String, (ResourceType,ShaderStagesUsage)> = resource_types.iter()
		.map(|(res_id,res)| {
			(res_id.clone(),(res.clone(),ShaderStagesUsage::default()))
	}).collect();
	for shader in &shaders {
		
		//Validate Shader Exports
		let mut found_depth_export = false;
		for export in &shader.shader_exports {
			
			//Must have a specified image resource
			let associated_resource = match resource_types.get(&export.id) {
				Some(v) => v,
				None => return Err(RenderGraphError::UnspecifiedResource(export.id.clone()))
			};
			if let ResourceType::ImageResource {
				format,
				samples,
				..
			} = associated_resource {
				
				//Limit of 1 depth format
				if format.is_depth_format() {
					if !found_depth_export {
						found_depth_export = true;
					}else{
						return Err(RenderGraphError::MultipleDepthTypes(export.id.clone()));
					}
				}
				
				//Validate dimension specification
				//FIXME: Add MultiSampling Correctly
				let export_resource = ResourceType::ImageResource {
					width: shader.shader_src.get_target_width(),
					height: shader.shader_src.get_target_height(),
					layers_depth: shader.shader_src.get_target_layers(),
					format: *format,
					samples: None
				};
				let mut export_usage = ShaderStagesUsage::default();
				if format.is_depth_format() {
					export_usage.depth_attachment_usage = true;
				}else{
					export_usage.colour_attachment_usage = true;
				}
				export_usage.stage_fragment = ReadWriteUsage;
				if let Some((res,usage)) = resource_nodes.get_mut(&export.id) {
					if res != &export_resource {
						return Err(RenderGraphError::DifferentFormatSizes(export.id.clone()));
					}
					usage.add_usage(&export_usage);
				}else{
					//Resource MUST be specified: Asserted Earlier
					unreachable!()
				}
			}else{
				return Err(RenderGraphError::IncorrectResourceType(export.id.clone()));
			}
		}
		
		//Validate shader imports
		for import in &shader.shader_imports {
			
			//Handle Resource Loading
			let mut new_usage = import.shader_stages.clone();
			if import.local_dependency {
				new_usage.input_attachment_usage = true;
			}
			if import.write_id.is_some() {
				//USAGE: FIXME
				panic!("Usage Not Implemented");
			}
			if let Some((res,usage)) = resource_nodes.get_mut(&import.id) {
				if res != &import.resource {
					return Err(RenderGraphError::DifferentFormatSizes(import.id.clone()));
				}
				
				usage.add_usage(&new_usage);
			}else{
				resource_nodes.insert(import.id.clone(),(import.resource.clone(),new_usage.clone()));
			}
			
			//Handle Resource Writing
			if let Some(res_write) = &import.write_id {
				if let Some((res,usage)) = resource_nodes.get_mut(res_write) {
					if res != &import.resource {
						return Err(RenderGraphError::DifferentFormatSizes(res_write.clone()));
					}
					usage.add_usage(&new_usage);
				}else{
					resource_nodes.insert(res_write.clone(),(import.resource.clone(),new_usage.clone()));
				}
			}
		}
	}
	
	//Create Resource Nodes
	let resource_nodes: HashMap<String,_> = resource_nodes.iter()
		.map(|(res_id,res_data)| {
		let node_id = graph.add_node(
				PipelineNode::ResourceNode {
					resource: res_id.clone(),
					resource_type: res_data.0.clone(),
					resource_usage: res_data.1.clone(),
					resource_assignment: None
				}
		);
		(res_id.clone(), node_id)
	}).collect();
	
	//Create Shader Nodes
	let shader_nodes : Vec<_> = shaders.into_iter().map(|shader| {
		let is_compute = shader.shader_src.is_compute_shader();
		graph.add_node(PipelineNode::ShaderNode {
			shader,
			is_compute,
			assigned_queue: None,
			assigned_pass: None
		})
	}).collect();
	
	//Generate Shader Dependencies: Add Non Consume Deps, Mark Consume Deps
	/*
	let mut shader_consume_dependencies: HashMap<String,Vec<String>> = HashMap::new();
	for shader_node in shader_nodes.iter() {
		//let mut deps = Vec::new();
		//let mut prods = Vec::new();
		if let PipelineNode::ShaderNode {
			shader,
			..
		} = graph.node_weight(*shader_node).unwrap() {
			for import in &shader.shader_imports {
				if import.write_id.is_none() {
					//import.
					let resource = resource_nodes.get(&import.id);
				}else{
					//Add Consume Dependencies
				}
			}
			
		}else{
			unreachable!();
		};
	}
	*/
	
	//Generate Shader Dependencies: Add Consume Deps w/ Duplicate Deps if Required
	
	//Assign Resources: Pass through graph & track resource usage, alias where possible
	//let mut resources = HashMap::new();
	
	//Update resource usage metadata:
	
	//Assign RenderPasses: Find local dependency chains and duplicate assignments if applicable
	// with fallback to unique pass per usage!?
	
	//Assign Chosen Pipelines
	// with fallback to use 1 pipeline with lots of synchronization
	// but that does not allow async-compute, etc.
	
	Ok(RenderGraph {
		graph,
	})
}