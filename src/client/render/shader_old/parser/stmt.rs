use super::*;


pub fn parse_if_statement<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> StmtResult<'l,E> {
	map(
		tuple(
			(
				preceded(
					tok(Token::TokIf),
					parse_expression
				),
				delimited(
					tok(Token::TokLSection),
					parse_statement_list,
					tok(Token::TokRSection)
				),
				opt(
					preceded(
						tok(Token::TokElse),
						alt(
							(
								delimited(
									tok(Token::TokLSection),
									parse_statement_list,
									tok(Token::TokRSection)
								),
								map(
									parse_if_statement,
									|e| {
										let mut stmts = Vec::new();
										stmts.push(e);
										stmts
									}
								)
							)
						)
					)
				)
			)
		),
		|v| {
			let else_stmt = v.2.unwrap_or(Vec::new());
			Box::new(
				Statement::IfStatement {
					cond: v.0,
					left: v.1,
					right: else_stmt
				}
			)
		}
	)(i)
}
pub fn parse_statement<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> StmtResult<'l,E> {
	alt(
		(
			map(
				tuple(
					(
						preceded(
							tok(Token::TokLet),
							tok_ident
						),
						opt(
							tok(Token::TokConst)
						),
						opt(
							tok(Token::TokExtern)
						),
						preceded(
							tok(Token::TokColon),
							parse_adv_type,
						),
						delimited(
							tok(Token::TokAssign),
							parse_expression,
							tok(Token::TokSemiColon)
						)
					)
				),
				|v| {
					Box::new(
						Statement::LetStatement {
							target: v.0,
							modifier_const: v.1.is_some(),
							modifier_extern: v.2.is_some(),
							target_type: v.3,
							assign: v.4
						}
					)
				}
			),
			map(
				tuple(
					(
						parse_expr_place,
						alt(
							(
								tok_to(Token::TokAssign, AssignOp::Assign),
								tok_to(Token::TokAddEq, AssignOp::AssignAdd),
								tok_to(Token::TokSubEq, AssignOp::AssignSub),
								tok_to(Token::TokMulEq, AssignOp::AssignMul),
								tok_to(Token::TokDivEq, AssignOp::AssignDiv),
								tok_to(Token::TokRemEq, AssignOp::AssignRem),
								//tok_to(Token::TokLShiftEq),
								//tok_to(Token::TokRShiftEq),
								tok_to(Token::TokAndEq, AssignOp::AssignBitwiseAnd),
								tok_to(Token::TokXorEq, AssignOp::AssignBitwiseXor),
								tok_to(Token::TokOrEq, AssignOp::AssignBitwiseOr)
							)
						),
						terminated(
							parse_expression,
							tok(Token::TokSemiColon)
						)
					)
				),
				|v| {
					Box::new(
						Statement::AssignStatement {
							target: v.0,
							op: v.1,
							assign: v.2
						}
					)
				}
			),
			map(
				delimited(
					tok(Token::TokReturn),
					parse_expression,
					tok(Token::TokSemiColon)
				),
				|v| {
					Box::new(
						Statement::ReturnStatement {
							value: v
						}
					)
				}
			),
			map(
				tuple(
					(
						preceded(
							tok(Token::TokWhile),
							parse_expression
						),
						delimited(
							tok(Token::TokLSection),
							parse_statement_list,
							tok(Token::TokRSection)
						)
					)
				),
				|v| {
					Box::new(
						Statement::WhileStatement {
							cond: v.0,
							inner: v.1
						}
					)
				}
			),
			parse_if_statement
		)
	)(i)
}

pub fn parse_statement_list<'l,E: ParseError<&'l[Token]>>(
	i: &'l[Token]
) -> ParseResult<'l,Vec<Box<Statement>>,E> {
	many0(parse_statement)(i)
}