use super::*;

pub fn parse_expr_constant<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			map(
				tok_bool,
				|v| {
					TaggedExpression::new(
						Expression::BooleanConstant(v)
					)
				}
			),
			map(
				tok_int,
				|v| {
					TaggedExpression::new(
						Expression::UnsignedIntConstant(v)
					)
				}
			),
			map(
				tok_float,
				|v| {
					TaggedExpression::new(
						Expression::DoubleConstant(v)
					)
				}
			),
			map(
				tok_ident,
				|v| {
					TaggedExpression::new(
						Expression::IdentConstant(v)
					)
				}
			),
			delimited(
				tok(Token::TokLBracket),
				parse_expression,
				tok(Token::TokRBracket)
			)
		)
	)(i)
}

pub fn parse_expr0<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr_constant,
			map(
				tuple(
					(
						parse_expr0,
						delimited(
							tok(Token::TokLSquare),
							parse_expression,
							tok(Token::TokRSquare)
						)
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::ArrayAccessExpr {
							target: v.0,
							index: v.1
						}
					)
				}
			),
			map(
				tuple(
					(
						parse_expr0,
						delimited(
							tok(Token::TokLBracket),
							separated_list(
								tok(Token::TokComma),
								parse_expression
							),
							tok(Token::TokRBracket)
						)
					)
				),
				|v: (Box<TaggedExpression>, Vec<Box<TaggedExpression>>)| {
					TaggedExpression::new(
						Expression::FuncCallExpr {
							target: v.0,
							args: v.1,
						}
					)
				}
			),
			map(
				tuple(
					(
						parse_expr0,
						preceded(
							tok(Token::TokDot),
							tok_ident
						)
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::FieldAccessExpr {
							target: v.0,
							field: v.1
						}
					)
				}
			)
		)
	)(i)
}

///Special Expr for Assignment Location
pub fn parse_expr_place<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			map(
				tok_ident,
				|v| {
					TaggedExpression::new(
						Expression::IdentConstant(v)
					)
				}
			),
			map(
				tuple(
					(
						parse_expr_place,
						delimited(
							tok(Token::TokLSquare),
							parse_expression,
							tok(Token::TokRSquare)
						)
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::ArrayAccessExpr {
							target: v.0,
							index: v.1
						}
					)
				}
			),
			map(
				tuple(
					(
						parse_expr_place,
						delimited(
							tok(Token::TokLBracket),
							separated_list(
								tok(Token::TokComma),
								parse_expression
							),
							tok(Token::TokRBracket)
						)
					)
				),
				|v: (Box<TaggedExpression>, Vec<Box<TaggedExpression>>)| {
					TaggedExpression::new(
						Expression::FuncCallExpr {
							target: v.0,
							args: v.1,
						}
					)
				}
			),
			map(
				tuple(
					(
						parse_expr_place,
						preceded(
							tok(Token::TokDot),
							tok_ident
						)
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::FieldAccessExpr {
							target: v.0,
							field: v.1
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr1<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr0,
			map(
				tuple(
					(
						tok_type,
						delimited(
							tok(Token::TokLBracket),
							separated_list(
								tok(Token::TokComma),
								parse_expression
							),
							tok(Token::TokRBracket)
						)
					)
				),
				|v: (BasicType, Vec<Box<TaggedExpression>>)| {
					TaggedExpression::new(
						Expression::TypeConstructExpr {
							target_type: v.0,
							args: v.1
						}
					)
				}
			),
			map(
				tuple(
					(
						parse_expr1,
						alt(
							(
								tok_to(Token::TokAddAdd, UnaryOp::PostfixInc),
								tok_to(Token::TokSubSub, UnaryOp::PostfixDec)
							)
						)
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::UnaryExpr {
							op: v.1,
							expr: v.0
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr2<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr1,
			map(
				tuple(
					(
						alt(
							(
								tok_to(Token::TokAddAdd, UnaryOp::PrefixDec),
								tok_to(Token::TokSubSub, UnaryOp::PostfixDec),
								tok_to(Token::TokBitNot, UnaryOp::BitwiseNot),
								tok_to(Token::TokBang, UnaryOp::Not),
								tok_to(Token::TokSub, UnaryOp::Negate)
							)
						),
						parse_expr1
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::UnaryExpr {
							op: v.0,
							expr: v.1
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr3<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr2,
			map(
				tuple(
					(
						parse_expr3,
						alt(
							(
								tok_to(Token::TokMul, BinaryOp::Mul),
								tok_to(Token::TokDiv, BinaryOp::Div),
								tok_to(Token::TokRem, BinaryOp::Rem)
							)
						),
						parse_expr2,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::BinaryExpr {
							left: v.0,
							op: v.1,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr4<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr3,
			map(
				tuple(
					(
						parse_expr4,
						alt(
							(
								tok_to(Token::TokAdd, BinaryOp::Add),
								tok_to(Token::TokSub, BinaryOp::Sub)
							)
						),
						parse_expr3,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::BinaryExpr {
							left: v.0,
							op: v.1,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr5<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr4,
			map(
				tuple(
					(
						parse_expr5,
						alt(
							(
								tok_to(Token::TokLShift, BinaryOp::ShiftLeft),
								tok_to(Token::TokRShift, BinaryOp::ShiftRight)
							)
						),
						parse_expr4,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::BinaryExpr {
							left: v.0,
							op: v.1,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr6<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr5,
			map(
				tuple(
					(
						parse_expr5,
						alt(
							(
								tok_to(Token::TokLT, CmpBinaryOp::LessThan),
								tok_to(Token::TokLTEq, CmpBinaryOp::LessThanEqual),
								tok_to(Token::TokGT, CmpBinaryOp::GreaterThan),
								tok_to(Token::TokGTEq, CmpBinaryOp::GreaterThanEqual),
								tok_to(Token::TokEqual, CmpBinaryOp::Equal),
								tok_to(Token::TokNotEqual, CmpBinaryOp::NotEqual)
							)
						),
						parse_expr5,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::CmpBinaryExpr {
							left: v.0,
							op: v.1,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr7<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr6,
			map(
				tuple(
					(
						parse_expr7,
						tok(Token::TokAnd),
						parse_expr6,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::BinaryExpr {
							left: v.0,
							op: BinaryOp::BitwiseAnd,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr8<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr7,
			map(
				tuple(
					(
						parse_expr8,
						tok(Token::TokXor),
						parse_expr7,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::BinaryExpr {
							left: v.0,
							op: BinaryOp::BitwiseXor,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr9<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr8,
			map(
				tuple(
					(
						parse_expr9,
						tok(Token::TokOr),
						parse_expr8,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::BinaryExpr {
							left: v.0,
							op: BinaryOp::BitwiseOr,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr10<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr9,
			map(
				tuple(
					(
						parse_expr10,
						tok(Token::TokAndAnd),
						parse_expr9,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::CmpBinaryExpr {
							left: v.0,
							op: CmpBinaryOp::LogicalAnd,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr11<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr10,
			map(
				tuple(
					(
						parse_expr11,
						tok(Token::TokXorXor),
						parse_expr10,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::CmpBinaryExpr {
							left: v.0,
							op: CmpBinaryOp::LogicalXor,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expr12<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr11,
			map(
				tuple(
					(
						parse_expr12,
						tok(Token::TokOrOr),
						parse_expr11,
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::CmpBinaryExpr {
							left: v.0,
							op: CmpBinaryOp::LogicalOr,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}

pub fn parse_expression<'l, E: ParseError<&'l [Token]>>(i: &'l [Token]) -> ExprResult<'l, E> {
	alt(
		(
			parse_expr12,
			map(
				tuple(
					(
						parse_expression,
						delimited(
							tok(Token::TokQuestion),
							parse_expr12,
							tok(Token::TokColon)
						),
						parse_expr12
					)
				),
				|v| {
					TaggedExpression::new(
						Expression::TernaryExpr {
							cond: v.0,
							left: v.1,
							right: v.2
						}
					)
				}
			)
		)
	)(i)
}
