use super::parser_ir::*;
use nom::{
	IResult,
	combinator::{
		map,
		map_res,
		opt,
	},
	multi::{
		many0,
		many1,
		separated_list,
		many_m_n,
	},
	branch::alt,
	sequence::{
		tuple,
		delimited,
		preceded,
		terminated
	},
	error::{
		ParseError,
		ErrorKind,
	},
	Err as NomErr,
	InputTake
};

///Lexer, input is passed to parser
pub mod lexer;
use self::lexer::Token;

mod expr;
mod stmt;
mod decl;
use self::expr::*;
use self::stmt::*;
use self::decl::*;


pub fn parse_file<'l,E: ParseError<&'l[Token]>>(
	i: &'l[Token]
) -> ParseResult<'l,Vec<Declaration>, E> {
	many0(parse_declaration)(i)
}


type ParseResult<'l,T,E> = IResult<&'l [Token],T,E>;
type ExprResult<'l,E> = ParseResult<'l,Box<TaggedExpression>,E>;
type StmtResult<'l,E> = ParseResult<'l,Box<Statement>,E>;

fn tok<'l, E: ParseError<&'l [Token]>>(tok: Token) -> impl Fn(&'l [Token]) -> ParseResult<'l,(),E> {
	move |i : &[Token]| {
		if let Some((t,r)) = i.split_first() {
			if t.eq(&tok) {
				return Ok((r,()))
			}
		}
		Err(NomErr::Error(E::from_error_kind(i,ErrorKind::Tag)))
	}
}

fn tok_to<'l,T: Copy, E: ParseError<&'l [Token]>>(
	tok: Token,
	res: T
) -> impl Fn(&'l [Token]) -> ParseResult<'l,T,E> {
	move |i : &[Token]| {
		if let Some((t,r)) = i.split_first() {
			if t.eq(&tok) {
				return Ok((r,res))
			}
		}
		Err(NomErr::Error(E::from_error_kind(i,ErrorKind::Tag)))
	}
}

fn tok_bool<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,bool, E> {
	if let Some((t,r)) = i.split_first() {
		if let Token::TokBoolConstant(val) = t {
			return Ok((r,*val));
		}
	}
	Err(NomErr::Error(E::from_error_kind(i,ErrorKind::Tag)))
}

fn tok_int<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,u64, E> {
	if let Some((t,r)) = i.split_first() {
		if let Token::TokIntegralConstant(val) = t {
			return Ok((r,*val));
		}
	}
	Err(NomErr::Error(E::from_error_kind(i,ErrorKind::Tag)))
}

fn tok_float<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,f64, E> {
	if let Some((t,r)) = i.split_first() {
		if let Token::TokFloatingConstant(val) = t {
			return Ok((r,*val));
		}
	}
	Err(NomErr::Error(E::from_error_kind(i,ErrorKind::Tag)))
}

fn tok_type<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,BasicType, E> {
	if let Some((t,r)) = i.split_first() {
		if let Token::TokType(val) = t {
			return Ok((r,*val));
		}
	}
	Err(NomErr::Error(E::from_error_kind(i,ErrorKind::Tag)))
}

fn tok_ident<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,String, E> {
	if let Some((t,r)) = i.split_first() {
		if let Token::TokIdent(val) = t {
			return Ok((r,val.clone()));
		}
	}
	Err(NomErr::Error(E::from_error_kind(i,ErrorKind::Tag)))
}
