use std::str::Chars;
use std::iter::Peekable;
use super::types::BasicType;


#[derive(PartialEq, Clone, Debug)]
pub enum Token {
	/// "true" | "false
	TokBoolConstant(bool),
	/// 0,1,2,...
	TokIntegralConstant(u64),
	/// 1.0,1.1,...
	TokFloatingConstant(f64),
	/// "if"
	TokIf,
	/// "else"
	TokElse,
	/// "while"
	TokWhile,
	/// "switch"
	TokSwitch,
	/// "fn"
	TokFn,
	/// "return"
	TokReturn,
	/// "inline"
	TokInline,
	/// "input"
	TokInput,
	/// "output"
	TokOutput,
	/// "uniform"
	TokUniform,
	/// "push_constant"
	TokPushConstant,
	/// "load_from"
	TokLoadFrom,
	/// "clear"
	TokClear,
	/// "undefined"
	TokUndefined,
	/// "local"
	TokLocal,
	/// "global"
	TokGlobal,
	/// "sample"
	TokSample,
	/// "let"
	TokLet,
	/// "extern"
	TokExtern,
	/// "const
	TokConst,
	/// "dyn"
	TokDyn,
	/// "shader
	TokShader,
	/// "vertex"
	TokVertex,
	/// "geometry"
	TokGeometry,
	/// "tess_eval"
	TokTessEval,
	/// "tess_control"
	TokTessControl,
	/// "fragment"
	TokFragment,
	/// "compute"
	TokCompute,
	/// "vec2","ivec4","dmat2","sampler2D", ...
	TokType(BasicType),
	/// "IDENT",...
	TokIdent(String),
	/// '+'
	TokAdd,
	/// '++'
	TokAddAdd,
	/// '+='
	TokAddEq,
	/// '-'
	TokSub,
	/// '--'
	TokSubSub,
	/// '-='
	TokSubEq,
	/// '*'
	TokMul,
	/// '*='
	TokMulEq,
	/// '/'
	TokDiv,
	/// '/='
	TokDivEq,
	/// '%'
	TokRem,
	/// %=
	TokRemEq,
	/// '&'
	TokAnd,
	/// '&&'
	TokAndAnd,
	/// '&='
	TokAndEq,
	/// '|'
	TokOr,
	/// '||'
	TokOrOr,
	/// '|='
	TokOrEq,
	/// '^'
	TokXor,
	/// '^^'
	TokXorXor,
	/// '^='
	TokXorEq,
	/// '~'
	TokBitNot,
	/// '<'
	TokLT,
	/// '<<'
	TokLShift,
	/// '<='
	TokLTEq,
	/// '>'
	TokGT,
	/// '>>'
	TokRShift,
	/// '>='
	TokGTEq,
	/// '='
	TokAssign,
	/// '=='
	TokEqual,
	/// '!'
	TokBang,
	/// '!='
	TokNotEqual,
	/// '#'
	TokHash,
	/// '#!'
	TokHashBang,
	/// ','
	TokComma,
	/// ';'
	TokSemiColon,
	/// ':'
	TokColon,
	/// '?'
	TokQuestion,
	/// '.'
	TokDot,
	/// '('
	TokLBracket,
	/// ')'
	TokRBracket,
	/// '{'
	TokLSection,
	/// '}'
	TokRSection,
	/// '['
	TokLSquare,
	/// ']'
	TokRSquare,
}

pub struct Lexer<'src> {
	input: Peekable<Chars<'src>>,
	lex_list: Vec<Token>,
}

impl<'src> Lexer<'src> {
	
	#[cold]
	#[inline(never)]
	fn calc_parsed(&mut self, input: &str) -> usize {
		let original_count = input.chars().count();
		let remaining = self.input.clone().count();
		original_count - remaining
	}
	
	pub fn run_lexer(input: &'src str) -> Result<Vec<Token>,usize> {
		let mut lexer = Lexer {
			input: input.chars().peekable(),
			lex_list: Vec::new()
		};
		if lexer.execute_lexer() {
			Ok(lexer.lex_list)
		}else{
			Err(lexer.calc_parsed(input))
		}
	}
	
	#[inline(always)]
	fn consume_peeked(&mut self) {
		let _ = self.input.next();
	}
	
	#[inline(always)]
	fn peek(&mut self) -> Option<char> {
		self.input.peek().copied()
	}
	
	fn run_lex(&mut self, tok: Token) {
		self.consume_peeked();
		self.lex_list.push(tok)
	}
	
	fn op_eq(&mut self, tok: Token, alt: Token) {
		self.consume_peeked();
		if let Some('=') = self.peek() {
			self.consume_peeked();
			self.lex_list.push(alt);
		}else{
			self.lex_list.push(tok);
		}
	}
	
	fn op_alt(&mut self, tok: Token, next: char,  alt: Token) {
		self.consume_peeked();
		if self.peek() == Some(next) {
			self.consume_peeked();
			self.lex_list.push(alt);
		}else{
			self.lex_list.push(tok);
		}
	}
	
	fn op_lex(&mut self, next: char, tok: Token, alt: Token, eq: Token) {
		self.consume_peeked();
		if let Some(peek) = self.peek() {
			if peek == next {
				self.consume_peeked();
				self.lex_list.push(alt);
			}else if peek == '=' {
				self.consume_peeked();
				self.lex_list.push(eq);
			}else{
				self.lex_list.push(tok);
			}
		}else{
			self.lex_list.push(tok);
		}
	}
	
	fn run_numeric(&mut self, value: char) -> bool {
		let mut build = String::with_capacity(16);
		let _ = self.input.next();
		build.push(value);
		
		//Flags to stop 000.000e000 (multi character zeros)
		let mut is_zero_num = value == '0';
		let mut is_zero_dec = false;
		let mut is_zero_exp = false;
		let mut write_is_zero = &mut is_zero_num;
		
		//0="123", 1="123.", 2="123.123", 3="123.123e",
		//4="123.123e123", 5="123.123.e-", 6="123.123e-123"
		let mut parse_mode = 0;
		
		while let Some(symbol) = self.peek() {
			match symbol {
				'0'..='9' => {
					let _ = self.input.next();
					build.push(symbol);
					
					//Move to even parse mode (0,2,4,6)
					if parse_mode % 2 == 1 {
						parse_mode += 1;
						*write_is_zero = symbol == '0';
					}else if *write_is_zero && symbol == '0' {
						//"00" etc..
						return false;
					}
				},
				'.' if parse_mode == 0 => {
					let _ = self.input.next();
					build.push('e');
					
					parse_mode = 1;
					write_is_zero = &mut is_zero_dec;
				},
				'e' | 'E' if parse_mode == 2 => {
					let _ = self.input.next();
					build.push('e');
					
					parse_mode = 3;
					write_is_zero = &mut is_zero_exp;
				},
				'-' if parse_mode == 3 => {
					let _ = self.input.next();
					build.push('-');
					
					parse_mode = 5;
				},
				_ => return false
			}
		}
		
		
		//Return if even(0,2,4,6) {and mode is therefore valid}
		if parse_mode % 2 == 0 {
			if parse_mode == 0 {
				//Integer
				match build.parse::<u64>() {
					Ok(v) => {
						self.lex_list.push(Token::TokIntegralConstant(v));
						true
					},
					Err(_) => false
				}
			}else{
				//Float
				match build.parse::<f64>() {
					Ok(v) => {
						self.lex_list.push(Token::TokFloatingConstant(v));
						true
					},
					Err(_) => false
				}
			}
		}else{
			false
		}
	}
	
	fn run_ident(&mut self, value: char) {
		let mut build = String::with_capacity(16);
		let _ = self.input.next();
		build.push(value);
		while let Some(symbol) = self.peek() {
			match symbol {
				'_' | 'a'..='z' | 'A'..='Z' | '0'..='9' => {
					let _ = self.input.next();
					build.push(symbol);
				},
				_ => break
			}
		}
		
		if let Some(tok) = self.match_keyword(&build) {
			self.lex_list.push(tok);
		}else if let Some(t) = BasicType::from_ident(&build) {
			self.lex_list.push(Token::TokType(t));
		}else{
			self.lex_list.push(Token::TokIdent(build));
		}
	}
	
	
	fn match_keyword(&mut self, word: &str) -> Option<Token> {
		static KEYWORD_MAP : [(&'static str,Token); 30] = [
			("clear",Token::TokClear),
			("compute",Token::TokCompute),
			("const",Token::TokConst),
			("dyn",Token::TokDyn),
			("else",Token::TokElse),
			("extern",Token::TokExtern),
			("false",Token::TokBoolConstant(false)),
			("fn",Token::TokFn),
			("fragment",Token::TokFragment),
			("geometry",Token::TokGeometry),
			("global",Token::TokGlobal),
			("if",Token::TokIf),
			("inline",Token::TokInline),
			("input",Token::TokInput),
			("let",Token::TokLet),
			("load_from",Token::TokLoadFrom),
			("local",Token::TokLocal),
			("output",Token::TokOutput),
			("push_constant",Token::TokPushConstant),
			("return",Token::TokReturn),
			("sample",Token::TokSample),
			("shader",Token::TokShader),
			("switch",Token::TokSwitch),
			("tess_control",Token::TokTessControl),
			("tess_eval",Token::TokTessEval),
			("true",Token::TokBoolConstant(true)),
			("undefined",Token::TokUndefined),
			("uniform",Token::TokUniform),
			("while",Token::TokWhile),
			("vertex",Token::TokVertex),
		];
		match KEYWORD_MAP.binary_search_by_key(&word,|entry| {
			entry.0
		}) {
			Ok(found_index) => {
				Some(KEYWORD_MAP[found_index].1.clone())
			},
			Err(_) => None
		}
	}
	
	fn execute_lexer(&mut self) -> bool {
		use self::Token::*;
		while let Some(symbol) = self.peek() {
			match symbol {
				' ' | '\t' | '\r' | '\n' => self.consume_peeked(),
				'+' => self.op_lex('+',TokAdd,TokAddAdd,TokAddEq),
				'-' => self.op_lex('-',TokSub,TokSubSub,TokSubEq),
				'*' => self.op_eq(TokMul,TokMulEq),
				'/' => self.op_eq(TokDiv,TokDivEq),
				'%' => self.op_eq(TokRem,TokRemEq),
				'&' => self.op_lex('&',TokAnd,TokAndAnd,TokAndEq),
				'|' => self.op_lex('|',TokOr,TokOrOr,TokOrEq),
				'^' => self.op_lex('^',TokXor,TokXorXor,TokXorEq),
				'~' => self.run_lex(TokBitNot),
				'<' => self.op_lex('<',TokLT,TokLShift,TokLTEq),
				'>' => self.op_lex('>',TokGT,TokRShift,TokGTEq),
				'=' => self.op_eq(TokAssign,TokEqual),
				'!' => self.op_eq(TokBang,TokNotEqual),
				'#' => self.op_alt(TokHash,'!',TokHashBang),
				',' => self.run_lex(TokComma),
				';' => self.run_lex(TokSemiColon),
				':' => self.run_lex(TokColon),
				'?' => self.run_lex(TokQuestion),
				'.' => self.run_lex(TokDot),
				'(' => self.run_lex(TokLBracket),
				')' => self.run_lex(TokRBracket),
				'{' => self.run_lex(TokLSection),
				'}' => self.run_lex(TokRSection),
				'[' => self.run_lex(TokLSquare),
				']' => self.run_lex(TokRSquare),
				'0'..='9' => if !self.run_numeric(symbol) {
					return false
				},
				'_' | 'a'..='z' | 'A'..='Z' => self.run_ident(symbol),
				_ => return false
			}
		}
		return true
	}
	
}