use super::*;

pub fn parse_adv_type<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,Box<AdvType>,E> {
	alt(
		(
			map(
				tok_type,
				|v| {
					Box::new(AdvType::Basic(v))
				}
			),
			map(
				tok_ident,
				|v| {
					Box::new(AdvType::UnknownStructureType(v))
				}
			),
			map(
				tuple(
					(
						tok(Token::TokLSquare),
						parse_adv_type,
						tok(Token::TokSemiColon),
						alt(
							(
								tok_to(Token::TokDyn,None),
								map(tok_int,Option::<u64>::Some)
							)
						),
						tok(Token::TokRSquare),
					)
				),
				|v| {
					match v.3 {
						Some(size) => {
							Box::new(AdvType::FixedSizeArray(v.1,size))
						},
						None => {
							Box::new(AdvType::UnknownSizeArray(v.1))
						}
					}
				}
			)
		)
	)(i)
}

pub fn parse_global_pragma<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,Declaration,E> {
	map(
		tuple(
			(
				tok(Token::TokHashBang),
				preceded(
					tok(Token::TokLSquare),
					tok_ident
				),
				delimited(
					tok(Token::TokLBracket),
					tok_ident,
					tok(Token::TokRBracket)
				),
				tok(Token::TokRSquare),
			)
		),
		|v| {
			Declaration::GlobalPragma {
				key: v.1,
				value: v.2
			}
		}
	)(i)
}

pub fn parse_pragma<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,(String,String),E> {
	preceded(
		tok(Token::TokHash),
		delimited(
			tok(Token::TokLSquare),
			tuple(
				(
					tok_ident,
					delimited(
						tok(Token::TokLBracket),
						tok_ident,
						tok(Token::TokRBracket),
					)
				)
			),
			tok(Token::TokRSquare)
		)
	)(i)
}

pub fn parse_global_input<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,Declaration,E> {
	map(
		tuple(
			(
				many0(parse_pragma),
				tok(Token::TokInput),
				tok(Token::TokLBracket),
				alt(
					(
						tok_to(Token::TokVertex,GlobalInputType::VertexInput),
						tok_to(Token::TokUniform,GlobalInputType::UniformInput),
						tok_to(Token::TokPushConstant,GlobalInputType::PushConstantInput)
					)
				),
				tok(Token::TokRBracket),
				alt(
					(
						map(tok(Token::TokSemiColon),|_| None),
						map(
							delimited(
								tok(Token::TokLSection),
								separated_list(
									tok(Token::TokComma),
									tuple(
										(
											tok_ident,
											preceded(
												tok(Token::TokAssign),
												parse_adv_type
											)
										)
									)
								),
								tok(Token::TokRSection)
							),
							Some
						)
					)
				)
			)
		),
		|v| {
			Declaration::GlobalInputDecl {
				pragmas: v.0.into_iter().collect(),
				input_type: v.3,
				decl_list: v.5
			}
		}
	)(i)
}

pub fn parse_shader_decl<'l,E: ParseError<&'l[Token]>>(i: &'l[Token]) -> ParseResult<'l,Declaration,E> {
	map(
		tuple(
			(
				many0(parse_pragma),
				delimited(
					tok(Token::TokShader),
					alt(
						(
							tok_to(Token::TokVertex,ShaderDeclType::VertexShader),
							tok_to(Token::TokGeometry,ShaderDeclType::GeometryShader),
							tok_to(Token::TokTessControl,ShaderDeclType::TessControlShader),
							tok_to(Token::TokTessEval,ShaderDeclType::TessEvalShader),
							tok_to(Token::TokFragment,ShaderDeclType::FragmentShader)
						)
					),
					tok(Token::TokAssign)
				),
				delimited(
					tok(Token::TokLSection),
					parse_statement_list,
					tok(Token::TokRSection)
				)
			)
		),
		|v| {
			Declaration::ShaderDecl {
				pragmas: v.0.into_iter().collect(),
				shader_type: v.1,
				statements: v.2
			}
		}
	)(i)
}

enum ConstantResult {
	Signed(i64),
	Unsigned(u64),
	Float(f64),
}
impl ConstantResult {
	fn make_signed(x: &[ConstantResult]) -> Option<Vec<i64>> {
		let mut out = Vec::new();
		for v in x {
			match v {
				ConstantResult::Signed(vv) => out.push(*vv),
				_ => return None
			}
		}
		Some(out)
	}
	fn make_unsigned(x: &[ConstantResult]) -> Option<Vec<u64>> {
		let mut out = Vec::new();
		for v in x {
			match v {
				ConstantResult::Unsigned(vv) => out.push(*vv),
				_ => return None
			}
		}
		Some(out)
	}
	fn make_float(x: &[ConstantResult]) -> Option<Vec<f64>> {
		let mut out = Vec::new();
		for v in x {
			match v {
				ConstantResult::Float(vv) => out.push(*vv),
				_ => return None
			}
		}
		Some(out)
	}
}

fn parse_local_constant<'l,E: ParseError<&'l[Token]>>(
	i: &'l[Token]
) -> ParseResult<'l,ConstantResult,E> {
	use std::convert::TryInto;
	alt(
		(
			map_res(
				preceded(tok(Token::TokSub),tok_int),
				|v| {
					if v < i64::max_value() as u64 {
						let x = v as i64;
						Ok(ConstantResult::Signed(-x))
					}else{
						Err(())
					}
				}
			),
			map(
				tok_int,
				|v| ConstantResult::Unsigned(v)
			),
			map(
				tok_float,
				|v| ConstantResult::Float(v)
			)
		)
	)(i)
}

pub fn parse_local_output_decl<'l,E: ParseError<&'l[Token]>>(
	i: &'l[Token]
) -> ParseResult<'l,LocalOutputSetup,E> {
	alt(
		(
			map(tok(Token::TokUndefined),|_| LocalOutputSetup::Undefined),
			map(
				preceded(
					tok(Token::TokLoadFrom),
					delimited(
						tok(Token::TokLBracket),
						tok_ident,
						tok(Token::TokRBracket)
					)
				),
				|v| LocalOutputSetup::LoadFrom(v)
			),
			map_res(
				preceded(
					tok(Token::TokClear),
					delimited(
						tok(Token::TokLBracket),
						tuple(
							(
								parse_local_constant,
								many_m_n(
									0,
									3,
									preceded(
										tok(Token::TokComma),
										parse_local_constant
									)
								)
							)
						),
						tok(Token::TokRBracket)
					)
				),
				|v: (ConstantResult,Vec<ConstantResult>) | {
					match v.0 {
						ConstantResult::Signed(s) => {
							let ss = match ConstantResult::make_signed(&v.1) {
								Some(v) => v,
								None => return Err(())
							};
							if ss.len() == 0 {
								Ok(LocalOutputSetup::ClearSignedInteger1(s))
							}else if ss.len() == 1 {
								Ok(LocalOutputSetup::ClearSignedInteger2(s,ss[0]))
							}else if ss.len() == 2 {
								Ok(LocalOutputSetup::ClearSignedInteger3(s,ss[0],ss[1]))
							}else {
								Ok(LocalOutputSetup::ClearSignedInteger4(s,ss[0],ss[1],ss[2]))
							}
						},
						ConstantResult::Unsigned(u) => {
							let uu = match ConstantResult::make_unsigned(&v.1) {
								Some(v) => v,
								None => return Err(())
							};
							if uu.len() == 0 {
								Ok(LocalOutputSetup::ClearUnsignedInteger1(u))
							}else if uu.len() == 1 {
								Ok(LocalOutputSetup::ClearUnsignedInteger2(u,uu[0]))
							}else if uu.len() == 2 {
								Ok(LocalOutputSetup::ClearUnsignedInteger3(u,uu[0],uu[1]))
							}else {
								Ok(LocalOutputSetup::ClearUnsignedInteger4(u,uu[0],uu[1],uu[2]))
							}
						},
						ConstantResult::Float(f) => {
							let ff = match ConstantResult::make_float(&v.1) {
								Some(v) => v,
								None => return Err(())
							};
							if ff.len() == 0 {
								Ok(LocalOutputSetup::ClearFloat1(f))
							}else if ff.len() == 1 {
								Ok(LocalOutputSetup::ClearFloat2(f,ff[0]))
							}else if ff.len() == 2 {
								Ok(LocalOutputSetup::ClearFloat3(f,ff[0],ff[1]))
							}else {
								Ok(LocalOutputSetup::ClearFloat4(f,ff[0],ff[1],ff[2]))
							}
						}
					}
				}
			)
		)
	)(i)
}

pub fn parse_image_output_decl<'l,E: ParseError<&'l[Token]>>(
	i: &'l[Token]
) -> ParseResult<'l,LocalOutputImage,E> {
	map(
		tuple(
			(
				tok_ident,
				delimited(
					tok(Token::TokLBracket),
					tok_ident,
					tok(Token::TokRBracket)
				),
				preceded(
					tok(Token::TokAssign),
					tok_type
				),
				preceded(
					tok(Token::TokColon),
					parse_local_output_decl,
				)
			)
		),
		|v| {
			LocalOutputImage{
				name: v.0,
				ref_id: v.1,
				image_type: v.2,
				load_op: v.3
			}
		}
	)(i)
}

pub fn parse_image_input_decl<'l,E: ParseError<&'l[Token]>>(
	i: &'l[Token]
) -> ParseResult<'l,LocalInputImage,E> {
	map(
		tuple(
			(
				tok_ident,
				delimited(
					tok(Token::TokLBracket),
					tok_ident,
					tok(Token::TokRBracket)
				),
				preceded(
					tok(Token::TokAssign),
					tok_type
				),
				preceded(
					tok(Token::TokColon),
					alt(
						(
							tok_to(Token::TokLocal,true),
							tok_to(Token::TokGlobal,false)
						)
					)
				)
			)
		),
		|v| {
			LocalInputImage {
				name: v.0,
				ref_id: v.1,
				image_type: v.2,
				local: v.3
			}
		}
	)(i)
}

pub fn parse_image_outputs_decl<'l,E: ParseError<&'l[Token]>>(
	i: &'l[Token]
) -> ParseResult<'l,Declaration,E> {
	map(
		tuple(
			(
				many0(parse_pragma),
				tok(Token::TokOutput),
				tok(Token::TokAssign),
				delimited(
					tok(Token::TokLSection),
					many1(parse_image_output_decl),
					tok(Token::TokRSection)
				)
			)
		),
		|v| {
			Declaration::LocalOutputDecl {
				pragmas: v.0.into_iter().collect(),
				decl_list: v.3
			}
		}
	)(i)
}

pub fn parse_image_inputs_decl<'l,E: ParseError<&'l[Token]>>(
	i: &'l[Token]
) -> ParseResult<'l,Declaration,E> {
	map(
		tuple(
			(
				many0(parse_pragma),
				tok(Token::TokInput),
				tok(Token::TokAssign),
				delimited(
					tok(Token::TokLSection),
					many1(parse_image_input_decl),
					tok(Token::TokRSection)
				)
			)
		),
		|v| {
			Declaration::LocalInputDecl {
				pragmas: v.0.into_iter().collect(),
				decl_list: v.3
			}
		}
	)(i)
}

pub fn parse_declaration<'l,E: ParseError<&'l[Token]>>(
	i: &'l[Token]
) -> ParseResult<'l,Declaration,E> {
	alt(
		(
			parse_global_pragma,
			parse_global_input,
			parse_image_inputs_decl,
			parse_image_outputs_decl,
			parse_shader_decl
		)
	)(i)
}