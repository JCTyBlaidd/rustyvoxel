use crate::prelude::*;
use hashbrown::HashMap;

pub mod parser;
pub mod parser_ir;
pub mod shader_ir;

pub mod shader_desc;
pub mod shader_graph;

pub fn parse_shader_file(src: &str) -> Option<Vec<parser_ir::Declaration>> {
	use self::parser::lexer::{Lexer,Token};
	
	let lex = Lexer::run_lexer(src).map_err(|e| {
		warn!("Failed to lex shader file");
		let failure = &src[0.max(e - 5)..src.len().min(e + 5)];
		warn!(" Failed at: {}", failure);
	}).ok()?;
	let decls = parser::parse_file(
		&lex
	).map_err(|e: nom::Err<nom::error::VerboseError<&[Token]>>| {
		warn!("Failed to parse shader file");
		warn!("{:#?}", e);
	}).ok()?;
	if decls.0.len() != 0 {
		warn!("Failed to parse entire shader file");
		warn!(" Tokens Unprocessed: {}", decls.0.len());
	}
	Some(decls.1)
}

//mod parse_tree {move expression, statement, +?types?}
//mod shader_ir


/*
vertex_input WORLD_DATA = {

}
//BUILT-IN
input(vertex) = WORLD_DATA | SCREEN_RECT | DRAW_DATA (From Struct Def Somewhere)
input(uniform) = BLOCK_ATLAS | BLOCK_DRAW_DATA


//DEFINED - vec4("DISPLAY") is only defined
input(sample) = {
	0: vec4("AWESOME") = local_sample,
	1: vec4("ALL") = global_sample
}
output = {/* size - N/A */
	0: vec4("COLOUR") = clear(0,0,0) | undefined | copy_from("OLD_COLOUR"),
	1: f32("DEPTH"),
}
shader vertex : (u32,u32,u32) = {
	
	vertex.Position = ....
	vertex.0 = ...
}
shader fragment : outputs = {
	let a : vec4 = vertex.0;
	
	outputs.0 = ...
}

*/