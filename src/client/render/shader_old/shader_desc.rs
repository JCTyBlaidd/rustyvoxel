use super::shader_graph::ResourceDimension;

#[derive(Clone, PartialEq, Debug)]
pub struct ShaderDescription {

}

impl ShaderDescription {
	pub fn get_target_width(&self) -> ResourceDimension {
		unimplemented!()
	}
	pub fn get_target_height(&self) -> ResourceDimension {
		unimplemented!()
	}
	pub fn get_target_layers(&self) -> ResourceDimension {
		unimplemented!()
	}
	pub fn is_compute_shader(&self) -> bool {
		unimplemented!()
	}
}