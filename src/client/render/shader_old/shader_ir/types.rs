use std::num::NonZeroU8;


#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ShaderType {
	Sized(SizedType),
	Image(ImageType),
	DynamicArray(Box<ShaderType>),
	SizedArray(Box<ShaderType>,u32),
	StructureType(Box<[ShaderType]>),
	PointerType(PointerType),
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct PointerType {
	pub target: Box<ShaderType>,
	//FIXME: PointerType
}


#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum SizedType {
	
	/// Zero Sized Type
	VoidType,
	
	/// Integer Type:
	///  signed = is_signed
	///  size = bits(8,16,32,64)
	///  vec_size = elem in vector
	IntegerType {
		signed: bool,
		size: NonZeroU8,
		vec_size: NonZeroU8,
	},
	
	/// Floating Type:
	///  size = bits(16,32,64)
	///  vec_size = elem in vector
	///  mat_size = vector in mat
	FloatType {
		size: NonZeroU8,
		vec_size: NonZeroU8,
		mat_size: NonZeroU8,
	}
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ImageType {
	Dim1D,
	Dim2D,
	DimCube,
	DimRect,
	DimBuffer,
	DimSubPass,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ImageTypeMeta {
	Standard,
	MultiSampled,
	Array,
	MultiSampledArray,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ImageSamplerType {
	
	/// Raw Sampler: No Configuration
	SamplerType,
	
	/// Image Without Sampler
	ImageType {
		
		/// dimensionality of the image
		img_type: ImageType,
		
		/// is the image a depth image, y/n/unknown
		img_depth: Option<bool>,
		
		/// is the image MS or Array
		img_meta: ImageTypeMeta,
		
		/// does the image use a sampler y/n/unknown
		uses_sampler: Option<bool>,
	},
	
	/// Image With Associated Sampler
	SampledImageType {
		
		/// dimensionality of the image
		img_type: ImageType,
		
		/// is the image a depth image, y/n/unknown
		img_depth: Option<bool>,
		
		/// Is the image MS or Array?
		img_meta: ImageTypeMeta,
	},
	
}