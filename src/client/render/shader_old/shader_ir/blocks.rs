use super::operation::{
	ShaderOperation,
	ValueIdx
};

type BlockIdx = u32;


/// Initial Op: Must be First
/// Terminal Op: Must be Last
pub struct BlockData {
	pub imported_ops: Vec<ValueIdx>,
	pub internal_ops: Vec<ValueIdx>,
	pub exported_ops: Vec<ValueIdx>,
}

pub struct Function {
	
	pub blocks: Vec<BlockIdx>,
}