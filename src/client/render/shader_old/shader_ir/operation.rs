
pub type ValueIdx = u32;
pub type TypeSize = u8;
pub type VecSize = u8;

//TODO: BUILTIN_VARIABLES
//TODO: BUILTIN_FUNCTIONS {OR KEEP AS OPERATIONS}

#[derive(Clone, PartialEq)]
pub enum ShaderOperation {
	DeletedOperation,
	
	IntSignedConstant(TypeSize,i64),
	IntUnsignedConstant(TypeSize,u64),
	FloatConstant(TypeSize,f64),
	BoolConstant(bool),
	
	MakeIntSignedVec(IntSize,[ValueIdx;4]),
	MakeFloatVec(FloatSize,[ValueIdx;4]),
	MakeBoolVec(VecSize,[ValueIdx;4]),
	
	IntUnaryOp(UnaryOp,IntSize,ValueIdx),
	IntBinaryOp(BinaryOp,IntSize,ValueIdx,ValueIdx),
	IntBitUnaryOp(BitwiseUnaryOp,IntSize,ValueIdx),
	IntBitBinaryOp(BitwiseBinaryOp,IntSize,ValueIdx,ValueIdx),
	IntCompareOp(CompareOp,IntSize,ValueIdx,ValueIdx),
	
	FloatUnaryOp(UnaryOp,FloatSize,ValueIdx),
	FloatBinaryOp(BinaryOp,FloatSize,ValueIdx,ValueIdx),
	FloatCompareOp(CompareOp,FloatSize,ValueIdx,ValueIdx),
	
	BoolNegateOp(ValueIdx),
	BoolCompareOp(CompareOp,ValueIdx,ValueIdx),
	
	IntVecMulScalar(IntSize,ValueIdx,ValueIdx),
	FloatVecMulScalar(FloatSize,ValueIdx,ValueIdx),
	
	FloatVecDotProduct(FloatSize,ValueIdx,ValueIdx),
	FloatVecOuterProduct(FloatSize,ValueIdx,ValueIdx),
	FloatVecCrossProduct(FloatSize,ValueIdx,ValueIdx),
	FloatVecLength(FloatSize,ValueIdx),
	
	FloatMatrixOp(BinaryOp),
	FloatMatVecMul,
	FloatVecMatMul,
	FloatMatDet,
	
	// Branch, Switch, Loop, Phi,
	// Load From Opaque
	//  ...
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct IntSize {
	pub int_size: TypeSize,
	pub vec_size: VecSize,
	pub int_signed: bool,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct FloatSize {
	pub float_size: TypeSize,
	pub vec_size: VecSize,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum UnaryOp {
	Negate,
}


#[derive(Copy, Clone, PartialEq, Eq)]
pub enum BinaryOp {
	Add,
	Sub,
	Mul,
	Div,
	Rem,
	Max,
	Min,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum BitwiseUnaryOp {
	BitwiseNot,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum BitwiseBinaryOp {
	BitwiseAnd,
	BitwiseOr,
	BitwiseXor,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum CompareOp {
	Equal,
	NotEqual,
	LessThan,
	LessThanEqual,
	GreaterThan,
	GreaterThanEqual,
}