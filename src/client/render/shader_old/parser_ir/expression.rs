use super::types::{
	BasicType,
	AdvType
};

#[derive(Clone, PartialEq, Debug)]
pub struct TaggedExpression {
	pub known_type: Option<Box<AdvType>>,
	pub value: Expression
}

impl TaggedExpression {
	pub fn new(exp: Expression) -> Box<TaggedExpression> {
		Box::new(
			TaggedExpression {
				known_type: None,
				value: exp,
			}
		)
	}
}

/// Abstract Parsed Expression
///  not a complete format for generation
///  of the shader
///  but stores the parse tree
#[derive(Clone, PartialEq, Debug)]
pub enum Expression {
	IdentConstant(String),
	BooleanConstant(bool),
	SignedIntConstant(i64),
	UnsignedIntConstant(u64),
	FloatConstant(f32),
	DoubleConstant(f64),
	UnaryExpr {
		op: UnaryOp,
		expr: Box<TaggedExpression>,
	},
	BinaryExpr {
		op: BinaryOp,
		left: Box<TaggedExpression>,
		right: Box<TaggedExpression>,
	},
	CmpBinaryExpr {
		op: CmpBinaryOp,
		left: Box<TaggedExpression>,
		right: Box<TaggedExpression>,
	},
	ArrayAccessExpr {
		target: Box<TaggedExpression>,
		index: Box<TaggedExpression>,
	},
	FieldAccessExpr {
		target: Box<TaggedExpression>,
		field: String,
	},
	FuncCallExpr {
		target: Box<TaggedExpression>,
		args: Vec<Box<TaggedExpression>>
	},
	TypeConstructExpr {
		target_type: BasicType,
		args: Vec<Box<TaggedExpression>>,
	},
	TernaryExpr {
		cond: Box<TaggedExpression>,
		left: Box<TaggedExpression>,
		right: Box<TaggedExpression>,
	},
}


#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum UnaryOp {
	Not,
	BitwiseNot,
	Negate,
	PrefixInc,
	PrefixDec,
	PostfixInc,
	PostfixDec,
}


#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum BinaryOp {
	Mul,
	Div,
	Rem,
	Add,
	Sub,
	ShiftLeft,
	ShiftRight,
	BitwiseAnd,
	BitwiseOr,
	BitwiseXor,
}


#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum CmpBinaryOp {
	LessThan,
	LessThanEqual,
	GreaterThan,
	GreaterThanEqual,
	Equal,
	NotEqual,
	LogicalAnd,
	LogicalOr,
	LogicalXor,
}