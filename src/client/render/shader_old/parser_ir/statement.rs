use super::expression::TaggedExpression;
use super::types::{
	AdvType,
	BasicType,
};
use hashbrown::HashMap;

#[derive(Clone, PartialEq, Debug)]
pub enum Declaration {
	GlobalPragma {
		key: String,
		value: String,
	},
	GlobalInputDecl {
		pragmas: HashMap<String, String>,
		input_type: GlobalInputType,
		decl_list: Option<Vec<(String, Box<AdvType>)>>,
	},
	LocalInputDecl {
		pragmas: HashMap<String, String>,
		decl_list: Vec<LocalInputImage>,
	},
	LocalOutputDecl {
		pragmas: HashMap<String, String>,
		decl_list: Vec<LocalOutputImage>,
	},
	ShaderDecl {
		pragmas: HashMap<String, String>,
		shader_type: ShaderDeclType,
		statements: Vec<Box<Statement>>,
	}
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct LocalInputImage {
	pub name: String,
	pub ref_id: String,
	pub image_type: BasicType,
	pub local: bool,
}

#[derive(Clone, PartialEq, Debug)]
pub struct LocalOutputImage {
	pub name: String,
	pub ref_id: String,
	pub image_type: BasicType,
	pub load_op: LocalOutputSetup
}

#[derive(Clone, PartialEq, Debug)]
pub enum LocalOutputSetup {
	Undefined,
	LoadFrom(String),
	ClearFloat1(f64),
	ClearFloat2(f64,f64),
	ClearFloat3(f64,f64,f64),
	ClearFloat4(f64,f64,f64,f64),
	ClearSignedInteger1(i64),
	ClearSignedInteger2(i64,i64),
	ClearSignedInteger3(i64,i64,i64),
	ClearSignedInteger4(i64,i64,i64,i64),
	ClearUnsignedInteger1(u64),
	ClearUnsignedInteger2(u64,u64),
	ClearUnsignedInteger3(u64,u64,u64),
	ClearUnsignedInteger4(u64,u64,u64,u64),
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum GlobalInputType {
	VertexInput,
	UniformInput,
	PushConstantInput,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ShaderDeclType {
	VertexShader,
	GeometryShader,
	TessEvalShader,
	TessControlShader,
	FragmentShader,
	ComputeShader,
}

#[derive(Clone, PartialEq, Debug)]
pub enum Statement {
	AssignStatement {
		op: AssignOp,
		target: Box<TaggedExpression>,
		assign: Box<TaggedExpression>,
	},
	LetStatement {
		target: String,
		modifier_extern: bool,
		modifier_const: bool,
		target_type: Box<AdvType>,
		assign: Box<TaggedExpression>,
	},
	IfStatement {
		cond: Box<TaggedExpression>,
		left: Vec<Box<Statement>>,
		right: Vec<Box<Statement>>,
	},
	WhileStatement {
		cond: Box<TaggedExpression>,
		inner: Vec<Box<Statement>>,
	},
	ReturnStatement {
		value: Box<TaggedExpression>,
	}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum AssignOp {
	Assign,
	AssignAdd,
	AssignSub,
	AssignMul,
	AssignDiv,
	AssignRem,
	AssignLeftShift,
	AssignRightShift,
	AssignBitwiseAnd,
	AssignBitwiseOr,
	AssignBitwiseXor
}