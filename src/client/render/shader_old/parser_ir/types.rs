pub use self::ImageFormatType::*;
pub use self::ImageLayoutType::*;
pub use self::BasicType::*;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ImageFormatType {
	FormatFloat,
	FormatSignedInteger,
	FormatUnsignedInteger
}
impl ImageFormatType {
	pub fn support_shadow(&self) -> bool {
		match self {
			FormatFloat => true,
			_ => false
		}
	}
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ImageLayoutType {
	Layout1D,
	Layout2D,
	Layout3D,
	LayoutCube,
	Layout2DRect,
	Layout1DArray,
	Layout2DArray,
	LayoutBuffer,
	Layout2DMS,
	Layout2DMSArray,
	LayoutCubeArray,
}
static IMAGE_LAYOUT_TYPE_LIST : [(&'static str, ImageLayoutType); 11] = [
	("1D",Layout1D),
	("2D",Layout2D),
	("3D",Layout3D),
	("Cube",LayoutCube),
	("2DRect",Layout2DRect),
	("1DArray",Layout1DArray),
	("2DArray",Layout2DArray),
	("Buffer",LayoutBuffer),
	("2DMS",Layout2DMS),
	("2DMSArray",Layout2DMSArray),
	("CubeArray",LayoutCubeArray)
];
impl ImageLayoutType {
	pub fn support_shadow(&self) -> bool {
		match self {
			Layout1D
			| Layout2D
			| LayoutCube
			| Layout1DArray
			| Layout2DArray
			| Layout2DRect
			| LayoutCubeArray => true,
			_ => false,
		}
	}
	pub fn match_layout(value: &str) -> Option<ImageLayoutType> {
		IMAGE_LAYOUT_TYPE_LIST.iter().find(|(slice,_)| {
			*slice == value
		}).map(|res| {
			res.1
		})
	}
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum BasicType {
	/// N/A (INTERNAL)
	TypeVoid,
	/// bool, bvec2, bvec3, bvec4
	TypeBoolean {
		dim: u8
	},
	/// int, ivec2, ivec3, ivec4
	TypeSignedInteger {
		dim: u8
	},
	/// uint, uvec2, uvec3, uvec4
	TypeUnsignedInteger {
		dim: u8
	},
	/// float, vec2, vec3, vec4,
	/// mat2, mat3, mat4,
	/// mat2x2, mat2x3, mat2x4
	/// mat3x2, mat3x3, mat3x4
	/// mat4x2, mat4x3, mat4x4
	TypeFloat {
		dim: (u8,u8)
	},
	/// double, dvec2, dvec3, dvec4
	/// dmat2, dmat3, dmat4
	/// dmat2x2, dmat2x3, dmat2x4
	/// dmat3x2, dmat3x2, dmat3x4
	/// dmat4x2, dmat4x2, dmat4x4
	TypeDouble {
		dim: (u8,u8)
	},
	/// sampler, samplerShadow
	TypeRawSampler {
		shadow: bool
	},
	/// Sampler & RawTexture:
	///  ("","i","u")"sampler"("2D",..)("","Shadow")
	TypeCombinedSampler {
		shadow: bool,
		format: ImageFormatType,
		layout: ImageLayoutType,
	},
	/// RawTexture:
	///  ("","i","u")"texture"("2D",...)
	TypeRawTexture {
		format: ImageFormatType,
		layout: ImageLayoutType,
	},
	/// StorageImage:
	///  ("","i","u")"image"("2D",...)
	TypeStorageImage {
		format: ImageFormatType,
		layout: ImageLayoutType,
	},
	/// "buffer"
	TypeStorageBuffer
}


impl BasicType {
	pub fn from_ident(ident: &str) -> Option<BasicType> {
		let mut char_iter = ident.chars();
		let first_char = char_iter.next();
		let remaining = char_iter.as_str();
		match first_char {
			Some('b') => {
				if remaining == "ool" {
					Some(TypeBoolean { dim: 1 })
				}else if remaining == "uffer" {
					Some(TypeStorageBuffer)
				}else if let Some(dim) = Self::match_vector(remaining) {
					Some(TypeBoolean { dim })
				}else{
					None
				}
			},
			Some('i') => {
				if remaining == "nt" {
					Some(TypeSignedInteger { dim: 1})
				}else if let Some(res) = Self::match_image(ident, FormatFloat) {
					Some(res)
				}else if let Some(res) = Self::match_all_layout(remaining, FormatSignedInteger) {
					Some(res)
				}else if let Some(dim) = Self::match_vector(remaining) {
					Some(TypeSignedInteger { dim })
				}else{
					None
				}
			},
			Some('u') => {
				if let Some(res) = Self::match_all_layout(remaining,FormatUnsignedInteger) {
					Some(res)
				}else if let Some(dim) = Self::match_vector(remaining) {
					Some(TypeUnsignedInteger { dim })
				}else{
					None
				}
			},
			Some('d') => {
				if remaining == "ouble" {
					Some(TypeDouble {dim: (1,1) })
				}else if let Some(dim) = Self::match_vector(remaining) {
					Some(TypeDouble {dim: (dim,1)})
				}else if let Some(dim) = Self::match_matrix(remaining) {
					Some(TypeDouble {dim})
				}else{
					None
				}
			},
			Some('a') => {
				None
			},
			Some('v') => {
				if let Some(dim) = Self::match_vector(ident) {
					Some(TypeFloat { dim: (dim,1) })
				}else{
					None
				}
			},
			Some('m') => {
				if let Some(dim) = Self::match_matrix(ident) {
					Some(TypeFloat { dim })
				}else{
					None
				}
			},
			Some('t') => {
				if let Some(res) = Self::match_texture(ident,FormatFloat) {
					Some(res)
				}else{
					None
				}
			},
			Some('s') => {
				if let Some(res) = Self::match_sampler(ident,FormatFloat) {
					Some(res)
				}else{
					None
				}
			},
			Some('f') => {
				if remaining == "loat" {
					Some(TypeFloat { dim: (1,1) })
				}else{
					None
				}
			}
			_ => None
		}
	}
	
	fn match_all_layout(input: &str, format: ImageFormatType) -> Option<BasicType> {
		if let Some(sampler) = Self::match_sampler(input,format) {
			Some(sampler)
		}else if let Some(texture) = Self::match_texture(input,format) {
			Some(texture)
		}else if let Some(image) = Self::match_image(input, format) {
			Some(image)
		}else{
			None
		}
	}
	
	fn match_sampler(sampler: &str, format: ImageFormatType) -> Option<BasicType> {
		if sampler.starts_with("sampler") {
			let (layout,shadow) = if sampler.ends_with("Shadow") {
				(&sampler[7..sampler.len() - 6],true)
			}else {
				(&sampler[7..], false)
			};
			if let Some(layout) = ImageLayoutType::match_layout(layout) {
				return Some(TypeCombinedSampler {
					shadow,
					format,
					layout
				})
			} else if format == FormatFloat && layout.len() == 0 {
				return Some(TypeRawSampler {
					shadow
				});
			}
		}
		None
	}
	
	fn match_texture(texture: &str, format: ImageFormatType) -> Option<BasicType> {
		if texture.starts_with("texture") {
			let layout = &texture[7..];
			if let Some(layout) = ImageLayoutType::match_layout(layout) {
				return Some(TypeRawTexture {
					format,
					layout
				})
			}
		}
		None
	}
	
	fn match_image(image: &str, format: ImageFormatType) -> Option<BasicType> {
		if image.starts_with("image") {
			let layout = &image[5..];
			if let Some(layout) = ImageLayoutType::match_layout(layout) {
				return Some(TypeStorageImage {
					format,
					layout
				})
			}
		}
		None
	}
	
	fn match_vector(vec: &str) -> Option<u8> {
		let (pre,post) = vec.split_at(3);
		if pre == "vec" {
			let mut post_iter = post.chars();
			let c1 = post_iter.next();
			let c2 = post_iter.next();
			if c2.is_none() {
				if let Some(dim) = c1 {
					return Self::match_dim(dim);
				}
			}
		}
		None
	}
	
	fn match_matrix(mat: &str) -> Option<(u8,u8)> {
		let (pre,post) = mat.split_at(3);
		if pre == "mat" {
			let mut post_iter = post.chars();
			let c1 = post_iter.next();
			let c2 = post_iter.next();
			let c3 = post_iter.next();
			let c4 = post_iter.next();
			
			if c2.is_none() {
				if let Some(dim) = c1 {
					if let Some(value) = Self::match_dim(dim) {
						return Some((value, value));
					}
				}
			}else if c4.is_none() && c2 == Some('x') {
				if let Some(x_dim) = c1 {
					if let Some(y_dim) = c2 {
						let x_val = Self::match_dim(x_dim);
						let y_val = Self::match_dim(y_dim);
						if let (Some(x),Some(y)) = (x_val,y_val) {
							return Some((x,y));
						}
					}
				}
			}
		}
		None
	}
	
	fn match_dim(val: char) -> Option<u8> {
		if val >= '2' && val <= '4' {
			Some((val as u8) - ('0' as u8))
		}else{
			None
		}
	}
	
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum AdvType {
	Basic(BasicType),
	FixedSizeArray(Box<AdvType>,u64),
	UnknownSizeArray(Box<AdvType>),
	UnknownStructureType(String),
	StructureType {
		fields: Vec<(String,Box<AdvType>)>,
	},
	UniformBufferType {
		fields: Vec<(String,Box<AdvType>)>,
	},
	FunctionType {
		arguments: Vec<Box<AdvType>>,
		return_type: Box<AdvType>,
	}
}