
#[repr(C)]
pub struct BlockVertex {
	pub position: (f32, f32, f32),
	pub rotation: (i8, i8, i8, i8),
	pub colour: u32,
	pub light: u32,
	pub uv_coord: (u8, u8),
	pub array_idx: u16,
	pub tex_idx: u16,
	pub anim_count: u16,
}

#[cfg(test)]
mod tests {
	use super::BlockVertex;
	#[test]
	fn test_block_vertex_size() {
		use std::mem::size_of;
		use std::mem::align_of;
		assert_eq!(size_of::<BlockVertex>(),  32);
		assert_eq!(align_of::<BlockVertex>(), 4);
	}
}