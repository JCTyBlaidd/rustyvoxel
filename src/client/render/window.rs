use crate::prelude::*;
use super::RenderAPI;
use winit::{
	event_loop::{
		EventLoop,
		ControlFlow
	},
	window::{
		Window,
		WindowBuilder
	},
	event::{
		Event,
		WindowEvent,
		DeviceEvent,
		KeyboardInput,
		MouseScrollDelta,
		MouseButton,
		ElementState,
		StartCause
	},
	dpi::LogicalPosition,
};
use std::time::Instant;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum StateSync {
	/// No Synchronization - Present as fast as possible
	Disabled = 0,
	
	/// Triple buffering - Present as fast as possible
	TripleBuffered = 1,
	
	/// Standard VSync
	Enabled = 2,
	
	/// Relaxed VSync
	Relaxed = 3,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum StateScreen {
	
	/// Standard Window Mode
	Windowed,
	
	/// Standard Window + No Border
	///  and fullscreen
	BorderLess,
	
	/// FIXME: IMPLEMENT IN PLACES
	/// Complete control of the display
	///  windowless fullscreen
	FullScreen,
}


/// A State Change has Occured:
///  - Assume Resize ALWAYS
///  - Maybe Change Window Mode & Sync
///  - Maybe Reload All Resources
#[derive(Clone, Debug)]
pub struct StateChange {
	reload_resources: bool,
	update_graphics: (),
	update_sync: Option<StateSync>,
	update_screen: Option<StateScreen>,
}
impl Default for StateChange {
	fn default() -> Self {
		StateChange {
			reload_resources: false,
			update_graphics: (),
			update_sync: None,
			update_screen: None,
		}
	}
}

#[derive(Clone, Debug)]
pub struct GlobalRenderConfig {
	pub field_of_view: f32,
	pub draw_distance: u32,
	
	pub screen_shot_request: bool,
	pub state_change: Option<StateChange>,
}

impl Default for GlobalRenderConfig {
	fn default() -> Self {
		GlobalRenderConfig {
			field_of_view: 100.0,
			draw_distance: 16,
			
			screen_shot_request: false,
			state_change: None,
		}
	}
}
impl GlobalRenderConfig {
	pub fn get_state_change(&mut self) -> &mut StateChange {
		self.state_change.get_or_insert_with(Default::default)
	}
}

pub trait WindowBackend {
	fn get_window(&mut self) -> &mut Window;
	fn get_event_loop(&mut self) -> &mut EventLoop<()>;
}

pub struct WindowMetadata {
	pub window_focused: bool,
	pub window_state: StateScreen,
	pub render_config: GlobalRenderConfig,
}

impl Default for WindowMetadata {
	fn default() -> Self {
		WindowMetadata {
			window_focused: true,
			window_state: StateScreen::Windowed,
			render_config: GlobalRenderConfig::default()
		}
	}
}

//FIXME: DEPRECATE!?!?!
pub trait UserEventHandle {
	fn on_window_char(
		&mut self, config: &mut GlobalRenderConfig, character: char
	);
	fn on_window_key(
		&mut self, config: &mut GlobalRenderConfig, key: &KeyboardInput
	);
	fn on_window_cursor_move(
		&mut self, config: &mut GlobalRenderConfig, pos: &LogicalPosition
	);
	fn on_window_cursor_enter(
		&mut self, config: &mut GlobalRenderConfig
	);
	fn on_window_cursor_leave(
		&mut self, config: &mut GlobalRenderConfig
	);
	fn on_window_cursor_scroll(
		&mut self, config: &mut GlobalRenderConfig, delta: &MouseScrollDelta
	);
	fn on_window_cursor_button(
		&mut self, config: &mut GlobalRenderConfig, state: ElementState, button: MouseButton
	);
	fn on_window_raw_delta(
		&mut self, config: &mut GlobalRenderConfig, delta: (f64,f64)
	);
}

impl WindowMetadata {
	
	#[inline(never)]
	pub fn main_loop<F,H: UserEventHandle> (
		&mut self,
		event_loop: &mut EventLoop<()>,
		event_handle: &mut H,
		mut loop_handle: F,
		render_api: &mut dyn RenderAPI,
	) where F: FnMut(
		&mut WindowMetadata,
		&mut StartCause,
		&mut dyn RenderAPI,
		bool,
		bool
	) -> ControlFlow {
		use winit::platform::desktop::EventLoopExtDesktop as _;
		
		let mut start_cause = StartCause::Init;
		let mut force_redraw = true;
		event_loop.run_return(| //FIXME: move?
			event,
			_ /* Window Target */,
			control_flow
		| {
			match &event {
				Event::WindowEvent {
					window_id,
					event
				} => {
					let config = &mut self.render_config;
					match event {
						WindowEvent::Resized(_) | WindowEvent::HiDpiFactorChanged(_) => {
							//Window Size Change => State Change Must Occur
							let _change = config.get_state_change();
						},
						WindowEvent::CloseRequested | WindowEvent::Destroyed => {
							//Window Shutdown Requested => Start Shutdown
							*control_flow = ControlFlow::Exit;
						},
						WindowEvent::RedrawRequested => {
							//Window Redraw Requested => Force Redraw
							force_redraw = true;
						},
						WindowEvent::Focused(is_focused) => {
							//Window Focus Changed => Flag Current Focus
							self.window_focused = *is_focused;
						},
						WindowEvent::ReceivedCharacter(character) => {
							//Forward Event
							event_handle.on_window_char(config,*character);
						}
						WindowEvent::KeyboardInput { input, .. } => {
							//Forward Event
							event_handle.on_window_key(config,input);
						}
						WindowEvent::CursorMoved{ position, .. } => {
							//Forward Event
							event_handle.on_window_cursor_move(config,position);
						},
						WindowEvent::CursorEntered { .. } => {
							//Forward Event
							event_handle.on_window_cursor_enter(config);
						},
						WindowEvent::CursorLeft { .. } => {
							//Forward Event
							event_handle.on_window_cursor_leave(config);
						},
						WindowEvent::MouseWheel { delta, .. } => {
							//Forward Event
							event_handle.on_window_cursor_scroll(config, delta);
						},
						WindowEvent::MouseInput { state, button, .. } => {
							//Forward Event
							event_handle.on_window_cursor_button(config,*state,*button);
						},
						_ => {},
					}
				},
				Event::DeviceEvent {
					device_id,
					event
				} => {
					let config = &mut self.render_config;
					match event {
						DeviceEvent::MouseMotion { delta } if self.window_focused => {
							event_handle.on_window_raw_delta(config, *delta);
						},
						_ => {}
					}
				},
				Event::NewEvents(cause) => {
					start_cause = *cause;
				},
				Event::EventsCleared => {
					//Events have been processed: Call Loop Code Processor
					// Detect if a shutdown sequence has been requested: notify if applicable
					let is_shutdown = *control_flow == ControlFlow::Exit;
					let result = loop_handle(self, &mut start_cause, render_api,is_shutdown,force_redraw);
					if !is_shutdown {
						*control_flow = result
					}
					force_redraw = false;
				},
				
				//FIXME: These are currently ignored
				Event::UserEvent(())
				| Event::LoopDestroyed
				| Event::Suspended
				| Event::Resumed => {},
			}
		})
	}
	
}

pub struct WindowHandle<W: WindowBackend> {
	pub window_metadata: WindowMetadata,
	pub window_backend: W,
	_private: ()
}

impl<W: WindowBackend> WindowHandle<W> {
	pub fn create_from(backend: W) -> WindowHandle<W> {
		WindowHandle {
			window_metadata: WindowMetadata::default(),
			window_backend: backend,
			_private: ()
		}
	}
}