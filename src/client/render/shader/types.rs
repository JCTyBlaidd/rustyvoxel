
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum BasicType {
	FloatType {
		size: u8,
		dimension: (u8,u8)
	},
	IntType {
		size: u8,
		dimension: u8,
		signed: bool,
	},
	BoolType {
		dimension: u8
	},
	VoidType,
}

impl BasicType {
	pub fn validate(&self) -> Option<&'static str> {
		match self {
			BasicType::FloatType {
				size,
				dimension
			} => {
				if dimension.1 == 1 {
					//Is a Vector | Singleton
					if dimension.0 <= 4 && dimension.0 > 0 {
						if *size == 16 || *size == 32 || *size == 64 {
							None
						}else{
							Some("Invalid Float Type Size")
						}
					}else{
						Some("Invalid Float Vec Size")
					}
				}else{
					//Is a Matrix
					if dimension.0 <= 4 && dimension.0 > 1
						&& dimension.1 <= 4 && dimension.1 > 1 {
						//(2,2) to (4,4) are valid sizes
						if *size == 16 || *size == 32 || *size == 64 {
							None
						}else{
							Some("Invalid Float Type Size")
						}
					}else{
						Some("Invalid Float Matrix Size")
					}
				}
			},
			BasicType::IntType {
				size,
				dimension,
				..
			} => {
				if *dimension <= 4 && *dimension > 0 {
					if *size == 8 || *size == 16 || *size == 32 || *size == 64 {
						None
					}else{
						Some("Invalid Integer Type Size")
					}
				}else{
					Some("Invalid Integer Vec Size")
				}
			},
			BasicType::BoolType {
				dimension
			} => {
				if *dimension <= 4 && *dimension > 0 {
					None
				}else{
					Some("Invalid Boolean Vec Size")
				}
			},
			BasicType::VoidType => None
		}
	}
	pub fn get_dimension(&self) -> (u8,u8) {
		match self {
			BasicType::FloatType {
				dimension,
				..
			} => {
				*dimension
			},
			BasicType::IntType {
				dimension,
				..
			} => {
				(*dimension,1)
			},
			BasicType::BoolType {
				dimension
			} => {
				(*dimension,1)
			},
			BasicType::VoidType => (0,0)
		}
	}
	pub fn is_vector(&self) -> Option<u8> {
		let dim = self.get_dimension();
		if dim.1 == 1 && dim.0 != 1 {
			Some(dim.0)
		}else{
			None
		}
	}
	pub fn is_singleton(&self) -> bool {
		self.get_dimension() == (1,1)
	}
	pub fn is_matrix(&self) -> Option<(u8,u8)> {
		let dim = self.get_dimension();
		if dim.1 > 1 {
			Some(dim)
		}else{
			None
		}
	}
	pub fn is_integer_type(&self) -> bool {
		if let BasicType::IntType {..} = self {
			true
		}else{
			false
		}
	}
	pub fn is_signed_type(&self) -> bool {
		if let BasicType::IntType {signed,..} = self {
			*signed
		}else{
			false
		}
	}
	pub fn is_unsigned_type(&self) -> bool {
		if let BasicType::IntType {signed,..} = self {
			!(*signed)
		}else{
			false
		}
	}
	pub fn is_boolean_type(&self) -> bool {
		if let BasicType::BoolType {..} = self {
			true
		}else{
			false
		}
	}
	pub fn is_float_type(&self) -> bool {
		if let BasicType::FloatType {..} = self {
			true
		}else{
			false
		}
	}
	pub fn is_void_type(&self) -> bool {
		if let BasicType::VoidType = self {
			true
		}else{
			false
		}
	}
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum AdvType {
	SamplerType,
	ImageType {
		//FIXME: Implement ImageType && SampledImageType
	},
	SampledImageType {
		//FIXME: Implement
	}
}

impl AdvType {
	pub fn validate(&self) -> Option<&'static str> {
		None
	}
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Type {
	Basic(BasicType),
	Advanced(AdvType),
	DynamicArray{
		base_type: Box<Type>
	},
	FixedArray{
		base_type: Box<Type>,
		size: u64
	},
	Pointer{
		target_type: Box<Type>,
		read: bool,
		write: bool
	},
	TypeHole
}


impl Type {
	pub fn hole() -> Type {
		Type::TypeHole
	}
}