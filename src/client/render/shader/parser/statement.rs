use super::{
	TaggedExpr,
	Type,
	BasicType
};

/// Pragma Definition
///  either global pragma:
///   #![key(value)] or #![key]
///  or local pragma:
///   #[key(value)] or #[key]
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct PragmaDecl {
	pub key: String,
	pub value: Option<String>,
}

/// Simple input definition, defines
///  the definition and/or usage of
///  a mapping from Strings => Simple Types
/// E.G.:
///  { position = vec4, uv = vec2 }
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum SimpleInputDecl {
	SimpleInputImport {
		input_id: String,
	},
	SimpleInputDefine {
		pragma_list: Vec<PragmaDecl>,
		use_define: bool,
		input_id: String,//FIXME: Accept Array??
		type_defines: Vec<(String,BasicType)>
	}
}

/// Complex input definition, handles
///  the same cases as the simple input
///  definition, with the additional
///  case of a complex type referenced,
///  with certain cases
#[derive(Clone, PartialEq, Debug)]
pub enum ComplexInputDecl {
	ComplexInputImport {
		input_id: String,
	},
	ComplexInputDefine {
		pragma_list: Vec<PragmaDecl>,
		use_define: bool,
		input_id: String,//FIXME: Accept Array??
		type_defines: Vec<(String,BasicType)>
	},
	ComplexInputDefineUsage {
		pragma_list: Vec<PragmaDecl>,
		use_define: bool,
		input_id: String,
		complex_type: Type,
	}
}


/// List of `FrameInputMeta`,
///  with associated pragmas
#[derive(Clone, Debug)]
pub struct FrameInputDecl {
	pub pragma_list: Vec<PragmaDecl>,
	pub input_list: Vec<FrameInputMeta>,
}

/// List of `FrameOutputMeta`,
///  with associated pragmas
#[derive(Clone, Debug)]
pub struct FrameOutputDecl {
	pub pragma_list: Vec<PragmaDecl>,
	pub output_list: Vec<FrameOutputMeta>,
}

/// Frame Input metadata, describes
///  a frame result from a previous pass
///  that has been generated
/// Example:
///  local uv_data = vec2("UV_FRAME")
#[derive(Clone, Debug)]
pub struct FrameInputMeta {
	pub local_id: String,
	pub local_type: BasicType,
	pub frame_id: String,
	pub is_local: bool,
}

/// Frame Output Declaration:
///
/// out_pos = vec3("AWESOME") : load_from("A")
/// out_pos = vec3("FAILURE") : undefined
/// out_pos = vec3("NO_STORE") : clear(0,0,0) => discard
#[derive(Clone, Debug)]
pub struct FrameOutputMeta {
	pub local_name: String,
	pub local_type: BasicType,
	pub frame_id: String,
	pub load_op: FrameOutputLoadOp,
	pub store_value: bool,
}

/// Frame Output Initial Load Op
///  set the value used for the frame to some
///  known identifier
#[derive(Clone, Debug)]
pub enum FrameOutputLoadOp {
	LoadFrom(String),
	Undefined,
	ClearFloat(Vec<f64>),
	ClearInt(Vec<(bool,u64)>),
}

/// Function Declaration:
///  fn function(a: at, b: &c: at,... ) -> ret {
///     ... args of form: type, &type, &mut type
/// }
#[derive(Clone, Debug)]
pub struct FunctionDecl {
	pub pragma_list: Vec<PragmaDecl>,
	pub name: String,
	pub input_types: Vec<(String,Type)>,
	pub output_type: Option<Type>,
	pub stmt_list: Vec<Statement>,
}

/// A statement of the program
#[derive(Clone, PartialEq, Debug)]
pub enum Statement {
	AssignStatement {
		target: Box<TaggedExpr>,
		target_op: AssignOp,
		value: Box<TaggedExpr>,
		value_type: Type
	},
	LetStatement {
		id: String,
		id_type: Type,
		value: Box<TaggedExpr>,
		is_extern: bool,
		is_const: bool,
	},
	LetDynStatement {
		id: String,
		id_type: Type,
		assign_expr: Option<Box<TaggedExpr>>,
		size_expr: Box<TaggedExpr>,
	},
	IfStatement {
		cond: Box<TaggedExpr>,
		stmt_true: Vec<Statement>,
		stmt_false: Vec<Statement>,
	},
	WhileStatement {
		cond: Box<TaggedExpr>,
		stmt_loop: Vec<Statement>,
	},
	SwitchStatement {
		cond: Box<TaggedExpr>,
		cond_type: Type,
		case_stmts: Vec<(bool,u64,Vec<Statement>)>,
		default_stmts: Vec<Statement>,
	},
	CastStatement {
		value: Box<TaggedExpr>,
		src_type: Type,
		dst_type: Type
	},
	StatementBlock {
		pragmas: Vec<PragmaDecl>,
		stmt_list: Vec<Statement>,
	}
}

/// List of operations that could
///  be applied to the assign operation
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum AssignOp {
	Assign,
	AssignAdd,
	AssignSub,
	AssignMul,
	AssignDiv,
	AssignRem,
	AssignLeftShift,
	AssignRightShift,
	AssignBitwiseAnd,
	AssignBitwiseOr,
	AssignBitwiseXor
}