use super::*;
use self::lexer::{
	Lexer,
	LexerTok
};


//
// Tests for the Lexer
//

fn assert_lex(src: &str, target: LexerTok) {
	let mut l = Lexer::new(src);
	let tok = l.next().unwrap().unwrap().1;
	assert_eq!(tok,target);
	assert!(l.next().is_none());
}

fn assert_lex_error(src: &str) {
	let mut l = Lexer::new(src);
	let tok = l.next().unwrap();
	if let Ok((_,tok,_)) = tok {
		panic!("Unexpected Token: {:?} from {}", tok, src);
	}
}

#[test]
pub fn test_lexer_ident() {
	assert_lex("hello_ident",LexerTok::TokIdent("hello_ident"));
	assert_lex(" \t _ident123 ",LexerTok::TokIdent("_ident123"));
	assert_lex("!=",LexerTok::TokNotEqual);
	assert_lex(" \t  => \t \t\n\n",LexerTok::TokArrow);
}

#[test]
pub fn test_lexer_quote() {
	assert_lex("\"Hello World\"", LexerTok::TokQuote("Hello World"));
	assert_lex(" \"\"  ", LexerTok::TokQuote(""));
	assert_lex(" \"  \"  ", LexerTok::TokQuote("  "));
}

#[test]
pub fn test_lexer_keyword() {
	assert_lex("if",LexerTok::TokIf);
	assert_lex("vertex",LexerTok::TokVertex);
	assert_lex("switch",LexerTok::TokSwitch);
}

#[test]
pub fn test_lexer_int() {
	assert_lex("\t  12345  \t",LexerTok::TokIntConst(12345));
	assert_lex("  123.0  ",LexerTok::TokFloatConst(123.0));
	assert_lex("  123.0025", LexerTok::TokFloatConst(123.0025));
	assert_lex("  123.0025  \t ", LexerTok::TokFloatConst(123.0025));
	assert_lex("  456.0025e53", LexerTok::TokFloatConst(456.0025e53));
	assert_lex("  456.0025E53",LexerTok::TokFloatConst(456.0025e53));
	assert_lex("  789.0005E-3",LexerTok::TokFloatConst(789.0005E-3));
	assert_lex("  0.0          ",LexerTok::TokFloatConst(0.0));
	assert_lex("  0.0e0        ",LexerTok::TokFloatConst(0.0));
	assert_lex("  0.0e-0       ",LexerTok::TokFloatConst(0.0));
}

#[test]
pub fn test_lexer_int_bad() {
	assert_lex_error("00");
	assert_lex_error("000");
	assert_lex_error("5.23434e00");
	assert_lex_error("5.2325e-00");
	assert_lex_error("000.00e000");
}


//
// Tests for the Parser
//

pub fn test_same_parse_tree(a: &str, b: &str) {

}


