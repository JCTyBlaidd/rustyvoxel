use super::{
	Type,
	BasicType,
	AdvType
};

///Expression formed
/// from some constant
/// or identifier or
/// operation there-on
///Some details of constants
/// and the operations used
/// require knowledge of the
/// associated tagged type
#[derive(Clone, PartialEq, Debug)]
pub enum Expression {
	ConstantBool(bool),
	ConstantSigned(i64),
	ConstantUnsigned(u64),
	ConstantFloat(f64),
	ConstantIdent(String),
	UnaryExpr {
		op: MonoOp,
		expr: Box<TaggedExpr>,
	},
	BinaryExpr {
		op: BinaryOp,
		left: Box<TaggedExpr>,
		right: Box<TaggedExpr>,
	},
	ComparisonExpr {
		op: CompareOp,
		left: Box<TaggedExpr>,
		right: Box<TaggedExpr>,
	},
	FieldExpr {
		expr: Box<TaggedExpr>,
		field: String
	},
	ArrayExpr {
		expr: Box<TaggedExpr>,
		index: Box<TaggedExpr>,
	},
	CallExpr {
		expr: Box<TaggedExpr>,
		args: Vec<Box<TaggedExpr>>,
	},
	MakeBasicTypeExpr {
		type_call: BasicType,
		args: Vec<Box<TaggedExpr>>,
	},
	MakeAdvTypeExpr {
		type_call: AdvType,
		args: Vec<Box<TaggedExpr>>,
	},
	TupleExpr {
		args: Vec<Box<TaggedExpr>>,
	},
	TernaryExpr {
		cond: Box<TaggedExpr>,
		expr_true: Box<TaggedExpr>,
		expr_false: Box<TaggedExpr>,
	}
}

impl Expression {
	pub fn tag(self) -> Box<TaggedExpr> {
		Box::new(TaggedExpr {
			expr: self,
			expr_type: Type::hole(),
		})
	}
}

#[derive(Clone, PartialEq, Debug)]
pub struct TaggedExpr {
	pub expr: Expression,
	pub expr_type: Type,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum MonoOp {
	LogicalNot,
	BitwiseNot,
	Negate,
	PrefixInc,
	PrefixDec,
	PostfixInc,
	PostfixDec,
}


#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum BinaryOp {
	Mul,
	Div,
	Rem,
	Add,
	Sub,
	ShiftL,
	ShiftR,
	BitAnd,
	BitOr,
	BitXor,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum CompareOp {
	Less,
	LessEq,
	Greater,
	GreaterEq,
	Equal,
	NotEqual,
	LogicalAnd,
	LogicalOr,
	LogicalXor,
}