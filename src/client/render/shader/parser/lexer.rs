use hashbrown::HashMap;
use std::str::CharIndices;
use std::fmt;
use super::{
	BasicType,
	AdvType,
};

pub struct Lexer<'input> {
	keyword_set: HashMap<&'static str, LexerTok<'input>>,
	input_iter: CharIndices<'input>,
	input_peek: Option<(usize,char)>,
	slice_peek: &'input str,
}

impl<'input> Lexer<'input> {
	pub fn make_keyword_map() -> HashMap<&'static str,LexerTok<'input>> {
		let mut map = HashMap::new();
		use self::LexerTok::*;
		
		//Insert BasicType
		static BASIC_TYPE_LIST : &[(&'static str,BasicType)] = &[
			("f16",   BasicType::FloatType {size: 16, dimension: (1,1) }),
			("f32",   BasicType::FloatType {size: 32, dimension: (1,1) }),
			("f64",   BasicType::FloatType {size: 64, dimension: (1,1) }),
			("float", BasicType::FloatType {size: 32, dimension: (1,1) }),
			("double",BasicType::FloatType {size: 64, dimension: (1,1) }),
			
			("f16vec2",BasicType::FloatType {size: 16, dimension: (2,1) }),
			("f32vec2",BasicType::FloatType {size: 32, dimension: (2,1) }),
			("f64vec2",BasicType::FloatType {size: 64, dimension: (2,1) }),
			("vec2",   BasicType::FloatType {size: 32, dimension: (2,1) }),
			("dvec2",  BasicType::FloatType {size: 64, dimension: (2,1) }),
			
			("f16vec3",BasicType::FloatType {size: 16, dimension: (3,1) }),
			("f32vec3",BasicType::FloatType {size: 32, dimension: (3,1) }),
			("f64vec3",BasicType::FloatType {size: 64, dimension: (3,1) }),
			("vec3",   BasicType::FloatType {size: 32, dimension: (3,1) }),
			("dvec3",  BasicType::FloatType {size: 64, dimension: (3,1) }),
			
			("f16vec4",BasicType::FloatType {size: 16, dimension: (4,1) }),
			("f32vec4",BasicType::FloatType {size: 32, dimension: (4,1) }),
			("f64vec4",BasicType::FloatType {size: 64, dimension: (4,1) }),
			("vec4",   BasicType::FloatType {size: 32, dimension: (4,1) }),
			("dvec4",  BasicType::FloatType {size: 64, dimension: (4,1) }),
			
			("f16mat2",  BasicType::FloatType {size: 16, dimension: (2,2) }),
			("f32mat2",  BasicType::FloatType {size: 32, dimension: (2,2) }),
			("f64mat2",  BasicType::FloatType {size: 64, dimension: (2,2) }),
			("f16mat2x2",BasicType::FloatType {size: 16, dimension: (2,2) }),
			("f32mat2x2",BasicType::FloatType {size: 32, dimension: (2,2) }),
			("f64mat2x2",BasicType::FloatType {size: 64, dimension: (2,2) }),
			("mat2",     BasicType::FloatType {size: 32, dimension: (2,2) }),
			("dmat2",    BasicType::FloatType {size: 64, dimension: (2,2) }),
			("mat2x2",   BasicType::FloatType {size: 32, dimension: (2,2) }),
			("dmat2x2",  BasicType::FloatType {size: 64, dimension: (2,2) }),
			
			("f16mat3",  BasicType::FloatType {size: 16, dimension: (3,3) }),
			("f32mat3",  BasicType::FloatType {size: 32, dimension: (3,3) }),
			("f64mat3",  BasicType::FloatType {size: 64, dimension: (3,3) }),
			("f16mat3x3",BasicType::FloatType {size: 16, dimension: (3,3) }),
			("f32mat3x3",BasicType::FloatType {size: 32, dimension: (3,3) }),
			("f64mat3x3",BasicType::FloatType {size: 64, dimension: (3,3) }),
			("mat3",     BasicType::FloatType {size: 32, dimension: (3,3) }),
			("dmat3",    BasicType::FloatType {size: 64, dimension: (3,3) }),
			("mat3x3",   BasicType::FloatType {size: 32, dimension: (3,3) }),
			("dmat3x3",  BasicType::FloatType {size: 64, dimension: (3,3) }),
			
			("f16mat4",  BasicType::FloatType {size: 16, dimension: (4,4) }),
			("f32mat4",  BasicType::FloatType {size: 32, dimension: (4,4) }),
			("f64mat4",  BasicType::FloatType {size: 64, dimension: (4,4) }),
			("f16mat4x4",BasicType::FloatType {size: 16, dimension: (4,4) }),
			("f32mat4x4",BasicType::FloatType {size: 32, dimension: (4,4) }),
			("f64mat4x4",BasicType::FloatType {size: 64, dimension: (4,4) }),
			("mat4",     BasicType::FloatType {size: 32, dimension: (4,4) }),
			("dmat4",    BasicType::FloatType {size: 64, dimension: (4,4) }),
			("mat4x4",   BasicType::FloatType {size: 32, dimension: (4,4) }),
			("dmat4x4",  BasicType::FloatType {size: 64, dimension: (4,4) }),
			
			("f16mat2x3",BasicType::FloatType {size: 16, dimension: (2,3) }),
			("f32mat2x3",BasicType::FloatType {size: 32, dimension: (2,3) }),
			("f64mat2x3",BasicType::FloatType {size: 64, dimension: (2,3) }),
			("mat2x3",   BasicType::FloatType {size: 32, dimension: (2,3) }),
			("dmat2x3",  BasicType::FloatType {size: 64, dimension: (2,3) }),
			
			("f16mat2x4",BasicType::FloatType {size: 16, dimension: (2,4) }),
			("f32mat2x4",BasicType::FloatType {size: 32, dimension: (2,4) }),
			("f64mat2x4",BasicType::FloatType {size: 64, dimension: (2,4) }),
			("mat2x4",   BasicType::FloatType {size: 32, dimension: (2,4) }),
			("dmat2x4",  BasicType::FloatType {size: 64, dimension: (2,4) }),
			
			("f16mat3x2",BasicType::FloatType {size: 16, dimension: (3,2) }),
			("f32mat3x2",BasicType::FloatType {size: 32, dimension: (3,2) }),
			("f64mat3x2",BasicType::FloatType {size: 64, dimension: (3,2) }),
			("mat3x2",   BasicType::FloatType {size: 32, dimension: (3,2) }),
			("dmat3x2",  BasicType::FloatType {size: 64, dimension: (3,2) }),
			
			("f16mat3x4",BasicType::FloatType {size: 16, dimension: (3,4) }),
			("f32mat3x4",BasicType::FloatType {size: 32, dimension: (3,4) }),
			("f64mat3x4",BasicType::FloatType {size: 64, dimension: (3,4) }),
			("mat3x4",   BasicType::FloatType {size: 32, dimension: (3,4) }),
			("dmat3x4",  BasicType::FloatType {size: 64, dimension: (3,4) }),
			
			("f16mat4x2",BasicType::FloatType {size: 16, dimension: (4,2) }),
			("f32mat4x2",BasicType::FloatType {size: 32, dimension: (4,2) }),
			("f64mat4x2",BasicType::FloatType {size: 64, dimension: (4,2) }),
			("mat4x2",   BasicType::FloatType {size: 32, dimension: (4,2) }),
			("dmat4x2",  BasicType::FloatType {size: 64, dimension: (4,2) }),
			
			("f16mat4x3",BasicType::FloatType {size: 16, dimension: (4,3) }),
			("f32mat4x3",BasicType::FloatType {size: 32, dimension: (4,3) }),
			("f64mat4x3",BasicType::FloatType {size: 64, dimension: (4,3) }),
			("mat4x3",   BasicType::FloatType {size: 32, dimension: (4,3) }),
			("dmat4x3",  BasicType::FloatType {size: 64, dimension: (4,3) }),
			
			("u8",     BasicType::IntType{ signed: false, size: 8,  dimension: 1}),
			("i8",     BasicType::IntType{ signed: true,  size: 8,  dimension: 1}),
			("u16",    BasicType::IntType{ signed: false, size: 16, dimension: 1}),
			("i16",    BasicType::IntType{ signed: true,  size: 16, dimension: 1}),
			("u32",    BasicType::IntType{ signed: false, size: 32, dimension: 1}),
			("i32",    BasicType::IntType{ signed: true,  size: 32, dimension: 1}),
			("u64",    BasicType::IntType{ signed: false, size: 64, dimension: 1}),
			("i64",    BasicType::IntType{ signed: true,  size: 64, dimension: 1}),
			("uint",   BasicType::IntType{ signed: false, size: 32, dimension: 1}),
			("int",    BasicType::IntType{ signed: true,  size: 32, dimension: 1}),
			
			("u8vec2", BasicType::IntType{ signed: false, size: 8,  dimension: 2}),
			("i8vec2", BasicType::IntType{ signed: true,  size: 8,  dimension: 2}),
			("u16vec2",BasicType::IntType{ signed: false, size: 16, dimension: 2}),
			("i16vec2",BasicType::IntType{ signed: true,  size: 16, dimension: 2}),
			("u32vec2",BasicType::IntType{ signed: false, size: 32, dimension: 2}),
			("i32vec2",BasicType::IntType{ signed: true,  size: 32, dimension: 2}),
			("u64vec2",BasicType::IntType{ signed: false, size: 64, dimension: 2}),
			("i64vec2",BasicType::IntType{ signed: true,  size: 64, dimension: 2}),
			("uvec2",  BasicType::IntType{ signed: false, size: 32, dimension: 2}),
			("ivec2",  BasicType::IntType{ signed: true,  size: 32, dimension: 2}),
			
			("u8vec3", BasicType::IntType{ signed: false, size: 8,  dimension: 3}),
			("i8vec3", BasicType::IntType{ signed: true,  size: 8,  dimension: 3}),
			("u16vec3",BasicType::IntType{ signed: false, size: 16, dimension: 3}),
			("i16vec3",BasicType::IntType{ signed: true,  size: 16, dimension: 3}),
			("u32vec3",BasicType::IntType{ signed: false, size: 32, dimension: 3}),
			("i32vec3",BasicType::IntType{ signed: true,  size: 32, dimension: 3}),
			("u64vec3",BasicType::IntType{ signed: false, size: 64, dimension: 3}),
			("i64vec3",BasicType::IntType{ signed: true,  size: 64, dimension: 3}),
			("uvec3",  BasicType::IntType{ signed: false, size: 32, dimension: 3}),
			("ivec3",  BasicType::IntType{ signed: true,  size: 32, dimension: 3}),
			
			("u8vec4", BasicType::IntType{ signed: false, size: 8,  dimension: 4}),
			("i8vec4", BasicType::IntType{ signed: true,  size: 8,  dimension: 4}),
			("u16vec4",BasicType::IntType{ signed: false, size: 16, dimension: 4}),
			("i16vec4",BasicType::IntType{ signed: true,  size: 16, dimension: 4}),
			("u32vec4",BasicType::IntType{ signed: false, size: 32, dimension: 4}),
			("i32vec4",BasicType::IntType{ signed: true,  size: 32, dimension: 4}),
			("u64vec4",BasicType::IntType{ signed: false, size: 64, dimension: 4}),
			("i64vec4",BasicType::IntType{ signed: true,  size: 64, dimension: 4}),
			("uvec4",  BasicType::IntType{ signed: false, size: 32, dimension: 4}),
			("ivec4",  BasicType::IntType{ signed: true,  size: 32, dimension: 4}),
			
			("bool",   BasicType::BoolType { dimension: 1}),
			("bvec2",  BasicType::BoolType { dimension: 2}),
			("bvec3",  BasicType::BoolType { dimension: 3}),
			("bvec4",  BasicType::BoolType { dimension: 4}),
			
			("void",   BasicType::VoidType)
		];
		for (basic_ident,basic_type) in &BASIC_TYPE_LIST[..] {
			map.insert(*basic_ident,TokBasicType(*basic_type));
		}
		
		//Insert AdvType
		//FIXME: IMPLEMENT PROPERLY
		static ADV_TYPE_LIST : &[(&'static str, AdvType)] = &[];
		
		//Insert Keywords
		static KEYWORD_LIST : &[(&'static str, LexerTok<'static>)] = &[
			("if",TokIf),
			("else",TokElse),
			("while",TokWhile),
			("switch",TokSwitch),
			("break",TokBreak),
			("fn",TokFn),
			("return",TokReturn),
			("input",TokInput),
			("output",TokOutput),
			("uniform",TokUniform),
			("push_constant",TokPushConst),
			("load_from",TokLoadFrom),
			("clear",TokClear),
			("undefined",TokUndefined),
			("discard",TokDiscard),
			("local",TokLocal),
			("global",TokGlobal),
			("let",TokLet),
			("extern",TokExternal),
			("mut",TokMut),
			("const",TokConst),
			("dyn",TokDyn),
			("shader",TokShader),
			("vertex",TokVertex),
			("geometry",TokGeometry),
			("tess_control",TokTessControl),
			("tess_eval",TokTessEval),
			("fragment",TokFragment),
			("compute",TokCompute),
			
			("true", TokBoolConst(true)),
			("false", TokBoolConst(false)),
		];
		for (keyword_ident, keyword_tok) in &KEYWORD_LIST[..] {
			map.insert(*keyword_ident, *keyword_tok);
		}
		
		map
	}
	
	pub fn new(input: &'input str) -> Lexer<'input> {
		Lexer {
			keyword_set: Lexer::make_keyword_map(),
			input_iter: input.char_indices(),
			input_peek: None,
			slice_peek: ""
		}
	}
	
	fn peek(&mut self) -> Option<(usize,char)> {
		match self.input_peek {
			peek @ Some(_) => peek,
			None => {
				self.slice_peek = self.input_iter.as_str();
				self.input_peek = self.input_iter.next();
				self.input_peek
			}
		}
	}
	
	fn consume_peek(&mut self) {
		self.input_peek = None;
	}
	
	fn get_slice_from(&self, slice: &'input str) -> &'input str {
		let len = slice.len() - self.slice_peek.len();
		&slice[..len]
	}
	
	fn parse_identifier(
		&mut self,
		start_loc: usize
	) -> (usize,LexerTok<'input>, usize) {
		let start_slice = self.slice_peek;
		self.consume_peek();
		
		while let Some((_,val)) = self.peek() {
			match val {
				'_' | 'a'..='z' | 'A'..='Z' | '0'..='9' => {
					self.consume_peek();
				},
				_ => {
					break;
				}
			}
		}
		let content_slice = self.get_slice_from(start_slice);
		
		let content = self.keyword_set.get(&content_slice)
			.cloned().unwrap_or(LexerTok::TokIdent(content_slice));
		
		(start_loc,content,start_loc + content_slice.len())
	}
	
	fn parse_quote(
		&mut self,
		start_loc: usize
	) -> (usize, LexerTok<'input>, usize) {
		self.consume_peek();
		let _ = self.peek();
		let start_slice = self.slice_peek;
		
		while let Some((_,val)) = self.peek() {
			self.consume_peek();
			if val == '"' {
				break;
			}
		}
		let content_slice = self.get_slice_from(start_slice);
		
		(start_loc,LexerTok::TokQuote(content_slice),start_loc + content_slice.len())
	}
	
	fn parse_numeric(
		&mut self,
		start_loc: usize,
		start_char: char,
	) -> Result<(usize,LexerTok<'input>, usize), LexerError<'input>> {
		let start_slice = self.slice_peek;
		self.consume_peek();
		
		//Is Zero Flags (Prevent Multiple Zeros)
		let mut is_zero_num = start_char == '0';
		let mut is_zero_dec = false;
		let mut is_zero_exp = false;
		let mut write_is_zero = &mut is_zero_num;
		
		//Parser Mode / State
		//0="123", 1="123.", 2="123.123", 3="123.123e",
		//4="123.123e123", 5="123.123.e-", 6="123.123e-123"
		let mut parse_mode = 0;
		
		while let Some((_,symbol)) = self.peek() {
			match symbol {
				'0'..='9' => {
					self.consume_peek();
					
					//Move to even parse mode (0,2,4,6)
					let symbol_is_zero = symbol == '0';
					if parse_mode % 2 != 0 {
						parse_mode += 1;
						*write_is_zero = symbol_is_zero;;
					}else if *write_is_zero && symbol_is_zero && parse_mode != 2 {
						
						//Error Multiple Zeros: "00" etc.. (Does not validate decimal values)
						let error_slice = self.get_slice_from(start_slice);
						if parse_mode == 0 {
							return Err(LexerError::InvalidIntegerZeros(error_slice));
						}else{
							return Err(LexerError::InvalidFloatZeros(error_slice));
						}
					}
				},
				'.' if parse_mode == 0 => {
					self.consume_peek();
					
					parse_mode = 1;
					write_is_zero = &mut is_zero_dec;
				},
				'e' | 'E' if parse_mode == 2 => {
					self.consume_peek();
					
					parse_mode = 3;
					write_is_zero = &mut is_zero_exp;
				},
				'-' if parse_mode == 3 => {
					self.consume_peek();
					
					parse_mode = 5;
				},
				_ => break
			}
		}
		
		//Return if even(0,2,4,6) {and mode is therefore valid}
		let content_slice = self.get_slice_from(start_slice);
		if parse_mode % 2 == 0 {
			let end_loc = start_loc + content_slice.len();
			if parse_mode == 0 {
				
				//Integer Constant
				if let Ok(v) = content_slice.parse::<u64>() {
					return Ok((start_loc,LexerTok::TokIntConst(v),end_loc));
				}
			}else{
				
				//Float Constant
				if let Ok(v) = content_slice.parse::<f64>() {
					return Ok((start_loc,LexerTok::TokFloatConst(v),end_loc));
				}
			}
		}
		Err(LexerError::InvalidFloat(content_slice))
	}
	
	fn run_lex(
		&mut self,
		loc: usize,
		tok: LexerTok<'input>
	) -> (usize, LexerTok<'input>, usize) {
		self.consume_peek();
		(loc,tok,loc + 1)
	}
	
	fn op_eq(
		&mut self,
		loc: usize,
		tok: LexerTok<'input>,
		alt: LexerTok<'input>
	) -> (usize, LexerTok<'input>, usize){
		self.consume_peek();
		if let Some((_,'=')) = self.peek() {
			self.consume_peek();
			(loc,alt,loc+2)
		}else{
			(loc,tok,loc+1)
		}
	}
	
	fn op_alt(
		&mut self,
		loc: usize,
		tok: LexerTok<'input>,
		next: char,
		alt: LexerTok<'input>
	) -> (usize, LexerTok<'input>, usize) {
		self.consume_peek();
		if let Some((_,val)) = self.peek() {
			if val == next {
				self.consume_peek();
				return (loc,alt,loc+2);
			}
		}
		(loc,tok,loc+1)
	}
	
	fn op_lex(
		&mut self,
		loc: usize,
		next: char,
		tok: LexerTok<'input>,
		alt: LexerTok<'input>,
		eq: LexerTok<'input>
	) -> (usize, LexerTok<'input>, usize) {
		self.consume_peek();
		if let Some((_,peek)) = self.peek() {
			if peek == next {
				self.consume_peek();
				return (loc,alt,loc+2);
			}else if peek == '=' {
				self.consume_peek();
				return (loc,eq,loc+2);
			}
		}
		(loc,tok,loc+1)
	}
}

impl<'input> Iterator for Lexer<'input> {
	type Item = Result<(usize,LexerTok<'input>, usize), LexerError<'input>>;
	
	fn next(&mut self) -> Option<Self::Item> {
		use self::LexerTok::*;
		while let Some((loc,symbol)) = self.peek() {
			match symbol {
				' ' | '\t' | '\r' | '\n' => {
					//Skip Whitespace
					self.consume_peek()
				},
				'0'..='9' => {
					//Parse Numeric (Float or Int)
					return Some(self.parse_numeric(loc,symbol));
				},
				'_' | 'a'..='z' | 'A'..='Z' => {
					//Parse Identifier Or Keyword Or Type
					return Some(Ok(self.parse_identifier(loc)));
				},
				'"' => {
					//Parse Quote
					return Some(Ok(self.parse_quote(loc)))
				},
				'+' => return Some(Ok(self.op_lex(loc,'+',TokAdd,TokAddAdd,TokAddEq))),
				'-' => return Some(Ok(self.op_lex(loc,'-',TokSub,TokSubSub,TokSubEq))),
				'*' => return Some(Ok(self.op_eq(loc,TokMul,TokMulEq))),
				'/' => return Some(Ok(self.op_eq(loc,TokDiv,TokDivEq))),
				'%' => return Some(Ok(self.op_eq(loc,TokRem,TokRemEq))),
				'&' => return Some(Ok(self.op_lex(loc,'&',TokAnd,TokAndAnd,TokAndEq))),
				'|' => return Some(Ok(self.op_lex(loc,'|',TokOr,TokOrOr,TokOrEq))),
				'^' => return Some(Ok(self.op_lex(loc,'^',TokXor,TokXorXor,TokXorEq))),
				'~' => return Some(Ok(self.run_lex(loc,TokBitNot))),
				'<' => return Some(Ok(self.op_lex(loc,'<',TokLT,TokLShift,TokLTEq))),
				'>' => return Some(Ok(self.op_lex(loc,'>',TokGT,TokRShift,TokGTEq))),
				'=' => return Some(Ok(self.op_lex(loc,'>',TokAssign,TokArrow,TokEqual))),
				'!' => return Some(Ok(self.op_eq(loc,TokBang,TokNotEqual))),
				'#' => return Some(Ok(self.op_alt(loc,TokHash,'!',TokHashBang))),
				',' => return Some(Ok(self.run_lex(loc,TokComma))),
				';' => return Some(Ok(self.run_lex(loc,TokSemiColon))),
				':' => return Some(Ok(self.run_lex(loc,TokColon))),
				'?' => return Some(Ok(self.run_lex(loc,TokQuestion))),
				'.' => return Some(Ok(self.run_lex(loc,TokDot))),
				'(' => return Some(Ok(self.run_lex(loc,TokLBracket))),
				')' => return Some(Ok(self.run_lex(loc,TokRBracket))),
				'{' => return Some(Ok(self.run_lex(loc,TokLSection))),
				'}' => return Some(Ok(self.run_lex(loc,TokRSection))),
				'[' => return Some(Ok(self.run_lex(loc,TokLSquare))),
				']' => return Some(Ok(self.run_lex(loc,TokRSquare))),
				other => {
					//Error: Invalid Character
					return Some(Err(LexerError::InvalidCharacter(other)));
				}
			}
		}
		None
	}
}

#[derive(Copy, Clone, Debug)]
pub enum LexerError<'input> {
	InvalidInteger(&'input str),
	InvalidFloat(&'input str),
	InvalidIntegerZeros(&'input str),
	InvalidFloatZeros(&'input str),
	InvalidCharacter(char),
	ParseError(&'static str),
}

impl<'input> fmt::Display for LexerError<'input> {
	fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
		match self {
			LexerError::InvalidInteger(s) => {
				write!(f,"Failed to parse integer: {}", *s)
			},
			LexerError::InvalidFloat(s) => {
				write!(f,"Failed to parse float: {}", *s)
			},
			LexerError::InvalidIntegerZeros(s) => {
				write!(f,"Invalid integer zeros: {}", *s)
			},
			LexerError::InvalidFloatZeros(s) => {
				write!(f,"Invalid float zeros: {}", *s)
			},
			LexerError::InvalidCharacter(c) => {
				write!(f,"Invalid lexer character: {}", *c)
			},
			LexerError::ParseError(s) => {
				write!(f,"Parser Error: \"{}\"", *s)
			}
		}
	}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum LexerTok<'input> {
	TokIdent(&'input str),
	TokQuote(&'input str),
	
	TokBoolConst(bool),
	TokIntConst(u64),
	TokFloatConst(f64),
	
	TokBasicType(BasicType),
	TokAdvType(AdvType),
	
	///"if"
	TokIf,
	///"else"
	TokElse,
	/// "while"
	TokWhile,
	/// "switch"
	TokSwitch,
	/// "break"
	TokBreak,
	/// "fn"
	TokFn,
	/// "return"
	TokReturn,
	/// "input"
	TokInput,
	/// "output"
	TokOutput,
	/// "uniform"
	TokUniform,
	/// "push_constant"
	TokPushConst,
	/// "load_from"
	TokLoadFrom,
	/// "clear"
	TokClear,
	/// "undefined"
	TokUndefined,
	/// "discard"
	TokDiscard,
	/// "local"
	TokLocal,
	/// "global"
	TokGlobal,
	/// "let"
	TokLet,
	/// "extern"
	TokExternal,
	/// "const"
	TokConst,
	/// "mut"
	TokMut,
	/// "dyn"
	TokDyn,
	/// "shader"
	TokShader,
	/// "vertex"
	TokVertex,
	/// "geometry"
	TokGeometry,
	/// "tess_control"
	TokTessControl,
	/// "tess_eval"
	TokTessEval,
	/// "fragment
	TokFragment,
	/// "compute"
	TokCompute,
	
	
	
	/// '+'
	TokAdd,
	/// '++'
	TokAddAdd,
	/// '+='
	TokAddEq,
	/// '-'
	TokSub,
	/// '--'
	TokSubSub,
	/// '-='
	TokSubEq,
	/// '*'
	TokMul,
	/// '*='
	TokMulEq,
	/// '/'
	TokDiv,
	/// '/='
	TokDivEq,
	/// '%'
	TokRem,
	/// %=
	TokRemEq,
	/// '&'
	TokAnd,
	/// '&&'
	TokAndAnd,
	/// '&='
	TokAndEq,
	/// '|'
	TokOr,
	/// '||'
	TokOrOr,
	/// '|='
	TokOrEq,
	/// '^'
	TokXor,
	/// '^^'
	TokXorXor,
	/// '^='
	TokXorEq,
	/// '~'
	TokBitNot,
	/// '<'
	TokLT,
	/// '<<'
	TokLShift,
	/// '<='
	TokLTEq,
	/// '>'
	TokGT,
	/// '>>'
	TokRShift,
	/// '>='
	TokGTEq,
	/// '='
	TokAssign,
	/// '=>'
	TokArrow,
	/// '=='
	TokEqual,
	/// '!'
	TokBang,
	/// '!='
	TokNotEqual,
	/// '#'
	TokHash,
	/// '#!'
	TokHashBang,
	/// ','
	TokComma,
	/// ';'
	TokSemiColon,
	/// ':'
	TokColon,
	/// '?'
	TokQuestion,
	/// '.'
	TokDot,
	/// '('
	TokLBracket,
	/// ')'
	TokRBracket,
	/// '{'
	TokLSection,
	/// '}'
	TokRSection,
	/// '['
	TokLSquare,
	/// ']'
	TokRSquare,
}