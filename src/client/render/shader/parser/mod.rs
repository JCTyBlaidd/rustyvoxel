use super::types::{
	BasicType,
	AdvType,
	Type
};
use lalrpop_util::lalrpop_mod;

pub mod expression;
pub mod statement;

mod lexer;

#[cfg(test)]
mod tests;

lalrpop_mod!(pub grammar, "/client/render/shader2/parser/grammar.rs");

pub use self::expression::{
	TaggedExpr,
	Expression,
	MonoOp,
	BinaryOp,
	CompareOp
};
pub use self::statement::{
	PragmaDecl,
	SimpleInputDecl,
	ComplexInputDecl,
	FunctionDecl,
	FrameInputDecl,
	FrameInputMeta,
	FrameOutputDecl,
	FrameOutputMeta,
	FrameOutputLoadOp,
	Statement,
	AssignOp
};

fn parse_to_decl_list(input: &str) {
	let lexer = self::lexer::Lexer::new(input);
	//let parser = self::grammer::ExprParse::new();
	//let result = parser.parse(&lexer);
	//result
}

/// Parse the shader and convert to a high level representation
///  this is then later converted into a mid level representation
///  in single-static-assignment form for consumption by the graphics
///  API for usage
pub fn parse_shader(input: &str) -> Result<ParsedShader,String> {
	unimplemented!()
}

/// Raw Shader Parse Information
///  will be later analysed and transformed
///  into shader metadata where each shader
///  stage is properly separated
pub struct ParsedShader {
	pub pragmas: Vec<PragmaDecl>,
	pub vertex_inputs: Vec<SimpleInputDecl>,
	pub push_inputs: Vec<SimpleInputDecl>,
	pub uniform_inputs: Vec<ComplexInputDecl>,
	pub function_decls: Vec<FunctionDecl>,
	pub input_decls: Option<FrameInputDecl>,
	//pub output_decls: Vec<OutputDecl>
	pub shader_vertex: Option<Vec<Statement>>,
	pub shader_geometry: Option<Vec<Statement>>,
	pub shader_tess_control: Option<Vec<Statement>>,
	pub shader_tess_eval: Option<Vec<Statement>>,
	pub shader_fragment: Option<Vec<Statement>>,
	pub shader_compute: Option<Vec<Statement>>
}