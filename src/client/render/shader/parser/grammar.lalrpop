use super::*;
use self::lexer::{
    LexerError,
    LexerTok,
    LexerTok::*
};
use std;

grammar<'input>;

extern {
    type Location = usize;
    type Error = LexerError<'input>;

    enum LexerTok<'input> {
        IDENT           => TokIdent(<&'input str>),
        QUOTE           => TokQuote(<&'input str>),
        BOOL            => TokBoolConst(<bool>),
        INT             => TokIntConst(<u64>),
        FLOAT           => TokFloatConst(<f64>),
        BASIC_TYPE      => TokBasicType(<BasicType>),
        ADV_TYPE        => TokAdvType(<AdvType>),
        "if"            => TokIf,
        "else"          => TokElse,
        "while"         => TokWhile,
        "switch"        => TokSwitch,
        "break"         => TokBreak,
        "fn"            => TokFn,
        "return"        => TokReturn,
        "input"         => TokInput,
        "output"        => TokOutput,
        "uniform"       => TokUniform,
        "push_constant" => TokPushConst,
        "load_from"     => TokLoadFrom,
        "clear"         => TokClear,
        "undefined"     => TokUndefined,
        "discard"       => TokDiscard,
        "local"         => TokLocal,
        "global"        => TokGlobal,
        "let"           => TokLet,
        "extern"        => TokExternal,
        "const"         => TokConst,
        "mut"           => TokMut,
        "dyn"           => TokDyn,
        "shader"        => TokShader,
        "vertex"        => TokVertex,
        "geometry"      => TokGeometry,
        "tess_control"  => TokTessControl,
        "tess_eval"     => TokTessEval,
        "fragment"      => TokFragment,
        "compute"       => TokCompute,
        "+"             => TokAdd,
        "++"            => TokAddAdd,
        "+="            => TokAddEq,
        "-"             => TokSub,
        "--"            => TokSubSub,
        "-="            => TokSubEq,
        "*"             => TokMul,
        "*="            => TokMulEq,
        "/"             => TokDiv,
        "/="            => TokDivEq,
        "%"             => TokRem,
        "%="            => TokRemEq,
        "&"             => TokAnd,
        "&&"            => TokAndAnd,
        "&="            => TokAndEq,
        "|"             => TokOr,
        "||"            => TokOrOr,
        "|="            => TokOrEq,
        "^"             => TokXor,
        "^^"            => TokXorXor,
        "^="            => TokXorEq,
        "~"             => TokBitNot,
        "<"             => TokLT,
        "<<"            => TokLShift,
        "<="            => TokLTEq,
        ">"             => TokGT,
        ">>"            => TokRShift,
        ">="            => TokGTEq,
        "="             => TokAssign,
        "=>"            => TokArrow,
        "=="            => TokEqual,
        "!"             => TokBang,
        "!="            => TokNotEqual,
        "#"             => TokHash,
        "#!"            => TokHashBang,
        ","             => TokComma,
        ";"             => TokSemiColon,
        ":"             => TokColon,
        "?"             => TokQuestion,
        "."             => TokDot,
        "("             => TokLBracket,
        ")"             => TokRBracket,
        "{"             => TokLSection,
        "}"             => TokRSection,
        "["             => TokLSquare,
        "]"             => TokRSquare
    }

}



//Utility Functions

CommaSeq<Rule>: Vec<Rule> =
    <rules: (<Rule> ",")*> <last: Rule?> => {
        let mut rules = rules;
        if let Some(v) = last {
            rules.push(v);
        }
        rules
};

CommaSeqNotEmpty<Rule> : Vec<Rule> = <rules: (<Rule> ",")*> <last: Rule> => {
    let mut rules = rules;
    rules.push(last);
    rules
};


//Expression Tree

ConstExpr: Box<TaggedExpr> = {
    <i:IDENT> => Expression::ConstantIdent(i.to_string()).tag(),
    <b:BOOL>  => Expression::ConstantBool(b).tag(),
    <i:INT>   => Expression::ConstantUnsigned(i).tag(),
    <f:FLOAT>  => Expression::ConstantFloat(f).tag(),
    "(" <e:Expr> ")" => e,
}

AccessExpr: Box<TaggedExpr> = {
    <expr:ConstExpr> => expr,
    <expr:AccessExpr> "[" <index:Expr> "]" => Expression::ArrayExpr {
        expr, index
    }.tag(),
    <expr:AccessExpr> "." <field:IDENT> => Expression::FieldExpr {
        expr, field: field.to_string()
    }.tag(),
    <expr:AccessExpr> "(" <args:CommaSeq<Expr>> ")" => Expression::CallExpr {
        expr, args
    }.tag()
}

TypedExpr: Box<TaggedExpr> = {
    <expr:AccessExpr> => expr,
    <type_call:BASIC_TYPE> "(" <args:CommaSeq<Expr>> ")" => Expression::MakeBasicTypeExpr {
        type_call, args
    }.tag(),
    <type_call:ADV_TYPE> "(" <args:CommaSeq<Expr>> ")" => Expression::MakeAdvTypeExpr {
        type_call, args
    }.tag()
}

PostfixExpr: Box<TaggedExpr> = {
    <expr:TypedExpr> => expr,
    <expr:TypedExpr> "++" => Expression::UnaryExpr {
        expr, op: MonoOp::PostfixInc
    }.tag(),
    <expr:TypedExpr> "--" => Expression::UnaryExpr {
        expr, op: MonoOp::PostfixDec
    }.tag()
}

PrefixExpr: Box<TaggedExpr> = {
    <expr: PostfixExpr> => expr,
    "++" <expr:PostfixExpr> => Expression::UnaryExpr {
        expr, op: MonoOp::PrefixInc
    }.tag(),
    "--" <expr:PostfixExpr> => Expression::UnaryExpr {
        expr, op: MonoOp::PrefixDec
    }.tag(),
    "!"  <expr:PostfixExpr> => Expression::UnaryExpr {
        expr, op: MonoOp::LogicalNot,
    }.tag(),
    "~"  <expr:PostfixExpr> => Expression::UnaryExpr {
    	expr, op: MonoOp::BitwiseNot,
    }.tag(),
    "-"  <expr:PostfixExpr> => Expression::UnaryExpr {
    	expr, op: MonoOp::Negate,
    }.tag()
}

MulExpr: Box<TaggedExpr> = {
    <expr: PrefixExpr> => expr,
    <left:MulExpr> "*" <right:PrefixExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::Mul
    }.tag(),
    <left:MulExpr> "/" <right:PrefixExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::Div
    }.tag(),
    <left:MulExpr> "%" <right:PrefixExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::Rem
    }.tag()
}

AddExpr: Box<TaggedExpr> = {
    <expr: MulExpr> => expr,
    <left:AddExpr> "+" <right:MulExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::Add
    }.tag(),
    <left:AddExpr> "-" <right:MulExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::Div
    }.tag()
}

ShiftExpr: Box<TaggedExpr> = {
    <expr: AddExpr> => expr,
    <left:ShiftExpr> ">>" <right:AddExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::ShiftL
    }.tag(),
    <left:ShiftExpr> "<<" <right:AddExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::ShiftR
    }.tag()
}

CompareExpr: Box<TaggedExpr> = {
    <expr: ShiftExpr> => expr,
    <left: ShiftExpr> "<" <right:ShiftExpr> => Expression::ComparisonExpr{
        left, right, op: CompareOp::Less
    }.tag(),
    <left: ShiftExpr> "<=" <right:ShiftExpr> => Expression::ComparisonExpr{
        left, right, op: CompareOp::LessEq
    }.tag(),
    <left: ShiftExpr> ">" <right:ShiftExpr> => Expression::ComparisonExpr{
        left, right, op: CompareOp::Greater
    }.tag(),
    <left: ShiftExpr> ">=" <right:ShiftExpr> => Expression::ComparisonExpr{
        left, right, op: CompareOp::GreaterEq
    }.tag(),
    <left: ShiftExpr> "==" <right:ShiftExpr> => Expression::ComparisonExpr{
        left, right, op: CompareOp::Equal
    }.tag(),
    <left: ShiftExpr> "!=" <right:ShiftExpr> => Expression::ComparisonExpr{
        left, right, op: CompareOp::NotEqual
    }.tag()
}

AndExpr: Box<TaggedExpr> = {
    <expr: CompareExpr> => expr,
    <left: AndExpr> "&" <right: CompareExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::BitAnd
    }.tag()
}

XorExpr: Box<TaggedExpr> = {
    <expr: AndExpr> => expr,
    <left: XorExpr> "^" <right: AndExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::BitXor
    }.tag()
}

OrExpr: Box<TaggedExpr> = {
    <expr: XorExpr> => expr,
    <left: OrExpr> "|" <right: XorExpr> => Expression::BinaryExpr{
        left, right, op: BinaryOp::BitXor
    }.tag()
}

BoolAndExpr: Box<TaggedExpr> = {
    <expr: OrExpr> => expr,
    <left: BoolAndExpr> "&&" <right: OrExpr> => Expression::ComparisonExpr{
        left, right, op: CompareOp::LogicalAnd
    }.tag()
}

BoolXorExpr: Box<TaggedExpr> = {
    <expr: BoolAndExpr> => expr,
    <left: BoolXorExpr> "^^" <right: BoolAndExpr> => Expression::ComparisonExpr{
        left, right, op: CompareOp::LogicalXor
    }.tag()
}

BoolOrExpr: Box<TaggedExpr> = {
    <expr: BoolXorExpr> => expr,
    <left: BoolOrExpr> "||" <right: BoolXorExpr> => Expression::ComparisonExpr{
        left, right, op: CompareOp::LogicalOr
    }.tag()
}


//Expression Types

PlaceExpr: Box<TaggedExpr> = {
    <i: IDENT> => Expression::ConstantIdent(i.to_string()).tag(),
    <expr: PlaceExpr> "[" <index: Expr> "]" => Expression::ArrayExpr {
        expr, index
    }.tag(),
    <expr: PlaceExpr> "." <field: IDENT> => Expression::FieldExpr {
        expr, field: field.to_string()
    }.tag()
}

Expr: Box<TaggedExpr> = {
    <expr: BoolOrExpr> => expr,
    <cond: BoolOrExpr> "?" <expr_true: BoolOrExpr> ":" <expr_false: Expr> => Expression::TernaryExpr {
        cond, expr_true, expr_false
    }.tag()
}


//Type Type

TypeExpr: Type = {
    <b: BASIC_TYPE> => Type::Basic(b),
    <a: ADV_TYPE> => Type::Advanced(a),
    "[" <t: TypeExpr> ";" <size: INT> "]" => Type::FixedArray {
        base_type: Box::new(t), size
    },
    "&" "const" <t: TypeExpr> => Type::Pointer {
        target_type: Box::new(t), read: true, write: false
    },
    "&" "mut" <t: TypeExpr> => Type::Pointer {
        target_type: Box::new(t), read: true, write: true
    },
    "&" "output" <t: TypeExpr> => Type::Pointer {
        target_type: Box::new(t), read: false, write: true
    }
}

DynTypeExpr: Type = {
    "[" <t: TypeExpr> "]" => Type::DynamicArray{
        base_type: Box::new(t)
    }
}

//Statement Types


LetStmt: Statement = {
    "let" <c: "const"?> <id: IDENT> ":" <t: TypeExpr> "=" <e:Expr> ";" => Statement::LetStatement{
        id: id.to_string(), id_type: t, value:e, is_const: c.is_some(), is_extern: false
    },
    "extern" <c: "const"?> <id: IDENT> ":" <t: TypeExpr> "=" <e:Expr> ";" => Statement::LetStatement{
        id: id.to_string(), id_type: t, value:e, is_const: c.is_some(), is_extern: true
    },
    "let" <id: IDENT> ":" <t: DynTypeExpr> "=" "[" "undefined" ";" "dyn" <e:Expr> "]" ";" => Statement::LetDynStatement{
        id: id.to_string(), id_type: t, assign_expr: None, size_expr: e
    },
    "let" <id: IDENT> ":" <t: DynTypeExpr> "=" "[" <v:Expr> ";" "dyn" <e: Expr> "]" ";" => Statement::LetDynStatement{
        id: id.to_string(), id_type: t, assign_expr: Some(v), size_expr: e
    }
}

AssignStmt: Statement = {
    <target: PlaceExpr> "=" <value:Expr> ";" => Statement::AssignStatement{
        target, value, target_op: AssignOp::Assign, value_type: Type::hole()
    },
    <target: PlaceExpr> "+=" <value:Expr> ";" => Statement::AssignStatement{
        target, value, target_op: AssignOp::AssignAdd, value_type: Type::hole()
    },
    <target: PlaceExpr> "-=" <value:Expr> ";" => Statement::AssignStatement{
        target, value, target_op: AssignOp::AssignSub, value_type: Type::hole()
    },
    <target: PlaceExpr> "*=" <value:Expr> ";" => Statement::AssignStatement{
        target, value, target_op: AssignOp::AssignMul, value_type: Type::hole()
    },
    <target: PlaceExpr> "/=" <value:Expr> ";" => Statement::AssignStatement{
        target, value, target_op: AssignOp::AssignDiv, value_type: Type::hole()
    },
    <target: PlaceExpr> "%=" <value:Expr> ";" => Statement::AssignStatement{
        target, value, target_op: AssignOp::AssignRem, value_type: Type::hole()
    },
//    <target: PlaceExpr> "<<=" <value:Expr> ";" => Statement::AssignStatement{
//        target, value, target_op: AssignOp::AssignLeftShift, value_type: Type::hole()
//    },
//    <target: PlaceExpr> ">>=" <value:Expr> ";" => Statement::AssignStatement{
//        target, value, target_op: AssignOp::AssignRightShift, value_type: Type::hole()
//    },
    <target: PlaceExpr> "&=" <value:Expr> ";" => Statement::AssignStatement{
        target, value, target_op: AssignOp::AssignBitwiseAnd, value_type: Type::hole()
    },
    <target: PlaceExpr> "|=" <value:Expr> ";" => Statement::AssignStatement{
        target, value, target_op: AssignOp::AssignBitwiseOr, value_type: Type::hole()
    },
    <target: PlaceExpr> "^=" <value:Expr> ";" => Statement::AssignStatement{
        target, value, target_op: AssignOp::AssignBitwiseXor, value_type: Type::hole()
    }
}

IfStmt: Statement = "if" <cond: Expr> "{" <ops: Stmt*> "}" <r: ElseStmt?> => Statement::IfStatement{
    cond, stmt_true: ops, stmt_false: r.unwrap_or(Vec::new())
};

ElseStmt: Vec<Statement> = {
    "else" "{" <op: Stmt*> "}" => op,
    "else" "if" <cond: Expr> "{" <ops: Stmt*> "}" <r: ElseStmt?> => {
        let mut res = Vec::with_capacity(1);
        res.push(Statement::IfStatement{
            cond, stmt_true: ops, stmt_false: r.unwrap_or(Vec::new())
        });
        res
    }
}

CaseArm: (Option<(bool,u64)>,Vec<Statement>) = {
    <i: INT> "=>" "{" <ops: Stmt*> "}" => (Some((false,i)),ops),
    "-" <i: INT> "=>" "{" <ops: Stmt*> "}" => (Some((true,i)),ops),
    <c: IDENT> "=>" "{" <ops: Stmt*> "}" =>? {
        if c == "_" {
            Ok((None,ops))
        }else{
            use lalrpop_util::ParseError;
            Err(ParseError::User {
                error: LexerError::ParseError("named default ident - not yet supported")
            })
        }
    }
};

CaseStmt: Statement = "switch" <cond: Expr> "{" <arms: CommaSeq<CaseArm>> "}" =>? {
    let mut branches = Vec::new();
    let mut default_arm = None;
    for (arm,stmts) in arms {
        match arm {
            Some((neg,val)) => {
                branches.push((neg,val,stmts));
            },
            None => {
                if default_arm.is_none() {
                    default_arm = Some(stmts);
                }else{
                    use lalrpop_util::ParseError;
                    return Err(ParseError::User {
                        error: LexerError::ParseError("multiple default switches")
                    });
                }
            }
        }
    }
    Ok(Statement::SwitchStatement {
        cond,
        cond_type: Type::hole(),
        case_stmts: branches,
        default_stmts: default_arm.unwrap_or(Vec::new())
    })
};

pub Stmt: Statement = {
    <lett: LetStmt> => lett,
    <assign: AssignStmt> => assign,
    <iff: IfStmt> => iff,
    "while" <cond: Expr> "{" <ops: Stmt*> "}" => Statement::WhileStatement{
        cond, stmt_loop: ops
    },
    <cases: CaseStmt> => cases
}

//Declarations

LocalPragma: PragmaDecl = {
    "#" "[" <i: IDENT> "]" => {
        PragmaDecl {
        	key: i.to_string(),
        	value: None
        }
    },
    "#" "[" <i: IDENT> "(" <v: IDENT> ")" "]" => {
        PragmaDecl {
            key: i.to_string(),
            value: Some(v.to_string())
        }
    },
    "#" "[" <i: IDENT> "=" <q: QUOTE> "]" => {
        PragmaDecl {
            key: i.to_string(),
            value: Some(q.to_string())
        }
    }
}

FnArg: (String, Type) = <id: IDENT> ":" <t: TypeExpr> => (id.to_string(),t);
FnReg: Type = "=>" <t: TypeExpr> => t;

pub FnDecl: FunctionDecl =  <p_list: LocalPragma*>
                            "fn" <id: IDENT> "(" <args: CommaSeq<FnArg>> ")"
                            <ret: FnReg?> "{" <ops: Stmt*> "}" => FunctionDecl {
    pragma_list: p_list,
    name: id.to_string(),
    input_types: args,
    output_type: ret,
    stmt_list: ops
};


FInputMeta: FrameInputMeta = {
    "local" <i: IDENT> "=" <b: BASIC_TYPE> "(" <q: QUOTE> ")" => {
        FrameInputMeta {
        	local_id: i.to_string(),
        	local_type: b,
            frame_id: q.to_string(),
        	is_local: true,
        }
    },
    "global" <i: IDENT> "=" <b: BASIC_TYPE> "(" <q: QUOTE> ")" => {
        FrameInputMeta {
            local_id: i.to_string(),
            local_type: b,
            frame_id: q.to_string(),
            is_local: false,
        }
    }
}

pub FInputDecl: FrameInputDecl = <p_list: LocalPragma*> "input" "=" "{" <decls: FInputMeta*> "}" => {
    FrameInputDecl {
    	pragma_list: p_list,
    	input_list: decls,
    }
};

SignAndInt: (bool,u64) = <a: "-"?> <u: INT> => (a.is_some(),u);

FOutputLoadOp: FrameOutputLoadOp = {
    "undefined" => FrameOutputLoadOp::Undefined,
    "load_from" "(" <q: QUOTE> ")" => FrameOutputLoadOp::LoadFrom(q.to_string()),
    "clear" "(" <fs: CommaSeqNotEmpty<FLOAT>> ")" => FrameOutputLoadOp::ClearFloat(fs),
    "clear" "(" <is: CommaSeqNotEmpty<SignAndInt>> ")" => FrameOutputLoadOp::ClearInt(is)
}

FOutputMeta: FrameOutputMeta = {
    <i: IDENT> "=" <b: BASIC_TYPE> "(" <q: QUOTE> ")" ":" <load: FOutputLoadOp> => {
        FrameOutputMeta {
        	local_name: i.to_string(),
        	local_type: b,
        	frame_id: q.to_string(),
        	load_op: load,
        	store_value: true,
        }
    },
    <i: IDENT> "=" <b: BASIC_TYPE> "(" <q: QUOTE> ")" ":" <load: FOutputLoadOp> "=>" "discard" => {
        FrameOutputMeta {
            local_name: i.to_string(),
            local_type: b,
            frame_id: q.to_string(),
            load_op: load,
            store_value: true,
        }
    }
}

pub FOutputDecl: FrameOutputDecl = <p_list: LocalPragma*> "output" "=" "{" <decls: FOutputMeta*> "}" => {
    FrameOutputDecl {
        pragma_list: p_list,
        output_list: decls
    }
};