

pub trait GuiBackend<'inst>: Send {
	//const SUPPORT_REDRAW: bool;
	
	fn start_draw(&mut self);
	fn store_data(&mut self, x: f32, y: f32, u: f32, v: f32, col: u32);
	fn change_state(&mut self);
	fn finish_draw(&mut self);
	fn redraw_old(&mut self);
}
