use super::{
	GlobalRenderBackend,
	ProgramConfig,
	TexAtlas,
	Backend,
	GuiBackend,
	WorldBackend,
	WindowBackend,
};
use ash::{
	vk_version_major,
	vk_version_minor,
	vk_version_patch
};
use winit::{
	window::Window,
	event_loop::EventLoop,
};
use crate::prelude::*;

mod core;
mod memory;
mod shader;
mod frame;
mod world;
mod gui;
mod window;

use self::core::{
	VkCore,
	Instance,
	PhysicalDevice,
	Device,
	SwapChain,
};
use self::gui::VulkanGui;
use self::world::VulkanWorld;
use self::window::VulkanWindow;
use self::memory::{
	MemoryAllocator,
	MemoryPage,
};

pub struct VulkanBackend {
	pub vk_core: VkCore,
	pub vk_memory: MemoryAllocator,
}

impl VulkanBackend {
	pub fn is_supported() -> Option<ash::Entry> {
		info!("{}", fmt_bold("Loading Vulkan Loader:"));
		let entry = ash::Entry::new().map_err(|e| {
			warn!("Vulkan not used: Failed to load loader - {:?}", fmt_failure(
				format!("{:?}",e)
			));
		}).ok()?;
		match entry.try_enumerate_instance_version() {
			Ok(Some(value)) => {
				if vk_version_major!(value) == 1 && vk_version_minor!(value) >= 1 {
					info!("  Vulkan Entry Loader:     {}", fmt_success("Found"));
					info!("  Vulkan Instance Version: {}",
						fmt_cyan(format!(
							"{}.{}.{}",
							vk_version_major!(value),
							vk_version_minor!(value),
							vk_version_patch!(value)
						))
					);
					info!("");
					Some(entry)
				}else{
					warn!("Vulkan not used: {} - {}.{}.{}",
						fmt_failure("Version not usable"),
						vk_version_major!(value),
						vk_version_minor!(value),
						vk_version_patch!(value)
					);
					None
				}
			},
			Err(e) => {
				warn!("Vulkan not used: Failed to load");
				warn!("  {:?}",fmt_failure(
					format!("{:?}",e)
				));
				None
			}
			Ok(None) => {
				warn!("Vulkan not used: {}",fmt_failure("Didn't enumerate instance version"));
				None
			}
		}
	}
	
	pub fn new(
		entry: ash::Entry,
		event_loop: EventLoop<()>,
		window: Window,
		config: &ProgramConfig
	) -> Result<Box<GlobalRenderBackend<VulkanBackend>>,(Window,EventLoop<()>)> {
		info!("{}", fmt_bold("Loading Vulkan Renderer:"));
		let core = VkCore::new(
			entry,
			&window,
			config.graphics_debug > 0,
			config.setup_render_doc,
			config.graphics_debug
		);
		let vk_core = match core {
			Ok(val) => val,
			Err(err) => {
				warn!("Failed to load vulkan: {:?}", err);
				return Err((window,event_loop))
			}
		};
		let vk_memory = MemoryAllocator::new(&vk_core.device);
		info!("");
		Ok(Box::new(GlobalRenderBackend::new(
			VulkanWindow {
				window,
				event_loop
			},
			VulkanBackend {
				vk_core,
				vk_memory
			}
		)))
	}
}

impl Drop for VulkanBackend {
	fn drop(&mut self) {
		unsafe {
			
			//Destroy Vulkan Core
			info!("{}",fmt_bold("Shutting Down Vulkan Backend:"));
			info!("");
			self.vk_core.destroy();
		}
	}
}



impl Backend for VulkanBackend {
	type Gui = VulkanGui;
	type World = VulkanWorld;
	type Window = VulkanWindow;
	
	fn update_atlas(&mut self, atlas: &TexAtlas) {
		unimplemented!()
	}
	
	fn make_gui(&self) -> VulkanGui {
		unimplemented!()
	}
}
