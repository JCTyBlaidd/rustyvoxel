use super::{
	WindowBackend,
	Window,
	EventLoop
};

//FIXME: Consider support for fullscreen display capture?
//  => ensure that this is detected somehow and direct device
//     events are called?,  longer term goal

/// Simple Wrapper around:
///  Window & EventLoop
///  Objects
pub struct VulkanWindow {
	pub window: Window,
	pub event_loop: EventLoop<()>,
}

impl WindowBackend for VulkanWindow {
	fn get_window(&mut self) -> &mut Window {
		&mut self.window
	}
	
	fn get_event_loop(&mut self) -> &mut EventLoop<()> {
		&mut self.event_loop
	}
}