use super::*;
use std::collections::LinkedList;

/// Allocated Buffer Page
pub struct PagedBuffer {
	view: vk::BufferView,
	page_index: usize,
	buffer_offset: u64,
	buffer_size: u64,
}

/// Permanent Buffer Allocation
///  with device allocation
///  fallback
/// Allocates memory on device local
///  is possible with fallback to
///  allocating on host
pub struct PagedBufferAlloc {
	usage: vk::BufferUsageFlags,
	share_with_compute: bool,
	share_with_transfer: bool,
	device_pages: Vec<InternalPage>,
	host_pages: Vec<InternalPage>,
}

impl PagedBufferAlloc {
	
	/// Round Allocation to required size
	#[inline(always)]
	fn round_size(alloc_size: u64) -> u64 {
		const SECTION_SIZE : u64 = 8192;
		(alloc_size + (SECTION_SIZE - 1)) % SECTION_SIZE
	}
	
	pub fn new(
		usage: vk::BufferUsageFlags,
		share_with_compute: bool,
		share_with_transfer: bool
	) -> PagedBufferAlloc {
		PagedBufferAlloc {
			usage,
			share_with_compute,
			share_with_transfer,
			device_pages: Vec::new(),
			host_pages: Vec::new()
		}
	}
	
	pub unsafe fn alloc_buffer(
		&mut self,
		device: &Device,
		alloc: &MemoryAllocator,
		alloc_size: u64
	) -> Result<PagedBuffer, vk::Result> {
		let rounded_size = Self::round_size(alloc_size);
		let page_count = (rounded_size + (alloc.memory_page_size - 1)) / alloc.memory_page_size;
		if page_count > 1 {
			unimplemented!()
		}else{
			//Single Page: Simple Allocation
			unimplemented!()
		}
	}
	
	pub unsafe fn re_alloc_buffer(
		&mut self,
		device: &Device,
		alloc: &MemoryAllocator,
		alloc_old: PagedBuffer,
		alloc_size: u64,
	) -> Result<(PagedBuffer, bool), vk::Result> {
		let rounded_size = Self::round_size(alloc_size);
		if rounded_size > alloc_old.buffer_size {
			//Requires New Allocation, May Fail
			
			unimplemented!()
		}else{
			//Same size or smaller - do nothing
			//FIXME: MAYBE FREE MEMORY!
			Ok((alloc_old,false))
		}
	}
	
	pub unsafe fn free_buffer(
		&mut self,
		device: &Device,
		alloc: &MemoryAllocator,
		alloc_old: PagedBuffer,
	) -> () {
	
	}
}

struct InternalPage {
	
	///Associated Memory Page
	page: MemoryPage,
	
	///Linked List of (offset,length) values
	free_space: LinkedList<(u64,u64)>,
	
	///Total space available for allocation
	total_free_space: u64,
}

impl InternalPage {
	pub fn try_alloc(&mut self, size: u64) -> Option<u64> {
		
		//Early Exit
		if self.total_free_space < size {
			return None;
		}
		
		//Search Linked List
		for value in self.free_space.iter_mut() {
			if value.1 >= size {
				
				//Alloc
				let start_idx = value.0;
				value.0 = start_idx + size;
				self.total_free_space -= size;
				return Some(start_idx);
			}
		}
		
		//Failed to Allocate
		None
	}
	pub fn free(&mut self, offset: u64, size: u64) {
		self.total_free_space += size;
		
		//Find useful insertion location
		let mut iter = self.free_space.iter_mut();
		let mut insert_loc = iter.find(|space| {
			space.0 > offset
		});
		//let before_ptr = iter.next_back();
		//iter.cursor();
		
		//FIXME: IMPLEMENT PROPERLY WITH MERGING WITH BEFORE & AFTER
		//FIXME: Replace both to use cursors (manual search)
		//FIXME: THIS ONLY MERGES in 1 direction, not both!
		
		//Insert Free Space
		if let Some(space) = insert_loc {
			if space.0 == offset + size {
				
				//Append space to start of section
				space.0 = offset;
				space.1 += size;
			}else{
				let rem = *space;
				space.0 = offset;
				space.1 = size;
				//FIXME: THIS IS ALL WRONG
				//iter.insert_next(rem);
			}
		}else{
			//self.free_space.append((offset,size));
		}
	}
}