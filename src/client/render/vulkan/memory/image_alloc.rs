use super::*;



/// Image Allocator for 1 Format of Image
///  - Host Allocation Fallback
///  - Delay Free
pub struct ImageAllocator {
	alloc_list: Vec<ImageAlloc>,
}

impl ImageAllocator {
	//pub fn allocate()
	//pub unsafe fn free_with_delay(ImageAllocView, delay: u32)
	//pub unsafe fn free_without_delay
	//pub unsafe fn destroy_all
	//pub unsafe make_image_blit_op(ImageAllocView, &mut ImageCopy)
}

//TODO: Better ImageAlloc ?(..Memory Requirements Stay The Same!)
// SO STANDARD ALLOCATOR W/ DEFRAGMENTATION MAYBE?
struct ImageAlloc {
	image: vk::Image,
	alloc_count: u32,
	image_size: vk::Extent2D,
	free_sections: Vec<vk::Rect2D>,
}

impl ImageAlloc {
	unsafe fn mark_free(&mut self, rect: &vk::Rect2D) {
		let size = Self::round_size(&rect.extent);
		
	}
	unsafe fn try_allocate(&mut self, rect: &vk::Extent2D) -> Option<vk::Offset2D>{
		let size = Self::round_size(&rect);
		for section in &mut self.free_sections {
			if section.extent.width >= size.width
				&& section.extent.height >= size.height {
				//Allocate YAY!!
			}
		}
		None
	}
	fn round_size(rect: &vk::Extent2D) -> vk::Extent2D {
		unimplemented!()
	}
}

/// Represents an image allocated from
///  an [ImageAllocator]
pub struct ImageAllocView {
	
	/// Image view for the allocated
	///  image stored
	view: vk::ImageView,
	
	/// Index of backing image for
	///  allocated image view
	image: u32,
	
	/// Allocation Rect
	///  Offset + Extent in image
	image_loc: vk::Rect2D,
}

impl ImageAllocView {
	
	#[inline(always)]
	pub fn image_view(&self) -> vk::ImageView {
		self.view
	}
	
	#[inline(always)]
	pub fn image_loc(&self) -> &vk::Rect2D {
		&self.image_loc
	}
	
}