use crate::prelude::*;
use super::{
	PhysicalDevice,
	Device,
};
use ash::vk;
use ash::version::{
	DeviceV1_0,
	DeviceV1_1
};
use std::sync::atomic::{
	AtomicU64,
	Ordering
};
use std::num::NonZeroU64;


pub mod image_alloc;
pub mod perm_buffer_alloc;
//pub mod temp_buffer_alloc;

/// Vulkan Memory Allocator:
///  - Allocates Pages
pub struct MemoryAllocator {
	device_memory: AtomicU64,
	host_memory: AtomicU64,
	supports_dedicated_allocation: bool,
	memory_page_size: u64,
}

impl MemoryAllocator {
	//TODO:
	// allocate_page [memory + buffer]
	// deallocate_page [memory + buffer]
	// TRACK_MEMORY_USAGE
	// allocate_dedicated_image [image]
	// deallocate_dedicated_image [image]
	// allocate_transient_image [May be Transient => May Also Not Be]
	// deallocate_transient_image [/\]
	
	pub fn new(device: &Device) -> MemoryAllocator {
		let supports_dedicated_allocation =
			device.enabled_extensions.contains("VK_KHR_dedicated_allocation");
		//FIXME: Better Memory Page Size Management
		let memory_page_size = 128 * 1024 * 1024; //128MB
		
		MemoryAllocator {
			device_memory: AtomicU64::new(0),
			host_memory: AtomicU64::new(0),
			supports_dedicated_allocation,
			memory_page_size
		}
	}
	
	//FIXME: ALLOCATE LARGER PAGES FOR FALLBACK?
	pub fn allocate_page(
		&self,
		device: &Device,
		page_usage: vk::BufferUsageFlags,
		page_size_mul: NonZeroU64,
		mut share_with_compute: bool,
		mut share_with_transfer: bool,
		property_list: &[vk::MemoryPropertyFlags],
	) -> Result<(usize,MemoryPage),vk::Result> {
		
		//Create Buffer
		if device.queue_compute.is_none() {
			share_with_compute = false;
		}
		if device.queue_transfer.is_none() {
			share_with_transfer = false;
		}
		let queue_memory = [
			device.queue_all.family,
			device.queue_compute.as_ref().map_or(0,|q| q.family),
			device.queue_transfer.as_ref().map_or(0, |q| q.family),
			device.queue_all.family
		];
		let (sharing_mode, queues) = if share_with_compute || share_with_transfer {
			(vk::SharingMode::CONCURRENT,
				if share_with_transfer && share_with_compute {
					&queue_memory[..3] //all|compute|transfer
				}else if share_with_transfer {
					&queue_memory[2..] //transfer|all
				}else{
					&queue_memory[..2] //all|compute
				}
			)
		}else{
			(vk::SharingMode::EXCLUSIVE, &queue_memory[..0])
		};
		let buffer_info = vk::BufferCreateInfo::builder()
			.size(self.memory_page_size * page_size_mul.get())
			.usage(page_usage) //flags? - unneeded
			.sharing_mode(sharing_mode)
			.queue_family_indices(queues);
		let buffer = unsafe {
			device.device.create_buffer(&buffer_info,None)
		}?;
		
		//Find Memory Index
		let buffer_memory_info = unsafe {
			//FIXME: memory_requirements2!!! (USE!!)
			device.device.get_buffer_memory_requirements(buffer)
		};
		let (selected_prop, memory_idx) = Self::find_memory_type(
			&device.raw_device,
			buffer_memory_info.memory_type_bits,
			property_list
		).ok_or_else(|| {
			unsafe {
				device.device.destroy_buffer(buffer,None);
			}
			//FIXME: Better Error Message
			vk::Result::ERROR_FORMAT_NOT_SUPPORTED
		})?;

		//Allocate Memory
		let memory_info = vk::MemoryAllocateInfo::builder()
			.allocation_size(buffer_memory_info.size)
			.memory_type_index(memory_idx);
		
		let memory = unsafe {
			device.device.allocate_memory(&memory_info, None)
		}.map_err(|e| {
			unsafe {
				device.device.destroy_buffer(buffer, None);
			}
			e
		})?;
		
		
		//Bind Memory
		unsafe {
			//FIXME: bind_buffer_memory2??
			device.device.bind_buffer_memory(
				buffer,
				memory,
				0
			)
		}.map_err(|e| {
			unsafe {
				device.device.destroy_buffer(buffer, None);
				device.device.free_memory(memory,None);
			}
			e
		})?;
		
		//Update memory tracking
		let used_property = &property_list[selected_prop];
		let device_local = used_property.contains(vk::MemoryPropertyFlags::DEVICE_LOCAL);
		if device_local {
			self.device_memory.fetch_add(
				buffer_memory_info.size,
					Ordering::Relaxed
			);
		}else{
			self.host_memory.fetch_add(
				buffer_memory_info.size,
				Ordering::Relaxed
			);
		}
		
		Ok((
			selected_prop,
			MemoryPage {
				buffer,
				memory,
				device_local,
				memory_size: buffer_memory_info.size,
				_private: ()
			}
		))
	}
	
	pub unsafe fn free_page(
		&self,
		device: &Device,
		page: MemoryPage
	) {
		if page.device_local {
			self.device_memory.fetch_sub(
					page.memory_size,
				Ordering::Relaxed
			);
		}else{
			self.host_memory.fetch_sub(
				page.memory_size,
				Ordering::Relaxed
			);
		}
		unsafe {
			device.device.destroy_buffer(
				page.buffer,
				None
			);
			device.device.free_memory(
				page.memory,
				None
			)
		}
	}
	
	/// Destroy Memory Allocator
	///  - Does not actually free memory
	///  - Instead notifies of any memory leaks that may have occured
	pub fn destroy(&mut self) {
		std::sync::atomic::fence(Ordering::SeqCst);
		let device_mem = self.device_memory.load(Ordering::Relaxed);
		let host_mem = self.host_memory.load(Ordering::Relaxed);
		if device_mem != 0 {
			warn!("Vulkan Memory: Device Allocation = {} Bytes", device_mem);
		}
		if host_mem != 0 {
			warn!("Vulkan Memory: Host Allocation = {} Bytes", host_mem);
		}
	}
	
	/// Return the maximum mip levels that can be
	///  allocated to an image of [width]x[height]
	///  size.
	/// Actual mip levels 1 <= mip <= result
	pub fn calc_2d_mip_levels(width: u32, height: u32) -> u8 {
		let max_pow: u32 = width.max(height).leading_zeros();
		let mip_levels: u8 = (32 - max_pow).max(1) as u8;
		mip_levels.min(15)
	}
	
	/// Find a valid memory type for allocation with fallback:
	///  [raw_device]: Raw Physical Device
	///  [type_filter]: memory_type_bits from metadata
	///  [property_list]: list of properties to attempt in order
	pub fn find_memory_type(
		raw_device: &PhysicalDevice,
		type_filter: u32,
		property_list: &[vk::MemoryPropertyFlags]
	) -> Option<(usize,u32)> {
		let memory_list = &raw_device.memory
			.memory_types[..raw_device.memory.memory_type_count as usize];
		for (prop_idx,property) in property_list.iter().enumerate() {
			if let Some(mem_idx) = Self::select_memory_type(
				memory_list,
				type_filter,
				*property
			) {
				return Some((prop_idx,mem_idx))
			}
		}
		None
	}
	
	/// Find a valid memory type for the allocation:
	///  type_filter: memory_type_bits from metadata
	///  properties: memory properties
	pub fn select_memory_type(
		memory_list: &[vk::MemoryType],
		type_filter: u32,
		properties: vk::MemoryPropertyFlags,
	) -> Option<u32> {
		for (idx, mem_type) in memory_list.iter().enumerate() {
			let allowed_type = (type_filter & (1 << idx as u32)) != 0;
			let contains_props = mem_type.property_flags.contains(properties);
			if allowed_type && contains_props {
				return Some(idx as u32);
			}
		}
		None
	}
}

pub struct MemoryPage {
	pub memory: vk::DeviceMemory,
	pub buffer: vk::Buffer,
	pub device_local: bool,
	pub memory_size: u64,
	_private: ()
}

impl MemoryPage {
}

//
// + VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT
// + VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT
//

//TODO: WORLD MEMORY ALLOCATOR (PAGES & TRACKING FOR MAYBE RAYTRACING)
//TODO: GUI MEMORY ALLOCATOR (STREAMED MEMORY + TEMP CACHING) + IMAGE STORAGE
//TODO: ENTITY MEMORY ALLOCATOR (STREAMED MEMORY + TEMP CACHING?)

//FIXME: ENSURE ALL MEMORY ALLOCATORS HAVE CPU FALLBACKS
//FIXME: ENSURE THAT DEDICATED DRAW ALLOCATIONS ARE ALWAYS ON THE GPU!!

//TODO: FULL MEMORY TEARDOWN TO ADD ALLOWANCE ON RESIZE