use hashbrown::HashSet;
use std::os::raw::c_char;
use std::ffi::CStr;

use crate::prelude::*;


/// Conservative comparison between a c_char array and a string
pub fn compare_with_str(array: &[c_char], string: &str) -> bool {
	if array.len() > 0 && array.iter().any(|v| *v == 0) {
		let other = unsafe {
			CStr::from_ptr(array.as_ptr())
		};
		other.to_str().map(|v| v == string).unwrap_or(false)
	}else{
		false
	}
}

/// Select from the given array of unknown type
///  - returns pointers to the passed array
///  - may fail if a required value is missing
pub fn select_entries<'a, T>(
	entries: &'a [T],
	required: &[&'static str],
	optional: &[&'static str],
	get_name: fn(&T) -> &[c_char],
	log_values: bool,
) -> Option<(Vec<*const c_char>, HashSet<&'static str>)> {
	let mut chosen = Vec::with_capacity(required.len() + optional.len());
	let mut enabled = HashSet::new();
	
	//Handle Required
	for req in required {
		let found = entries.iter().find(|v| {
			compare_with_str(get_name(*v),*req)
		});
		match found {
			Some(entry) => {
				if log_values {
					info!("    {}: {}", fmt_bold("Required"), fmt_success(*req));
				}
				chosen.push(get_name(entry).as_ptr());
				enabled.insert(*req);
			}
			None => {
				warn!("    Required Entry Missing: {}", fmt_failure(*req));
				return None;
			}
		}
	}
	
	
	for opt in optional {
		let found = entries.iter().find(|v| {
			compare_with_str(get_name(*v),*opt)
		});
		match found {
			Some(entry) => {
				if log_values {
					info!("    {}: {}", fmt_bold("Optional"), fmt_success(*opt));
				}
				chosen.push(get_name(entry).as_ptr());
				enabled.insert(*opt);
			},
			None => {
				if log_values {
					info!("    {}:  {}", fmt_bold("Missing"), fmt_failure(*opt));
				}
			}
		}
	}
	
	//Return Success
	Some((chosen, enabled))
}