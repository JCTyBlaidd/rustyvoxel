use crate::prelude::*;
use super::{
	Device,
	Surface,
	VulkanError,
	Window,
};
use ash::vk;
use ash::version::DeviceV1_0;
use smallvec::SmallVec;

pub struct SwapChain {
	pub swap_handle: vk::SwapchainKHR,
	//FIXME: SmallVec
	pub swap_images: Vec<(vk::Image,vk::ImageView)>,
	pub swap_current: u8,
	pub swap_config: SwapChainConfig,
	_private: (),
}

impl SwapChain {
	
	pub unsafe fn create(
		device: &Device,
		window: &Window,
		surface: &Surface
	) -> Result<SwapChain,VulkanError> {
		let swap_config = SwapChainConfig::create_default(
			device,
			window,
			surface
		);
		swap_config.print_metadata();
		let swap_handle = swap_config.make_swap_chain(
			device,
			surface,
			None
		).map_err(|e| {
			warn!("  {}: {}",fmt_bold("Creating Vulkan SwapChain"), fmt_failure("Failure"));
			e
		})?;
		info!("  {}: {}",fmt_bold("Creating Vulkan SwapChain"), fmt_success("Success"));
		let mut swap = SwapChain {
			swap_handle,
			swap_images: Vec::new(),
			swap_current: 0,
			swap_config,
			_private: ()
		};
		match swap.load_swap_images(device) {
			Ok(()) => Ok(swap),
			Err(e) => {
				
				//Clean-up
				swap.destroy(device);
				Err(e)
			}
		}
	}
	
	//FIXME: Should it Return CHANGE_IMAGE_COUNT? etc...
	pub unsafe fn recreate(
		&mut self,
		device: &Device,
		window: &Window,
		surface: &Surface
	) -> Result<(),VulkanError> {
		self.destroy_image_views(device);
		
		self.swap_config.update_swap_extent(device, window,surface);
		
		self.swap_handle = self.swap_config.make_swap_chain(
			device,
			surface,
			Some(self.swap_handle)
		)?;
		self.swap_current = 0;
		
		self.load_swap_images(device)?;
		Ok(())
	}
	
	pub unsafe fn destroy(&mut self, device: &Device) {
		self.destroy_image_views(device);
		device.ext_swap_chain.destroy_swapchain(
			self.swap_handle,
			None
		);
	}
	
	/// Prefix: swap_images must be clear
	unsafe fn load_swap_images(&mut self, device: &Device) -> Result<(),VulkanError>{
		debug_assert!(self.swap_images.is_empty());
		let images = device.ext_swap_chain.get_swapchain_images(self.swap_handle)?;
		
		let mut image_view_info = self.swap_config.get_image_view();
		for image in images.iter() {
			image_view_info.image = *image;
			let image_view = device.device.create_image_view(
				&image_view_info,
				None
			).map_err(|e| VulkanError::tag("swap::createImageView",e))?;
			self.swap_images.push((*image,image_view));
		}
		
		Ok(())
	}
	
	unsafe fn destroy_image_views(&mut self, device: &Device) {
		for (_,image_view) in self.swap_images.iter() {
			device.device.destroy_image_view(
				*image_view,
				None
			);
		}
		self.swap_images.clear();
	}
}


pub struct SwapChainConfig {
	pub chosen_swap_extent: vk::Extent2D,
	pub chosen_present_mode: vk::PresentModeKHR,
	pub chosen_format: vk::SurfaceFormatKHR,
	pub chosen_image_count: u32,
	//FIXME: changing the image count: SUPPORTED?
}

impl SwapChainConfig {
	
	pub fn create_default(
		device: &Device,
		window: &Window,
		surface: &Surface
	) -> SwapChainConfig {
		SwapChainConfig {
			chosen_swap_extent: SwapChainConfig::default_swap_extent(
				device,
				window,
				surface
			),
			chosen_present_mode: SwapChainConfig::default_present_mode(device),
			chosen_format: SwapChainConfig::default_format(device),
			chosen_image_count: SwapChainConfig::default_image_count(device),
		}
	}
	
	pub fn print_metadata(&self) {
		info!("  {}",fmt_bold("Selected Swap Configuration:"));
		info!(
			"    Swap Extent: {}x{}",
			self.chosen_swap_extent.width,
			self.chosen_swap_extent.height
		);
		info!(
			"    Swap Present Mode: {:?}",
			self.chosen_present_mode
		);
		info!(
			"    Swap Display Format: {:?}",
			self.chosen_format
		);
		info!(
			"    Swap Image Count: {:?}",
			self.chosen_image_count
		);
	}
	
	pub fn update_swap_extent(
		&mut self,
		device: &Device,
		window: &Window,
		surface: &Surface
	) {
		self.chosen_swap_extent = SwapChainConfig::default_swap_extent(
			device,
			window,
			surface
		);
	}
	
	pub unsafe fn make_swap_chain(
		&self,
		device: &Device,
		surface: &Surface,
		old_swap_chain: Option<vk::SwapchainKHR>
	) -> Result<vk::SwapchainKHR,VulkanError> {
		let info = vk::SwapchainCreateInfoKHR::builder()
			.surface(surface.surface)
			.min_image_count(self.chosen_image_count)
			.image_format(self.chosen_format.format)
			.image_color_space(self.chosen_format.color_space)
			.image_extent(self.chosen_swap_extent)
			.image_array_layers(1)
			.image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::TRANSFER_SRC)
			.image_sharing_mode(vk::SharingMode::EXCLUSIVE)
			.pre_transform(device.raw_device.surface_caps.current_transform)
			.composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
			.present_mode(self.chosen_present_mode)
			.clipped(true)
			.old_swapchain(old_swap_chain.unwrap_or_default());
		
		device.ext_swap_chain.create_swapchain(
			&info,
			None
		).map_err(|e| VulkanError::tag("vk::CreateSwapChain",e))
	}
	
	pub fn get_image_view(
		&self
	) -> vk::ImageViewCreateInfo {
		vk::ImageViewCreateInfo::builder()
			.view_type(vk::ImageViewType::TYPE_2D)
			.format(self.chosen_format.format)
			.subresource_range(vk::ImageSubresourceRange {
				aspect_mask: vk::ImageAspectFlags::COLOR,
				base_mip_level: 0,
				level_count: 1,
				base_array_layer: 0,
				layer_count: 1,
			})
			.build()
	}
	
	
	
	fn default_swap_extent(
		device: &Device,
		window: &Window,
		surface: &Surface
	) -> vk::Extent2D {
		if device.raw_device.surface_caps.current_extent.width != 0xFFFFFFFF {
			device.raw_device.surface_caps.current_extent
		} else {
			let log_size = window.inner_size();
			let size = log_size.to_physical(window.hidpi_factor());
			let width = (size.width as u32)
				.min(device.raw_device.surface_caps.max_image_extent.width)
				.max(device.raw_device.surface_caps.min_image_extent.width);
			let height = (size.height as u32)
				.min(device.raw_device.surface_caps.max_image_extent.height)
				.max(device.raw_device.surface_caps.min_image_extent.height);
			vk::Extent2D { width, height }
			/*
			match window.inner_size() {
				Some(size) => {
				
				}
				None => device.raw_device.surface_caps.min_image_extent,
			}*/
		}
	}
	
	fn default_present_mode(device: &Device) -> vk::PresentModeKHR {
		if device.raw_device.surface_present.contains(&vk::PresentModeKHR::MAILBOX) {
			vk::PresentModeKHR::MAILBOX
		} else {
			vk::PresentModeKHR::FIFO
		}
	}
	
	fn default_format(device: &Device) -> vk::SurfaceFormatKHR {
		let formats = device.raw_device.surface_formats.as_slice();
		if formats.len() == 1 && formats[0].format == vk::Format::UNDEFINED {
			vk::SurfaceFormatKHR {
				format: vk::Format::B8G8R8A8_UNORM,
				color_space: vk::ColorSpaceKHR::SRGB_NONLINEAR,
			}
		} else {
			for format in formats {
				if format.format == vk::Format::B8G8R8A8_UNORM
					&& format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR
				{
					return *format;
				}
			}
			formats[0]
		}
	}
	
	fn default_image_count(device: &Device) -> u32 {
		2.max(device.raw_device.surface_caps.min_image_count)
			.min(device.raw_device.surface_caps.max_image_count)
	}
}