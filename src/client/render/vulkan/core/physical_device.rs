use super::{
	Instance,
	Surface,
	VulkanError
};
use ash::{
	vk,
	version::{
		InstanceV1_0,
		//InstanceV1_1
	}
};
use hashbrown::{
	HashMap,
	hash_map::Entry,
};


pub struct PhysicalDevice {
	pub handle: vk::PhysicalDevice,
	
	pub props: vk::PhysicalDeviceProperties,
	pub memory: vk::PhysicalDeviceMemoryProperties,
	pub features: vk::PhysicalDeviceFeatures,
	pub queues: Vec<(vk::QueueFamilyProperties,bool)>,
	
	pub surface_caps: vk::SurfaceCapabilitiesKHR,
	pub surface_formats: Vec<vk::SurfaceFormatKHR>,
	pub surface_present: Vec<vk::PresentModeKHR>,
	
	_private: ()
}

impl PhysicalDevice {
	
	/// Unsafe since vk::PhysicalDevice could be an invalid handle.
	///  Safe: Instance Open & Surface Open & dev Valid
	pub unsafe fn init(
		instance: &Instance,
		dev: vk::PhysicalDevice,
		surface: &Surface
	) -> Result<PhysicalDevice,VulkanError> {
		
		let props = instance.instance.get_physical_device_properties(
			dev
		);
		
		let memory = instance.instance.get_physical_device_memory_properties(
			dev
		);
		
		let features = instance.instance.get_physical_device_features(
			dev
		);
		
		let queues = instance.instance.get_physical_device_queue_family_properties(
			dev
		).into_iter().enumerate().map(|(idx,queue)| {
			let support = instance.ext_surface.get_physical_device_surface_support(
				dev,
				idx as u32,
				surface.surface
			);
			(queue,support)
		}).collect::<Vec<_>>();
		
		let surface_caps = instance.ext_surface.get_physical_device_surface_capabilities(
			dev,
			surface.surface
		).map_err(|e| VulkanError::tag("vk::getSurfaceCapabilities",e))?;
		
		let surface_formats = instance.ext_surface.get_physical_device_surface_formats(
			dev,
			surface.surface
		).map_err(|e| VulkanError::tag("vk::getSurfaceFormats",e))?;
		
		let surface_present = instance.ext_surface.get_physical_device_surface_present_modes(
			dev,
			surface.surface
		).map_err(|e| VulkanError::tag("vk::getSurfacePresentModes",e))?;
		
		Ok(PhysicalDevice {
			handle: dev,
			props,
			memory,
			features,
			queues,
			surface_caps,
			surface_formats,
			surface_present,
			_private: ()
		})
	}
	
	pub fn is_usable_device(&self) -> bool {
		//FIXME: IMPLEMENT
		true
	}
	
	pub fn score_device(&self) -> i64 {
		//FIXME: MAKE BETTER
		if self.props.device_type == vk::PhysicalDeviceType::DISCRETE_GPU {
			100
		}else{
			10
		}
	}
	
	pub fn print_metadata(&self) {
		use crate::prelude::*;
		use std::ffi::CStr;
		info!("  {}",fmt_bold("Chosen Physical Device:"));
		let name_str = unsafe {
			CStr::from_ptr(self.props.device_name.as_ptr())
		};
		info!(
			"    Name:           {}",
			  fmt_highlight(format!("{:?}",name_str))
		);
		info!(
			"    Driver Version: {}",
			fmt_cyan(format!("{}.{}.{}",
				ash::vk_version_major!(self.props.driver_version),
				ash::vk_version_minor!(self.props.driver_version),
				ash::vk_version_patch!(self.props.driver_version)
			))
		);
		info!(
			"    API Version:    {}",
			  fmt_cyan(format!("{}.{}.{}",
				   ash::vk_version_major!(self.props.api_version),
				   ash::vk_version_minor!(self.props.api_version),
				   ash::vk_version_patch!(self.props.api_version)
			  ))
		);
		info!(
			"    Type:           {}",
			  fmt_cyan(format!("{:?}",self.props.device_type))
		);
	}
	
	pub fn seek_queue_family(
		&self,
		f: impl Fn(&vk::QueueFamilyProperties, bool) -> bool,
		used: &[(u32,u32)]
	) -> Option<(u32,u32)> {
		self.queues.iter().enumerate().filter_map(|(family,(data,support))| {
			if f(data, *support) {
				for index in 0..data.queue_count {
					let test = (family as u32, index);
					if !used.contains(&test) {
						return Some(test);
					}
				}
			}
			None
		}).next()
	}
	
	/// NB: Requires that the queues be formatted correctly,
	///   i.e. no (0,0),(0,16) instead (0,0),(0,1) is VALID
	///   Will return None if this happens
	pub fn generate_queue_info<R,F: FnOnce(&[vk::DeviceQueueCreateInfo]) -> R>(
		f: F,
		queues: &[(u32,u32)],
		priorities: &[f32],
	) -> Option<R> {
		
		//Generate Metadata Mappings
		let mut count_map = HashMap::new();
		for (family,idx) in queues.iter() {
			match count_map.entry(*family) {
				Entry::Occupied(mut e) => {
					*e.get_mut() += 1u32;
				},
				Entry::Vacant(e) => {
					e.insert(1u32);
				},
			}
 		}
		let mut priority_map = count_map.into_iter()
			.map(|(k,v)| {
				let mut vec = Vec::new();
				vec.resize(v as usize,0.0f32);
				(k,vec)
			}).collect::<HashMap<_,_>>();
		
		
		//Assign Priorities
		if queues.len() != priorities.len() {
			return None;
		}
		for ((family, idx),priority) in queues.iter().zip(priorities.iter()) {
			let vec = priority_map.get_mut(family)?;
			let val = vec.get_mut(*idx as usize)?;
			*val = *priority;
		}
		
		//Generate Create Info
		let create_info = priority_map.iter().map(|(family,p_list)| {
			vk::DeviceQueueCreateInfo::builder()
				.queue_family_index(*family)
				.queue_priorities(p_list.as_slice())
				.build()
		}).collect::<Vec<_>>();
		
		//Call Function
		Some(f(create_info.as_slice()))
	}
	
}