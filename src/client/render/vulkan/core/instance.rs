use super::*;
use ash::{
	vk_make_version,
	vk_version_major,
	vk_version_minor,
	vk_version_patch,
	Instance as RawInstance,
	Entry as RawEntry,
	version::{
		EntryV1_0,
		InstanceV1_0,
		//InstanceV1_1
	}
};
use std::ffi::{CString, CStr};
use hashbrown::HashSet;


/// Vulkan Device Instance
pub struct Instance {
	pub entry: RawEntry,
	pub instance: RawInstance,
	pub debug_report: Option<(ext::DebugUtils, vk::DebugUtilsMessengerEXT)>,
	pub ext_surface: khr::Surface,
	pub enabled_layers: HashSet<&'static str>,
	pub enabled_extensions: HashSet<&'static str>,
	
	_private: ()
}


impl Instance {
	
	/// Callback for the vulkan debug utilities messenger
	///  Prints the passed message directly to the log
	///  output
	unsafe extern "system" fn debug_messenger_callback(
		message_severity: vk::DebugUtilsMessageSeverityFlagsEXT,
		message_type: vk::DebugUtilsMessageTypeFlagsEXT,
		p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
		p_user_data: *mut std::ffi::c_void,
	) -> vk::Bool32 {
		let raw_message = CStr::from_ptr((*p_callback_data).p_message);
		let message = raw_message.to_string_lossy();
		match message_severity {
			vk::DebugUtilsMessageSeverityFlagsEXT::ERROR => {
				error!(
					"{}:\n          {}:     {}\n          {}: {}\n          {}:      {}",
					fmt_bold("Vulkan Debug Message"),
					fmt_bold("Severity"),
					fmt_failure("ERROR"),
					fmt_bold("Message Type"),
					fmt_cyan(format!("{:?}",message_type)),
					fmt_bold("Message"),
					&message
				);
			}
			vk::DebugUtilsMessageSeverityFlagsEXT::WARNING => {
				warn!(
					"{}:\n          {}:     {}\n          {}: {}\n          {}:      {}",
					fmt_bold("Vulkan Debug Message"),
					fmt_bold("Severity"),
					fmt_yellow("WARNING"),
					fmt_bold("Message Type"),
					fmt_cyan(format!("{:?}",message_type)),
					fmt_bold("Message"),
					&message
				);
			}
			other_severity => {
				info!(
					"{}:\n          {}:     {}\n          {}: {}\n          {}:      {}",
					fmt_bold("Vulkan Debug Message"),
					fmt_bold("Severity"),
					fmt_highlight(format!("{:?}",other_severity)),
					fmt_bold("Message Type"),
					fmt_cyan(format!("{:?}",message_type)),
					fmt_bold("Message"),
					&message
				);
			}
		}
		vk::FALSE
	}
	
	/// Create a new instance for vulkan
	pub fn new(
		entry: RawEntry,
		enable_debug_utils: bool,
		enable_render_doc_capture: bool,
		enable_debug_layers: u8
	) -> Result<Box<Instance>,VulkanError> {
		const VK_API_MAJOR: u32 = 1;
		const VK_API_MINOR: u32 = 1;
		
		//Check if desired version is available: FAIL if not
		//   - Should Never Fail Due to Check Earlier
		if let Ok(Some(version)) = entry.try_enumerate_instance_version() {
			if !(vk_version_major!(version) == VK_API_MAJOR
				&& vk_version_minor!(version) >= VK_API_MINOR) {
				warn!("Invalid Vulkan API Version: {}.{}.{}",
					vk_version_major!(version),
					vk_version_minor!(version),
					vk_version_patch!(version)
				);
				return Err(VulkanError::MiscError("Insufficient Vulkan Version"));
			}
		}
		
		//Build Application Info
		let application_name = CString::new("Rusty Voxel")
			.map_err(|_| VulkanError::MiscError("CString::new"))?;
		let engine_name = CString::new("Rusty Voxel Engine")
			.map_err(|_| VulkanError::MiscError("CString::new"))?;
		let application_info = vk::ApplicationInfo::builder()
			.engine_name(&engine_name)
			.engine_version(vk_make_version!(0, 1, 0))
			.application_name(&application_name)
			.application_version(vk_make_version!(0, 1, 0))
			.api_version(vk_make_version!(VK_API_MAJOR,VK_API_MINOR,0));
		
		
		//Load Valid Layers & Extensions
		let valid_layer_list = entry.enumerate_instance_layer_properties()
			.map_err(|e| VulkanError::tag("Enumerate Instance Layers",e))?;
		let valid_ext_list = entry.enumerate_instance_extension_properties()
			.map_err(|e| VulkanError::tag("Enumerate Instance Extensions",e))?;
		
		
		//Select Extensions
		let required_extensions = surface::Surface::get_instance_extensions(valid_ext_list.as_slice());
		let mut optional_extensions = Vec::new();
		if enable_debug_utils {
			optional_extensions.push("VK_EXT_debug_utils");
		}
		optional_extensions.push("VK_KHR_get_physical_device_properties2");
		optional_extensions.push("VK_KHR_get_surface_capabilities2");
		
		info!("  {}",fmt_bold("Selecting Vulkan Instance Extensions:"));
		let (ext_list, enabled_extensions) = select_entries(
			&valid_ext_list,
			&required_extensions,
			&optional_extensions,
			|v| &v.extension_name,
			true
		).ok_or(VulkanError::MiscError("Failed to select instance extensions"))?;
		
		
		// Select Optional Layers
		let mut optional_layers = Vec::new();
		if enable_debug_layers > 0 {
			optional_layers.push("VK_LAYER_KHRONOS_validation");
		}
		if enable_debug_layers > 1 {
			optional_layers.push("VK_LAYER_LUNARG_assistant_layer");
		}
		if enable_debug_layers > 2 {
			optional_layers.push("VK_LAYER_LUNARG_api_dump");
		}
		if enable_render_doc_capture {
			optional_layers.push("VK_LAYER_RENDERDOC_Capture");
		}
		info!("  {}",fmt_bold("Selecting Vulkan Instance Layers:"));
		let (layer_list, enabled_layers) = select_entries(
			&valid_layer_list,
			&[],
			&optional_layers,
			|v| &v.layer_name,
			true
		).ok_or(VulkanError::MiscError("Failed to select instance layers"))?;
		
		
		let create_info = vk::InstanceCreateInfo::builder()
			.application_info(&application_info)
			.enabled_extension_names(ext_list.as_slice())
			.enabled_layer_names(layer_list.as_slice());
		let instance : RawInstance = unsafe {
			entry.create_instance(&create_info,None)
		}.map_err(|e| {
			warn!("  {}: {}",fmt_bold("Creating Vulkan Instance"), fmt_failure("Failure"));
			e
		})?;
		info!("  {}: {}",fmt_bold("Creating Vulkan Instance"), fmt_success("Success"));
		
		
		//Enable Debug Utils Messenger if applicable
		let debug_report = if enable_debug_utils && enabled_extensions.contains("VK_EXT_debug_utils"){
			let ext_debug_utils = ext::DebugUtils::new(&entry, &instance);
			let msg_res = unsafe {
				let msg_info = vk::DebugUtilsMessengerCreateInfoEXT::builder()
					.message_severity(
						vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
							| vk::DebugUtilsMessageSeverityFlagsEXT::ERROR
					)
					.message_type(
						vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE
							| vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION
							| vk::DebugUtilsMessageTypeFlagsEXT::GENERAL,
					).pfn_user_callback(Some(Instance::debug_messenger_callback));
				ext_debug_utils.create_debug_utils_messenger(
					&msg_info,
					None
				)
			};
			info!("  {}: {}",
				  fmt_bold("Creating Vulkan Debug Messenger"),
				  fmt_enabled(msg_res.is_ok())
			);
			match msg_res {
				Ok(ext_msg) => {
					info!("  {}:", fmt_bold("Testing Vulkan Debug Callback"));
					unsafe {
						let callback_data = vk::DebugUtilsMessengerCallbackDataEXT::builder()
							.message(std::ffi::CStr::from_bytes_with_nul_unchecked(
								b"Testing...\0"
							));
						ext_debug_utils.submit_debug_utils_message(
							instance.handle(),
							vk::DebugUtilsMessageSeverityFlagsEXT::WARNING,
							vk::DebugUtilsMessageTypeFlagsEXT::GENERAL,
							&callback_data
						);
					}
					Some((ext_debug_utils,ext_msg))
				},
				Err(e) => {
					warn!("  Failed to create debug messenger: {:?}", e);
					None
				}
			}
		}else {
			None
		};
		let ext_surface = khr::Surface::new(&entry, &instance);
		
		//Build Instance
		Ok(Box::new(Instance {
			entry,
			instance,
			debug_report,
			ext_surface,
			enabled_layers,
			enabled_extensions,
			_private: (),
		}))
	}
	
	/// List all Physical Devices available to the vulkan implementation
	///  May return an error if one occurs
	pub fn list_physical_devices(&self, surface: &Surface) -> Result<Vec<PhysicalDevice>,VulkanError> {
		let devices = unsafe {
			self.instance.enumerate_physical_devices()
		}.map_err(|e| VulkanError::tag("vk::enumeratePhysicalDevices",e))?;
		let mut wrapped = Vec::with_capacity(devices.len());
		for device in devices {
			let wrapped_device = unsafe {
				PhysicalDevice::init(
					self,
					device,
					surface
				)
			}?;
			wrapped.push(wrapped_device)
		}
		Ok(wrapped)
	}
	
	/// Destroy the vulkan instance
	pub unsafe fn destroy(&self) {
		if let Some((ext,messenger)) = &self.debug_report {
			ext.destroy_debug_utils_messenger(*messenger,None);
		}
		
		self.instance.destroy_instance(None);
	}
}