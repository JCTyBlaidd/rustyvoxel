use crate::prelude::*;
use super::{
	Instance,
	PhysicalDevice,
	VulkanError,
	util::select_entries
};
use ash::{
	vk,
	extensions::{
		khr::Swapchain
	},
	version::{
		EntryV1_0,
		InstanceV1_0,
		DeviceV1_0,
	},
	Device as RawDevice
};
use hashbrown::HashSet;


pub struct Queue {
	pub handle: vk::Queue,
	pub family: u32,
}


impl Queue {
	pub fn print_metadata(
		device: &PhysicalDevice,
		name: &str,
		value: Option<(u32,u32)>
	) {
		match value {
			Some((family,index)) => {
				info!("  {}", fmt_bold(format!(
					"Selected {} Queue:",
					name
				)));
				info!("    {}: {}",
					  fmt_bold("Family"),
					  family
				);
				info!("    {}:  {}",
					  fmt_bold("Index"),
					  index
				);
				
				let meta = match device.queues.get(family as usize) {
					Some(val) => val,
					None => return
				};
				let props = &meta.0;
				
				info!("    {}:", fmt_bold("Capability"));
				info!("      Present:  {}", fmt_enabled(meta.1));
				info!("      Graphics: {}",
					  fmt_enabled(props.queue_flags.contains(vk::QueueFlags::GRAPHICS))
				);
				info!("      Compute:  {}",
					  fmt_enabled(props.queue_flags.contains(vk::QueueFlags::COMPUTE))
				);
				info!("      Transfer: {}",
					  fmt_enabled(props.queue_flags.contains(vk::QueueFlags::TRANSFER))
				);
				info!("      Sparse:   {}",
					  fmt_enabled(props.queue_flags.contains(vk::QueueFlags::SPARSE_BINDING))
				);
				info!("      Timestamp Bits: {}", props.timestamp_valid_bits);
			},
			None => {
				info!("  {}", fmt_bold(format!(
					"Ignored {} Queue: {}",
					name,
					fmt_failure("Failed to find valid Queue")
				)));
			}
		}
	}
	pub unsafe fn load(dev: &RawDevice, value: (u32, u32)) -> Queue {
		let val = dev.get_device_queue(
			value.0,
			value.1
		);
		Queue {
			handle: val,
			family: value.0,
		}
	}
}

/// A Vulkan Device:
///  Requires:
///   Vulkan 1.1 Core
///  Extensions:
///   Maintainance 1,2,3
///   SwapChain
///   Dedicated Allocation
/// FIXME:
///  Feature Metadata
pub struct Device {
	/// Raw Device Handle
	pub device: RawDevice,
	
	/// Queue with Graphics|Compute|Transfer|Sparse|Present
	pub queue_all: Queue,
	/// Queue with Compute
	pub queue_compute: Option<Queue>,
	/// Queue with Transfer
	pub queue_transfer: Option<Queue>,
	
	pub ext_swap_chain: Swapchain,
	//TODO: OTHER EXTENSIONS
	pub enabled_extensions: HashSet<&'static str>,
	
	pub raw_device: PhysicalDevice,
	_private: (),
}

impl Device {
	
	pub fn required_extensions() -> Vec<&'static str> {
		let mut required_extensions = Vec::with_capacity(4);
		required_extensions.push("VK_KHR_swapchain");
		required_extensions.push("VK_KHR_maintenance1");
		required_extensions.push("VK_KHR_maintenance2");
		required_extensions.push("VK_KHR_maintenance3");
		required_extensions.push("VK_KHR_dedicated_allocation");
		required_extensions
	}
	
	pub fn optional_extensions() -> Vec<&'static str> {
		let mut optional_extensions = Vec::new();
		optional_extensions.push("VK_KHR_push_descriptor");
		optional_extensions
	}
	
	pub unsafe fn init(
		raw_device: PhysicalDevice,
		instance: &Instance,
	) -> Result<Box<Device>,VulkanError>{
		
		//Load applicable extensions & layers
		let valid_ext_list = instance.instance.enumerate_device_extension_properties(
			raw_device.handle
		).map_err(|e| VulkanError::tag("Enumerate Device Extensions",e))?;
		let valid_layer_list = instance.entry.enumerate_instance_layer_properties()
			.map_err(|e| VulkanError::tag("Enumerate Instance{Device} Layers",e))?;
		
		//Choose same layers as instance
		let req_layer = instance.enabled_layers.iter()
			.map(|e| *e).collect::<Vec<_>>();
		let (layer_list,_) = select_entries(
			&valid_layer_list,
			&req_layer,
			&[],
			|layer| &layer.layer_name,
			false
		).ok_or(VulkanError::MiscError("Failed to select device layers"))?;
		
		//Setup Extensions
		info!("  {}",fmt_bold("Selecting Vulkan Device Extensions:"));
		let req_ext = Device::required_extensions();
		let opt_ext = Device::optional_extensions();
		let (ext_list, enabled_extensions)= select_entries(
			&valid_ext_list,
			&req_ext,
			&opt_ext,
			|ext| &ext.extension_name,
			true
		).ok_or(VulkanError::MiscError("Failed to select device extensions"))?;
		
		//Setup Queues
		let mut used_queues = Vec::new();
		let mut priority_queues = Vec::new();
		let all_queue = raw_device.seek_queue_family(|data,support| {
			support && data.queue_flags.contains(
				vk::QueueFlags::GRAPHICS
					| vk::QueueFlags::COMPUTE
					| vk::QueueFlags::TRANSFER
					| vk::QueueFlags::SPARSE_BINDING
			)
		}, &used_queues).ok_or(
			VulkanError::MiscError("Failed to find Graphics|Compute|Transfer|Sparse Queue")
		)?;
		used_queues.push(all_queue);
		priority_queues.push(1.0);
		
		let mut compute_queue = raw_device.seek_queue_family(|data,_| {
			data.queue_flags.contains(vk::QueueFlags::COMPUTE) &&
				!data.queue_flags.contains(vk::QueueFlags::GRAPHICS | vk::QueueFlags::TRANSFER)
		}, &used_queues);
		if compute_queue.is_none() {
			compute_queue = raw_device.seek_queue_family(|data,_| {
				data.queue_flags.contains(vk::QueueFlags::COMPUTE) &&
					!data.queue_flags.contains(vk::QueueFlags::GRAPHICS)
			}, &used_queues);
		}
		if compute_queue.is_none() {
			compute_queue = raw_device.seek_queue_family(|data,_| {
				data.queue_flags.contains(vk::QueueFlags::COMPUTE)
			}, &used_queues);
		}
		if let Some(compute) = &compute_queue {
			used_queues.push(*compute);
			priority_queues.push(0.7);
		}
		
		let mut transfer_queue = raw_device.seek_queue_family(|data,_| {
			data.queue_flags.contains(vk::QueueFlags::TRANSFER)
				&& !data.queue_flags.contains(vk::QueueFlags::GRAPHICS | vk::QueueFlags::COMPUTE)
		}, &used_queues);
		if transfer_queue.is_none() {
			transfer_queue = raw_device.seek_queue_family(|data,_| {
				data.queue_flags.contains(vk::QueueFlags::TRANSFER)
					&& !data.queue_flags.contains(vk::QueueFlags::GRAPHICS)
			}, &used_queues);
		}
		if transfer_queue.is_none() {
			transfer_queue = raw_device.seek_queue_family(|data,_| {
				data.queue_flags.contains(vk::QueueFlags::TRANSFER)
			}, &used_queues);
		}
		if let Some(transfer) = &transfer_queue {
			used_queues.push(*transfer);
			priority_queues.push(0.7);
		}
		
		//Log Queue Selection
		Queue::print_metadata(&raw_device,"General",Some(all_queue));
		Queue::print_metadata(&raw_device,"Compute", compute_queue);
		Queue::print_metadata(&raw_device,"Transfer", transfer_queue);
		
		//Create Queue Info
		let res_device: Result<RawDevice,VulkanError> = PhysicalDevice::generate_queue_info(
			|data| {
				//Create Device Info
				let device_create_info = vk::DeviceCreateInfo::builder()
					.enabled_features(&raw_device.features)
					.enabled_extension_names(&ext_list)
					.enabled_layer_names(&layer_list)
					.queue_create_infos(data);
				
				let device = instance.instance.create_device(
					raw_device.handle,
					&device_create_info,
					None
				).map_err(|e| {
					warn!(
						"  {}: {}",
						fmt_bold("Creating Vulkan Device"),
						fmt_failure("Failure")
					);
					VulkanError::tag("vk::createDevice", e)
				})?;
				info!(
					"  {}: {}",
					fmt_bold("Creating Vulkan Device"),
					fmt_success("Success")
				);
				Ok(device)
			}, &used_queues, &priority_queues
		).ok_or(VulkanError::MiscError("Failed to handle queue info"))?;
		let device = res_device?;
		
		//Load Queues
		let queue_all = Queue::load(&device,all_queue);
		let queue_compute = compute_queue
			.map(|q| Queue::load(&device, q));
		let queue_transfer = transfer_queue
			.map(|q| Queue::load(&device, q));
		
		//Load Extensions
		let ext_swap_chain = Swapchain::new(&instance.instance,&device);
		
		Ok(Box::new(Device {
			device,
			queue_all,
			queue_compute,
			queue_transfer,
			ext_swap_chain,
			enabled_extensions,
			raw_device,
			_private: ()
		}))
	}
	
	pub unsafe fn destroy(&self) {
		self.device.destroy_device(None);
	}
}
