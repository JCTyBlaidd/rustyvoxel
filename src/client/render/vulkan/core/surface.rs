use crate::prelude::*;
use super::{
	compare_with_str,
	Instance,
	VkResult,
	VulkanError
};
use ash::vk;
use winit::window::Window;

pub struct Surface {
	pub surface: vk::SurfaceKHR,
	_private: ()
}

impl Surface {
	pub fn get_instance_extensions(ext_list: &[vk::ExtensionProperties]) -> Vec<&'static str> {
		let mut vec = Vec::with_capacity(2);
		vec.push("VK_KHR_surface");
		vec.push(get_os_extensions(ext_list));
		vec
	}
	
	pub fn new(instance: &Instance, window: &Window) -> Result<Surface, VulkanError> {
		let surface : vk::SurfaceKHR = create_os_surface(instance, window)
			.map_err(|e| VulkanError::tag("Create Vulkan Surface",e))?;
		info!("  {}: {}",fmt_bold("Creating Vulkan Surface"), fmt_success("Success"));
		Ok(Surface {
			surface,
			_private: ()
		})
	}
	
	pub unsafe fn get_physical_device_surface_support(
		&self,
		instance: &Instance,
		physical_device: vk::PhysicalDevice,
		queue_index: u32
	) -> bool {
		instance.ext_surface.get_physical_device_surface_support(
			physical_device,
			queue_index,
			self.surface
		)
	}
	
	pub unsafe fn get_physical_device_surface_present_modes(
		&self,
		instance: &Instance,
		physical_device: vk::PhysicalDevice,
	) -> VkResult<Vec<vk::PresentModeKHR>> {
		instance.ext_surface.get_physical_device_surface_present_modes(
			physical_device,
			self.surface
		)
	}
	
	
	//TODO: surface_capabilities_2 ???
	pub unsafe fn get_physical_device_surface_capabilities(
		&self,
		instance: &Instance,
		physical_device: vk::PhysicalDevice,
	) -> VkResult<vk::SurfaceCapabilitiesKHR> {
		instance.ext_surface.get_physical_device_surface_capabilities(
			physical_device,
			self.surface
		)
	}
	
	//TODO: surface_capabilities_2 ???
	pub unsafe fn get_physical_device_surface_formats(
		&self,
		instance: &Instance,
		physical_device: vk::PhysicalDevice,
	) -> VkResult<Vec<vk::SurfaceFormatKHR>> {
		instance.ext_surface.get_physical_device_surface_formats(
			physical_device,
			self.surface
		)
	}
	
	pub unsafe fn destroy(&self, instance: &Instance) {
		instance.ext_surface.destroy_surface(self.surface,None);
	}
}

#[cfg(target_os = "windows")]
fn get_os_extensions(_: &[vk::ExtensionProperties]) -> &'static str {
	"VK_KHR_win32_surface"
}
#[cfg(target_os = "macos")]
fn get_os_extensions(_: &[vk::ExtensionProperties]) -> &'static str {
	"VK_MVK_macos_surface"
}
#[cfg(target_os = "android")]
fn get_os_extensions(_: &[vk::ExtensionProperties]) -> &'static str {
	"VK_KHR_android_surface"
}
#[cfg(target_os = "ios")]
fn get_os_extensions(_: &[vk::ExtensionProperties]) -> &'static str {
	"VK_MVK_ios_surface"
}
#[cfg(any(
	target_os = "linux",
	target_os = "dragonfly",
	target_os = "freebsd",
	target_os = "netbsd",
	target_os = "openbsd"
))]
fn get_os_extensions(ext: &[vk::ExtensionProperties]) -> &'static str {
	let wayland_found = ext.iter().any(|entry| {
		compare_with_str(&entry.extension_name, "VK_KHR_wayland_surface")
	});
	let x_lib_found = ext.iter().any(|entry| {
		compare_with_str(&entry.extension_name, "VK_KHR_xlib_surface")
	});
	
	if wayland_found {
		"VK_KHR_wayland_surface"
	} else if x_lib_found {
		"VK_KHR_xlib_surface"
	} else {
		"VK_KHR_xcb_surface"
	}
}

#[cfg(target_os = "windows")]
fn create_os_surface(
	instance: &Instance,
	window: &Window
) -> VkResult<vk::SurfaceKHR> {
	use winit::platform::windows::WindowExtWindows;
	let ext = ash::extensions::khr::Win32Surface::new(&instance.entry, &instance.instance);
	let info = ash::vk::Win32SurfaceCreateInfoKHR::builder()
		.hwnd(window.get_hwnd() as _);
	unsafe { ext.create_win32_surface(&info, None) }
}
#[cfg(target_os = "macos")]
fn create_os_surface(
	instance: &Instance,
	window: &Window
) -> VkResult<vk::SurfaceKHR> {
	use winit::platform::macos::WindowExtMacOS;
	let ext = ash::extensions::mvk::MacOSSurface::new(&instance.entry, &instance.instance);
	//TODO: MAY NEED MORE CONFIGURATION?
	let info = ash::vk::MacOSSurfaceCreateInfoMVK::builder()
		.view(window.get_nsview() as _);
	unsafe { ext.create_mac_os_surface_mvk(&info, None) }
}
#[cfg(target_os = "android")]
fn create_os_surface(
	instance: &Instance,
	window: &Window
) -> VkResult<vk::SurfaceKHR> {
	use winit::platform::android::WindowExtAndroid;
	let ext = ash::extensions::khr::AndroidSurface::new(&instance.entry, &instance.instance);
	let info = ash::vk::AndroidSurfaceCreateInfoKHR::builder()
		.window(window.get_native_window() as _);
	unsafe { ext.create_android_surface(&info, None) }
}
#[cfg(target_os = "ios")]
fn create_os_surface(
	instance: &Instance,
	window: &Window
) -> VkResult<vk::SurfaceKHR> {
	use winit::platform::ios::WindowExtIOS;
	let ext = ash::extensions::mvk::IOSSurface::new(&instance.entry, &instance.instance);
	let info = ash::vk::IOSSurfaceCreateInfoMVK::builder()
		.view(window.get_uiview() as _);
	unsafe { ext.create_ios_surface_mvk(&info, None) }
}
#[cfg(any(
	target_os = "linux",
	target_os = "dragonfly",
	target_os = "freebsd",
	target_os = "netbsd",
	target_os = "openbsd"
))]
fn create_os_surface(
	instance: &Instance,
	window: &Window
) -> VkResult<vk::SurfaceKHR> {
	use winit::platform::unix::WindowExtUnix;
	if instance.enabled_extensions.contains("VK_KHR_wayland_surface") {
		let ext = ash::extensions::khr::WaylandSurface::new(&instance.entry, &instance.instance);
		let info = ash::vk::WaylandSurfaceCreateInfoKHR::builder()
			.surface(window.wayland_surface().unwrap())
			.display(window.wayland_display().unwrap());
		unsafe { ext.create_wayland_surface(&info, None) }
	}else if instance.enabled_extensions.contains("VK_KHR_xlib_surface") {
		let ext = ash::extensions::khr::XlibSurface::new(&instance.entry, &instance.instance);
		let info = ash::vk::XlibSurfaceCreateInfoKHR::builder()
			.dpy(window.xlib_display().unwrap() as _)
			.window(window.xlib_window().unwrap());
		unsafe { ext.create_xlib_surface(&info, None) }
	}else{
		let ext = ash::extensions::khr::XcbSurface::new(&instance.entry, &instance.instance);
		let info = ash::vk::XcbSurfaceCreateInfoKHR::builder()
			.connection(window.xcb_connection().unwrap() as _)
			.window(window.xlib_window().unwrap() as _);
		unsafe { ext.create_xcb_surface(&info, None) }
	}
}