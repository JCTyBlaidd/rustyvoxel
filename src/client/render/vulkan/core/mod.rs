use std::cmp::Reverse;
use ash::prelude::VkResult;
use ash::vk;
use ash::extensions::{ext, khr};
use winit::{
	window::Window, error::OsError ,
};

use crate::prelude::*;

pub mod instance;
pub mod surface;
pub mod physical_device;
pub mod device;
pub mod swap_chain;

pub use self::{
	instance::Instance,
	surface::Surface,
	physical_device::PhysicalDevice,
	device::Device,
	swap_chain::SwapChain
};

pub mod util;
use self::util::*;

pub struct VkCore {
	pub instance: Box<Instance>,
	pub surface: Surface,
	pub device: Box<Device>,
	pub swap_chain: SwapChain,
	_private: (),
}

impl VkCore {
	pub fn new(
		entry: ash::Entry,
		window: &Window,
		enable_debug_utils: bool,
		enable_render_doc_capture: bool,
		enable_debug_layers: u8
	) -> Result<VkCore,VulkanError> {
		let instance = Instance::new(
			entry,
			enable_debug_utils,
			enable_render_doc_capture,
			enable_debug_layers
		)?;
		
		let surface = Surface::new(
			&instance,
			window,
		).map_err(|e| {
			unsafe {
				instance.destroy();
			}
			e
		})?;
		
		let physical_device_list =  instance.list_physical_devices(
			&surface
		).map_err(|e| {
			unsafe {
				surface.destroy(&instance);
				instance.destroy();
			}
			e
		})?;
		let mut usable_device_list = physical_device_list.into_iter().filter(
			|p| p.is_usable_device()
		).collect::<Vec<_>>();
		usable_device_list.sort_by_key(|k|{
			Reverse(k.score_device())
		});
		let chosen_physical_device = usable_device_list.into_iter()
			.next().ok_or(VulkanError::MiscError("No Usable Physical Device"))
			.map_err(|e| {
				unsafe {
					surface.destroy(&instance);
					instance.destroy();
				}
				e
			})?;
		chosen_physical_device.print_metadata();
		
		let device = unsafe {
			Device::init(chosen_physical_device,&instance)
		}.map_err(|e| {
			unsafe {
				surface.destroy(&instance);
				instance.destroy();
			}
			e
		})?;
		
		let swap_chain = unsafe {
			SwapChain::create(
				&device,
				window,
				&surface
			)
		}.map_err(|e| {
			unsafe {
				device.destroy();
				surface.destroy(&instance);
				instance.destroy();
			}
			e
		})?;
		
		Ok(VkCore {
			instance,
			surface,
			device,
			swap_chain,
			_private: ()
		})
	}
	
	pub unsafe fn destroy(&mut self) {
		self.swap_chain.destroy(&self.device);
		self.device.destroy();
		self.surface.destroy(&self.instance);
		self.instance.destroy();
	}
}

#[derive(Debug)]
pub enum VulkanError {
	LoadingError(ash::LoadingError),
	InstanceError(ash::InstanceError),
	WindowError(OsError),
	VulkanError(Option<&'static str>, vk::Result),
	MiscError(&'static str),
}
impl From<ash::LoadingError> for VulkanError {
	fn from(err: ash::LoadingError) -> Self {
		VulkanError::LoadingError(err)
	}
}
impl From<ash::InstanceError> for VulkanError {
	fn from(err: ash::InstanceError) -> Self {
		VulkanError::InstanceError(err)
	}
}
impl From<OsError> for VulkanError {
	fn from(err: OsError) -> Self {
		VulkanError::WindowError(err)
	}
}
impl From<vk::Result> for VulkanError {
	fn from(err: vk::Result) -> Self {
		VulkanError::VulkanError(None, err)
	}
}
impl VulkanError {
	pub fn tag(tag: &'static str, res: vk::Result) -> VulkanError {
		VulkanError::VulkanError(Some(tag), res)
	}
}