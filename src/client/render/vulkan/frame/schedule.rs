

/// Schedule Operations
///  with dependency edges
///  between values
pub struct ScheduleGraph {
	nodes: Vec<ScheduleNode>,
	dependencies: Vec<(usize,usize)>,
	resources: Vec<ResourceNode>,
}

pub struct ResourceNode {
	is_generated_before: bool,
}

pub struct ScheduleNode {
	use_resources: Vec<usize>,
	consume_resource: Vec<usize>,
	produce_resource: Vec<usize>,
}