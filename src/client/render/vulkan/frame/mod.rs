use ash::{
	Device as RawDevice,
	version::{
		DeviceV1_0,
		DeviceV1_1
	},
	vk
};

mod schedule;

/// Represents a submission
///  to a command buffer
/// FIXME: Allow more than 1 queue to be
///  used (async-compute) (multi-graphics)?
///
pub struct FrameGraph {
	nodes: Vec<FrameGraph>,
}

/// Operation that can be performed on a command buffer
///  is implemented like this so re-drawing similar yet
///  distinct passes is efficient as possible since each
///  command corresponds to a pre-recoded enum
pub enum FrameNode {
	NodeBeginRenderPass {
		render_pass: vk::RenderPass,
		frame_buffer: vk::Framebuffer,
		render_size: vk::Extent2D,
		clear_values: Box<[vk::ClearValue]>,
		pass_flags: vk::SubpassContents,
	},
	NodeNextSubPass {
		pass_flags: vk::SubpassContents,
	},
	NodeEndRenderPass,
	NodeDrawCommands {
		draw_id: u32,
	},
	NodeBindPipeline {
		pipeline: vk::Pipeline,
		bind_point: vk::PipelineBindPoint,
	},
	NodePipelineBarrier {
		src_stage_mask: vk::PipelineStageFlags,
		dst_stage_mask: vk::PipelineStageFlags,
		dependency_flags: vk::DependencyFlags,
		memory_barriers: Box<[vk::MemoryBarrier]>,
		buffer_memory_barriers: Box<[vk::BufferMemoryBarrier]>,
		image_memory_barriers: Box<[vk::ImageMemoryBarrier]>,
	}
	//NodeWaitEvents?
	//NodeBlitImage?
	//NodeResolveImage?
	//NodeClearBuffer
	//NodeClearImage?
	//NodeCopyBuffer?
}

impl FrameNode {
	pub unsafe fn exec_node(
		&self,
		dev: &RawDevice,
		cmd: vk::CommandBuffer,
		mut draw: impl FnMut(u32,&RawDevice,vk::CommandBuffer)
	) {
		use FrameNode::*;
		match self {
			NodeBeginRenderPass {
				render_pass,
				frame_buffer,
				render_size,
				clear_values,
				pass_flags,
			} => {
				let begin_info = vk::RenderPassBeginInfo::builder()
					.render_pass(*render_pass)
					.framebuffer(*frame_buffer)
					.render_area(vk::Rect2D {
						offset: vk::Offset2D::default(),
						extent: *render_size
					}).clear_values(clear_values);
				dev.cmd_begin_render_pass(
					cmd,
					&begin_info,
					*pass_flags
				);
			},
			NodeNextSubPass {
				pass_flags
			} => {
				dev.cmd_next_subpass(cmd,*pass_flags);
			},
			NodeEndRenderPass => {
				dev.cmd_end_render_pass(cmd);
			},
			NodeDrawCommands {
				draw_id,
			} => {
				draw(*draw_id, dev, cmd);
			}
			NodeBindPipeline {
				pipeline,
				bind_point
			} => {
				dev.cmd_bind_pipeline(
					cmd,
					*bind_point,
					*pipeline
				);
			},
			NodePipelineBarrier {
				src_stage_mask,
				dst_stage_mask,
				dependency_flags,
				memory_barriers,
				buffer_memory_barriers,
				image_memory_barriers,
			} => {
				dev.cmd_pipeline_barrier(
					cmd,
					*src_stage_mask,
					*dst_stage_mask,
					*dependency_flags,
					memory_barriers,
					buffer_memory_barriers,
					image_memory_barriers
				)
			}
		}
	}
}