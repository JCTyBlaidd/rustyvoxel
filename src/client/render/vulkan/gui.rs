use super::*;


pub struct VulkanGui {

}
impl<'inst> GuiBackend<'inst> for VulkanGui {
	fn start_draw(&mut self) {
		unimplemented!()
	}
	
	fn store_data(&mut self, x: f32, y: f32, u: f32, v: f32, col: u32) {
		unimplemented!()
	}
	
	fn change_state(&mut self) {
		unimplemented!()
	}
	
	fn finish_draw(&mut self) {
		unimplemented!()
	}
	
	fn redraw_old(&mut self) {
		unimplemented!()
	}
}