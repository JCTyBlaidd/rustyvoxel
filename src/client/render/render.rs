use super::{
	GuiBackend,
	WorldBackend,
	WindowBackend,
	TexAtlas,
	WindowMetadata,
	WindowHandle,
	StateChange
};
use winit::{
	window::Window,
	event_loop::EventLoop
};

/// Rendering API Backend
pub trait Backend {
	type Gui : for <'a> GuiBackend<'a>;
	type World : for <'a> WorldBackend<'a>;//FIXME: CONVERT TO BORROWABLE?!?
	type Window : WindowBackend;
	
	fn update_atlas(&mut self, atlas: &TexAtlas);
	
	fn make_gui(&self) -> Self::Gui;
}


pub struct RenderBackend<B: Backend> {
	backend: B,
}
impl<B: Backend> RenderBackend<B> {
	pub fn new(backend: B) -> RenderBackend<B> {
		RenderBackend {
			backend
		}
	}
}
pub trait RenderAPI {
	fn perform_state_change(&mut self,change: &StateChange) -> Result<(),()>;
	fn draw_frame(
		&mut self,
		world_state: &(),
		gui_state: &(),
		meta: &WindowMetadata
	) -> Result<(),()>;
}
impl<B: Backend> RenderAPI for RenderBackend<B> {
	fn perform_state_change(&mut self,change: &StateChange) -> Result<(),()> {
		
		Ok(())
	}
	fn draw_frame(
		&mut self,
		world_state: &(),
		gui_state: &(),
		meta: &WindowMetadata
	) -> Result<(),()> {
		
		//If required: Graphics Tick [force & not rate limited & not api limited]
		//               [DrawGui] [w/ DrawDebugData]
		// [PrepareFrame]                     [SubmitFrame]
		//               [GenWorld][DrawWorld]   //CAN GenWorld concurrently with prepare frame?
		// [SendServerUpdates]
		
		Ok(())
	}
}


/// Generic Structure That Manages
///  and handles the window backend
pub struct GlobalRenderBackend<B: Backend> {
	backend: RenderBackend<B>,
	window: WindowHandle<B::Window>,
}
impl<B: Backend> GlobalRenderBackend<B> {
	pub fn new(window: B::Window, backend: B) -> GlobalRenderBackend<B> {
		GlobalRenderBackend {
			backend: RenderBackend::new(backend),
			window: WindowHandle::create_from(window),
		}
	}
}
impl<B: Backend> GlobalRenderAPI for GlobalRenderBackend<B> {
	fn get_event_loop_and_meta(&mut self) -> (&mut EventLoop<()>, &mut WindowMetadata, &mut dyn RenderAPI) {
		let events = self.window.window_backend.get_event_loop();
		let metadata = &mut self.window.window_metadata;
		let api = &mut self.backend;
		(events,metadata,api)
	}
}

///Dynamically Dispatched Global Render API
///  called by rendering api
pub trait GlobalRenderAPI {
	fn get_event_loop_and_meta(& mut self) -> (
		&mut EventLoop<()>,
		&mut WindowMetadata,
		&mut dyn RenderAPI
	);
}