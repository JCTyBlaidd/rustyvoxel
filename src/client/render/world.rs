
pub trait WorldBackend<'inst>: Send + Sync {

}

pub struct WorldRender<'inst, T: WorldBackend<'inst>> {
	backend: T,
	refe: &'inst (),
}

impl<'inst,T: WorldBackend<'inst>> WorldRender<'inst,T> {

}