use std::sync::Arc;
use parking_lot::RwLock;

mod loader;
pub use self::loader::{
	ResourceSupplier,
	ResourceMutator,
	PathResourceSupplier
};

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum ResourceType {
	Shader,
	Texture,
}

pub struct Resource {
	last_update_id: usize,
	resource_type: ResourceType,
	resource_id: Box<str>,
}

impl Resource {
	pub fn get_id(&self) -> &str {
		&self.resource_id
	}
	pub fn get_type(&self) -> ResourceType {
		self.resource_type
	}
	pub fn needs_update(&self, manager: &ResourceManager) -> bool {
		self.last_update_id != manager.current_update_id
	}
	pub fn get_bytes(&mut self, manager: &ResourceManager) -> Option<Vec<u8>> {
		manager.resource_loader.load_resource(
			&self.resource_id,
			self.resource_type
		)
	}
}

pub struct ResourceManager {
	current_update_id: usize,
	resource_loader: loader::ResourceLoader
}

impl ResourceManager {
	pub fn create() -> ResourceManager {
		let mut resource_loader = loader::ResourceLoader::new();
		resource_loader.register_supplier(Box::new(
			PathResourceSupplier::create_default())
		);
		ResourceManager {
			current_update_id: 0,
			resource_loader
		}
	}
	pub fn register_supplier(&mut self, supplier: Box<dyn ResourceSupplier>) {
		self.resource_loader.register_supplier(supplier);
	}
	
	pub fn register_mutator(&mut self, mutator: Box<dyn ResourceMutator>) {
		self.resource_loader.register_mutator(mutator);
	}
}