use super::ResourceType;
use std::path::{
	Path,
	PathBuf,
	Component,
};
use std::fs::read;

pub trait ResourceSupplier {
	fn supply_resource(
		&self,
		res_id: &str,
		res_type: ResourceType,
	) -> Option<Vec<u8>>;
}

pub trait ResourceMutator {
	fn mutate_resource(
		&self,
		res_id: &str,
		res_type: ResourceType,
		value: &mut Vec<u8>
	);
}

pub struct ResourceLoader {
	supplier_list: Vec<Box<dyn ResourceSupplier>>,
	mutator_list: Vec<Box<dyn ResourceMutator>>,
}

impl ResourceLoader {
	pub fn new() -> ResourceLoader {
		ResourceLoader {
			supplier_list: Vec::new(),
			mutator_list: Vec::new(),
		}
	}
	
	pub fn register_supplier(&mut self, supplier: Box<dyn ResourceSupplier>) {
		self.supplier_list.push(supplier);
	}
	
	pub fn register_mutator(&mut self, mutator: Box<dyn ResourceMutator>) {
		self.mutator_list.push(mutator);
	}
	
	pub fn load_resource(&self, res_id: &str, res_type: ResourceType) -> Option<Vec<u8>>{
		let mut supplied = self.supplier_list
			.iter().filter_map(|supplier| {
			supplier.supply_resource(res_id,res_type)
		}).next()?;
		
		for mutator in self.mutator_list.iter() {
			mutator.mutate_resource(res_id, res_type, &mut supplied);
		}
		
		Some(supplied)
	}
}

pub struct PathResourceSupplier {
	base_path: PathBuf
}

impl PathResourceSupplier {
	pub fn create(base_path: PathBuf) -> PathResourceSupplier {
		PathResourceSupplier {
			base_path
		}
	}
	
	//FIXME: IMPLEMENT BETTER LOCATION???
	pub fn create_default() -> PathResourceSupplier {
		use std::env;
		let mut path = env::current_dir().unwrap_or_else(
			|_| {
				let mut other_path = PathBuf::new();
				other_path.push("./");
				other_path
			}
		);
		path.push("resources");
		Self::create(path)
	}
	
	fn get_folder_for_type(res_type: ResourceType) -> &'static str {
		match res_type {
			ResourceType::Shader => "shaders",
			ResourceType::Texture => "textures",
		}
	}
	fn get_file_type_for_type(res_type: ResourceType) -> &'static str {
		match res_type {
			ResourceType::Shader => "shader",
			ResourceType::Texture => "png",
		}
	}
}

impl ResourceSupplier for PathResourceSupplier {
	fn supply_resource(&self, res_id: &str, res_type: ResourceType) -> Option<Vec<u8>> {
		let mut new_path = self.base_path.clone();
		new_path.push(Self::get_folder_for_type(res_type));
		
		//Split ID into Sections
		let sub_path = Path::new(res_id);
		let mut sub_id_list = Vec::new();
		for value in sub_path.components() {
			if let Component::Normal(entry) = value {
				sub_id_list.push(entry);
			}
		}
		
		//Append Sections
		let (id,sections) = sub_id_list.as_slice().split_last()?;
		for section in sections {
			new_path.push(*section);
		}
		
		//Append File Type
		let mut new_file = id.to_os_string();
		new_file.push(".");
		new_file.push(Self::get_file_type_for_type(res_type));
		new_path.push(&new_file);
		
		//Read File Contents
		//log::debug!("Loading Resource At: {:?}", new_path);
		read(new_path).ok()
	}
}