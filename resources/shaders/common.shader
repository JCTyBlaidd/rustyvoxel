#![type(library)]
//FIXME: ADD SHADER PIPE DESCRIPTIONS && CONFIG -> DESCRIPTION MAPPING

#[id(gui_vertex)]
input(vertex) = GuiVertex {
    position = vec2
    uv       = vec2
    colour   = vec4
}

#[id(gui_textures)]
input(uniform) = GuiTextures {
    array = sampler2D[32]
};


#[id(gui_push_constant)]
input(push_constant) = GuiPushConstants {
    tex_index = int,
    use_texture = bool
}

#[id(world_vertex)]
input(vertex) = WorldVertex {
    position = vec3,
    tangents = vec4,
    colour   = vec4,
    lighting = vec4,
    uv       = vec2,
    texture  = ivec2,
    max_anim = int
}

#[id(world_textures)]
input(uniform) = WorldTextures {

}

//FIXME: ADD ADDITIONAL STUFF
#[id(world_uniform_data)]
input(uniform) = WorldData {
    projection = mat4,
    view       = mat4

}

#[id(world_push_constant)]
input(push_constant) = GuiPushConstants {
    chunk_offset = vec3
}



//FIXME: CHUNK_SRC_BUFFER + CHUNK_LOC_TREE_BUFFER (FOR RAY-TRACING)