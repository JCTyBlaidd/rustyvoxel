#![type(shader)]

input(vertex) = GuiVertex;
input(uniform) = GuiTextures;
input(push_constant) = GuiPushConstants;

output = {
    colour(FINAL) = vec4 : load_from(WORLD_FINAL)
}

shader vertex = {
    let extern uv : vec2 = GuiVertex.uv;
    let extern colour : vec2 = GuiVertex.colour;
    Position = vec4(GuiVertex.pos, 0.0, 1.0);
}

shader fragment = {
    let tmp : vec4 = vec4(1.0);
    if(GuiPushConstants.use_texture) {
        tmp = GuiTextures.array[GuiPushConstants.tex_index].get(vertex.uv)
    }
    output.colour = tmp * vertex.colour
}